import {
  Injectable,
  Logger,
  UnsupportedMediaTypeException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as fs from 'fs-extra';
import { basename, extname, join, parse } from 'path';
import { execFile } from 'child-process-promise';
import { js2xml, xml2js } from 'xml-js';
import * as sharp from 'sharp';
import { BufferGeometry, EdgesGeometry, Mesh } from 'three';
import * as BufferGeometryUtils from '@utils/three/buffer-geometry-utils';
import { OBJLoader } from '@utils/three/obj-loader';
import { geo2dae } from '@utils/three-utils';
import { extractFileFromZip, openZipFile } from '@utils/jszip';
import { escapeRegex } from '@utils/string-utils';
import { findAndReplaceLine } from '@utils/line-reader';

@Injectable()
export class ObjectMediaService {
  private readonly logger = new Logger(ObjectMediaService.name);

  constructor(private readonly config: ConfigService) {}

  async generateGltfFromObj(file: { path: string; name: string }): Promise<{
    name: string;
    path: string;
    original: string;
    gltf: string;
    type: string;
  }> {
    const parsedPath = parse(file.name);
    const objFile = join(file.path, file.name);
    const daeRawFile = join(file.path, 'temp_raw.dae');
    const daeEdgesFile = join(file.path, 'temp_edges.dae');
    const gltfFile = join(file.path, parsedPath.base + '.gltf');

    // check if file exists
    const exists = await fs.pathExists(objFile);
    if (!exists) {
      throw new Error(`Path does not exist: ${objFile}`);
    }

    // check for correct file type
    if (parsedPath.ext !== '.obj') {
      throw new Error(`File format not supported: ${parsedPath.ext}`);
    }

    this.logger.log('Processing: ' + file.name);

    // convert OBJ to COLLADA
    await execFile(this.config.get<string>('exec.Assimp'), [
      'export',
      objFile,
      daeRawFile,
      '-jiv',
      '-om',
      '-tri',
    ]);

    // read COLLADA file
    const daeRaw = await fs.readFile(daeRawFile, 'utf8');
    const dae: any = xml2js(daeRaw, { compact: true });

    const daeGeo = dae.COLLADA.library_geometries;
    const daeScene = dae.COLLADA.library_visual_scenes.visual_scene;

    if (!Array.isArray(daeGeo.geometry)) {
      daeGeo.geometry = [daeGeo.geometry];
    }

    if (!Array.isArray(daeScene.node)) {
      daeScene.node = [daeScene.node];
    }

    // load obj
    const objRaw = await fs.readFile(objFile, { encoding: 'utf8' });
    const obj = new OBJLoader().parse(objRaw);

    // generate edges
    const edgesGeometries: BufferGeometry[] = [];

    obj.children.forEach((child) => {
      if (child instanceof Mesh) {
        const geo = new EdgesGeometry(child.geometry, 24.0);
        edgesGeometries.push(geo);
      }
    });

    const geoCombined =
      BufferGeometryUtils.mergeBufferGeometries(edgesGeometries);
    geoCombined.name = 'edges';

    // add edges to collada
    daeGeo.geometry.push(geo2dae(geoCombined));

    daeScene.node.push({
      _attributes: {
        id: geoCombined.name,
      },
      matrix: {
        _text: '1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1',
      },
      instance_geometry: {
        _attributes: {
          url: '#' + geoCombined.name + '_geometry',
        },
        bind_material: {
          technique_common: {
            instance_material: {
              _attributes: {
                symbol: 'defaultMaterial',
                target: '#DefaultMaterial',
              },
            },
          },
        },
      },
    });

    // write DAE file
    await fs.writeFile(daeEdgesFile, js2xml(dae, { spaces: 2, compact: true }));

    // convert DAE to GLTF
    await execFile(this.config.get<string>('exec.Collada2Gltf'), [
      '-i',
      daeEdgesFile,
      '-o',
      gltfFile,
      '-d',
      '-t',
    ]);

    // remove temp files
    await fs.remove(daeRawFile);
    await fs.remove(daeEdgesFile);

    return {
      name: parsedPath.base,
      path: file.path,
      original: file.name,
      gltf: parsedPath.base + '.gltf',
      type: 'gltf',
    };
  }

  async generateGltfFromZip(file: { path: string; name: string }): Promise<{
    name: string;
    path: string;
    original: string;
    gltf: string;
    type: string;
  }> {
    const parsedPath = parse(file.path);
    const zipFile = join(file.path, file.name);
    const objTempFile = join(file.path, parsedPath.base + '.obj');

    // read zip file
    const zip = await openZipFile(zipFile);

    // extract obj file
    const objResults = zip.file(/.+\.obj$/i);
    if (objResults[0]) {
      await extractFileFromZip(objResults[0], objTempFile);
    } else {
      throw new UnsupportedMediaTypeException(
        'Zip file does not contain one of the following formats: obj',
      );
    }

    // find and extract mtl files and textures
    const mtlFiles: string[] = [];
    const imgFiles: { originalName: string; approvedName: string }[] = [];

    // find (and replace) mtl files
    await findAndReplaceLine(
      objTempFile,
      /^(mtllib)\s+(?:.*[\\/])?(.+)$/i,
      (matches) => {
        mtlFiles.push(matches[2]);
        return `${matches[1]} ${matches[2]}`;
      },
    );

    // iterate over mtl files
    for (const mtl of mtlFiles) {
      const mtlResults = zip.file(new RegExp(escapeRegex(mtl) + '$'));

      if (mtlResults[0]) {
        // extract mtl file
        const mtlFile = join(file.path, mtl);
        await extractFileFromZip(mtlResults[0], mtlFile);

        // find textures
        await findAndReplaceLine(
          mtlFile,
          /^(\s*map.+)\s+(?:.*[\\/])?(.+)$/,
          (matches) => {
            const tFile = matches[2];
            let tFileMeta = imgFiles.find((m) => m.originalName === tFile);

            if (!tFileMeta) {
              tFileMeta = {
                originalName: tFile,
                approvedName: tFile,
              };
              // rename if not jpg or png
              if (!['.jpg', '.png'].includes(extname(tFile))) {
                tFileMeta.approvedName = tFile.replace(/\.\S+$/, '.jpg');
              }
              imgFiles.push(tFileMeta);
            }

            return `${matches[1]} ${tFileMeta.approvedName}`;
          },
        );
      }
    }

    // iterate over found texture files
    for (const img of imgFiles) {
      const imgResults = zip.file(
        new RegExp(escapeRegex(img.originalName) + '$'),
      );

      if (imgResults[0]) {
        // extract image file
        await extractFileFromZip(
          imgResults[0],
          join(file.path, img.originalName),
        );

        // resize and convert image
        const buffer = await sharp(join(file.path, img.originalName))
          .resize({
            width: 2048,
            height: 2048,
            fit: 'inside',
            withoutEnlargement: true,
          })
          .jpeg({
            quality: 80,
            mozjpeg: true,
          })
          .toBuffer();
        await sharp(buffer).toFile(join(file.path, img.approvedName));

        // resize for mobile usage (max 1024 pixels)
        await sharp(join(file.path, img.originalName))
          .resize({
            width: 1024,
            height: 1024,
            fit: 'inside',
            withoutEnlargement: true,
          })
          .jpeg({
            quality: 60,
            mozjpeg: true,
          })
          .toFile(
            join(file.path, img.approvedName.replace(/\.[^.]+$/, '_mobile$&')),
          );

        // if approved name differs from original name, remove the original file
        if (img.originalName !== img.approvedName) {
          await fs.remove(join(file.path, img.originalName));
        }
      }
    }

    // process obj
    const meta = await this.generateGltfFromObj({
      path: file.path,
      name: basename(objTempFile),
    });

    // remove extracted obj and mtl
    await fs.remove(objTempFile);
    for (const mtl of mtlFiles) {
      await fs.remove(join(file.path, mtl));
    }

    return meta;
  }
}
