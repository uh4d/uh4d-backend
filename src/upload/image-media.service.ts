import { Injectable, Logger } from '@nestjs/common';
import { v4 as uuid } from 'uuid';
import { join, parse } from 'node:path';
import * as sharp from 'sharp';
import * as fs from 'fs-extra';

@Injectable()
export class ImageMediaService {
  private readonly logger = new Logger(ImageMediaService.name);

  /**
   * Process image: copy to data folder, create thumbnails, determine dimensions, etc.
   */
  async generateImageMedia(file: string) {
    const parsedPath = parse(file);
    const shortPath = `images/${uuid()}/`;
    const path = join(parsedPath.dir, shortPath);
    const filename = parsedPath.base;
    const filenameThumb = parsedPath.name + '_thumb.jpg';
    const filenamePreview = parsedPath.name + '_preview.jpg';
    const filenameTiny = parsedPath.name + '_tiny.jpg';

    this.logger.log(`Directory: ${shortPath} File: ${filename}`);

    try {
      const image = sharp(file);

      // get image size
      const metadata = await image.metadata();

      // create directory
      await fs.ensureDir(path);

      // copy image into directory
      await fs.copy(file, join(path, filename));

      // create thumbnail
      await image
        .clone()
        .resize({
          width: 200,
          height: 200,
          fit: 'outside',
          withoutEnlargement: true,
        })
        .jpeg({
          quality: 80,
          mozjpeg: true,
        })
        .toFile(join(path, filenameThumb));

      // down-sample preview image
      await image
        .clone()
        .resize({
          width: 2048,
          height: 2048,
          fit: 'outside',
          withoutEnlargement: true,
        })
        .jpeg({
          quality: 80,
          mozjpeg: true,
        })
        .toFile(join(path, filenamePreview));

      // create tiny thumbnail (for 3D preview
      await image
        .clone()
        .resize({
          width: 64,
          height: 64,
          fit: 'fill',
        })
        .jpeg({
          quality: 80,
          mozjpeg: true,
        })
        .toFile(join(path, filenameTiny));

      return {
        path: shortPath,
        original: filename,
        type: parsedPath.ext.slice(1),
        preview: filenamePreview,
        thumb: filenameThumb,
        tiny: filenameTiny,
        width: metadata.width,
        height: metadata.height,
      };
    } catch (e) {
      this.logger.warn('Unlink ' + path);
      await fs.remove(path);

      throw e;
    }
  }
}
