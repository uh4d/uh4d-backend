import { Test, TestingModule } from '@nestjs/testing';
import { ObjectMediaService } from './object-media.service';

describe('ObjectMediaService', () => {
  let service: ObjectMediaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ObjectMediaService],
    }).compile();

    service = module.get<ObjectMediaService>(ObjectMediaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
