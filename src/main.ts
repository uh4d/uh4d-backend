import { NestFactory } from '@nestjs/core';
import { RequestMethod, ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { WsAdapter } from '@nestjs/platform-ws';
import { ensureDir, pathExistsSync } from 'fs-extra';
import { join } from 'node:path';
import { RedocConfig } from '@config/redoc.config';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, { cors: true });

  const config = app.get(ConfigService);

  // check if directories exists
  if (!pathExistsSync(config.get('path.data')))
    throw new Error(
      `Data directory ${config.get(
        'path.data',
      )} as specified in config.yaml does not exist!`,
    );
  if (!pathExistsSync(config.get('path.logs')))
    throw new Error(
      `Logs directory ${config.get(
        'path.logs',
      )} as specified in config.yaml does not exist!`,
    );
  if (!pathExistsSync(config.get('path.cache')))
    throw new Error(
      `Cache directory ${config.get(
        'path.cache',
      )} as specified in config.yaml does not exist!`,
    );

  await ensureDir(join(config.get('path.data'), 'tmp'));

  // setup documentation
  await app.get(RedocConfig).setup(app);

  // global settings
  app.useGlobalPipes(new ValidationPipe({ whitelist: true }));
  app.setGlobalPrefix('api', {
    exclude: [{ path: 'data/batch', method: RequestMethod.ALL }],
  });

  app.useWebSocketAdapter(new WsAdapter(app));

  app.enableShutdownHooks();

  await app.listen(config.get('port'));
}
bootstrap();
