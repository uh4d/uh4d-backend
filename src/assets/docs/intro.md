The public API of the backend of the UH4D Browser and associated applications.

### General information

References to files have to be resolved using the file information (path + filename) prefixed by `https://4dbrowser.urbanhistory4d.org/data/`.

---
