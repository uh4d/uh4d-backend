### Spatial status

Images can have different levels of spatialization.
These levels are marked by a number and can be accessed by the property `spatialStatus: <number>`.
The meaning are as follows:

| #   | Label                                    | Description                                                                                                                                                                                                  |
|-----|------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 0   | Not yet spatialized                      | There is no spatial information at all. However, `latitude` and `longitude` in combination with `radius` (> 0) are still set to define a region (e.g. city), in which the image should be somewhere located. |
| 1   | Spatialization not possible              | Same as `0`, but the image is not a photograph or has no central perspective and can therefore not be spatialized (e.g. drawings, plans).                                                                    |
| 2   | Spatialization may be possible in future | Same as `0`, but the image may be spatialized as soon as necessary information is given, e.g. the 3D/4D city model has been extended accordingly.                                                            |
| 3   | Position is known, but no orientation    | Only `latitude` and `longitude` are known, but no `altitude` and any other camera parameters. It differs from `0` that `radius` is not set or equals `0`.                                                    |
| 4   | Manually spatialized                     | The image has been manually spatialized. All interior and exterior camera parameters are given, but might not be very precise.                                                                               |
| 5   | Spatialized by automatic pipeline        | Same as `4`, but all the camera parameters were computed by a (semi-)automatic spatialization pipeline and can therefore be considered as more precise.                                                      |

### Spatial information of images

Images have a `camera` property that has information about the exterior and interior orientation of the camera.
This information is needed to position and construct the image in 3D space.
`latitude` and `longitude` are always given.
If the other values are given depends on according to the `spatialStatus`.

The `camera` property of a database entry is an object with several values:

| `camera.`     | Description                                                                                    |
|---------------|------------------------------------------------------------------------------------------------|
| longitude     | Geographic coordinate that specifies the east-west position                                    |
| latitude      | Geographic coordinate that specifies the north-south position                                  |
| altitude      | Height above sea level (meter)                                                                 |
| radius        | If radius is > 0, than the image can not be positioned, and all following values are not given |
| omega &omega; | Pitch angle (radians)                                                                          |
| phi &phi;     | Heading angle (radians)                                                                        |
| kappa &kappa; | Roll angle (radians)                                                                           |
| ck            | Camera constant                                                                                |
| offset        | Principal point (offset from midpoint)                                                         |
| k             | List of distortion coefficients                                                                |

<object data="/api/assets/docs/image-orientation.svg" type="image/svg+xml"></object>

The world coordinate system is supposed to be oriented as follows:
- positive X axis -> east
- positive Y axis -> upwards
- positive Z axis -> south

#### Exterior Orientation

Since the position of an image is stored in WGS-84 coordinates, i.e. latitude, longitude, and altitude,
it might be necessary to convert the values into Cartesian coordinates to properly display the images.

The position and orientation of an object that visualizes an image in 3D can be set as follows (assuming `image` is an instance of `Object3D`):

<small>_All code snippets are using three.js classes and may differ in the actual implementation._</small>

```javascript
// set position
// values probably have to be transformed to your application needs (e.g. Cartesian coordinates)
const position = convertToCartesian(camera.longitude, camera.altitude, camera.latitude);
image.position.copy(position);

// set orientation
image.rotation.set(camera.omega, camera.phi, camera.kappa, 'YXZ');
```

#### Interior Orientation

For the interior orientation of the camera, the camera constant `ck` is used to compute the field of view.
The camera constant `ck` is unitless and relates to an image height of `1`, while the image width is the aspect ratio.
The actual image dimensions width and height (in pixels) can be retrieved from the `file` property.

The field of view (in degrees) can be calculated trigonometrically:

<object data="/api/assets/docs/field-of-view.svg" type="image/svg+xml" width="60%"></object>

```javascript
const width = file.width / file.height; // aspect ratio
const height = 1;

const fov = 2 * Math.atan(height / (2 * camera.ck)) * MathUtils.RAD2DEG;
```

Especially in architectural photography, tilt-shift lenses have been used, such that vertical lines of the object appear vertical in the image.
This results in a principal point that is usually located in the lower half of the image.
`offset` is the offset of the principal point from the midpoint of the image.
For manually spatialized images, the `offset` will be `[0,0]`, the center of the images.
The values also relate to an image height of `1` and the aspect ratio as the image width.

When constructing an image plane to visualize the image, the camera center would be at the local origin and the image plane would positioned in front of the camera with a distance (z axis) set by the `ck` and shifted along x and y axes by the offset:

```javascript
const offset = new Vector2().fromArray(config.offset).negate();
      
plane.position.set(offset.x, offset.y, -camera.ck);
```

The distortion coefficients `k` are not yet supported, but will be saved as part of the automatically computed spatialization values.
