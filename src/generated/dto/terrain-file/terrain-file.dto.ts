import { ApiProperty } from '@nestjs/swagger';

export class TerrainFileDto {
  @ApiProperty({
    required: false,
  })
  path: string;
  @ApiProperty({
    required: false,
  })
  file: string;
  @ApiProperty({
    required: false,
  })
  type: string;
}
