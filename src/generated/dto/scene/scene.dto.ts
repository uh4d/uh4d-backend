import { ApiProperty } from '@nestjs/swagger';

export class SceneDto {
  @ApiProperty({
    required: false,
  })
  id: string;
  @ApiProperty({
    description: 'Name of the scene',
    required: false,
  })
  name: string;
  @ApiProperty({
    description:
      'Geographic coordinate that specifies the east-west position (origin of local coordinate system)',
    type: 'number',
    format: 'float',
    required: false,
  })
  latitude: number;
  @ApiProperty({
    description:
      'Geographic coordinate that specifies the north-south position (origin of local coordinate system)',
    type: 'number',
    format: 'float',
    required: false,
  })
  longitude: number;
  @ApiProperty({
    description:
      'Height above sea level (meter) (origin of local coordinate system)',
    type: 'number',
    format: 'float',
    required: false,
  })
  altitude: number;
}
