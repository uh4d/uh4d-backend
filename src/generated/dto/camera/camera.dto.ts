import { ApiProperty } from '@nestjs/swagger';

export class CameraDto {
  @ApiProperty({
    type: 'number',
    format: 'float',
    required: false,
  })
  latitude: number;
  @ApiProperty({
    type: 'number',
    format: 'float',
    required: false,
  })
  longitude: number;
  @ApiProperty({
    type: 'number',
    format: 'float',
    required: false,
  })
  altitude: number;
  @ApiProperty({
    type: 'number',
    format: 'float',
    required: false,
    nullable: true,
  })
  radius: number | null;
  @ApiProperty({
    type: 'number',
    format: 'float',
    required: false,
    nullable: true,
  })
  omega: number | null;
  @ApiProperty({
    type: 'number',
    format: 'float',
    required: false,
    nullable: true,
  })
  phi: number | null;
  @ApiProperty({
    type: 'number',
    format: 'float',
    required: false,
    nullable: true,
  })
  kappa: number | null;
  @ApiProperty({
    type: 'number',
    format: 'float',
    required: false,
    nullable: true,
  })
  ck: number | null;
  @ApiProperty({
    type: 'number',
    format: 'float',
    required: false,
  })
  offset: number[];
  @ApiProperty({
    type: 'number',
    format: 'float',
    required: false,
  })
  k: number[];
}
