import { ApiProperty } from '@nestjs/swagger';

export class ImageMetaDto {
  @ApiProperty({
    required: false,
    nullable: true,
  })
  description: string | null;
  @ApiProperty({
    required: false,
    nullable: true,
  })
  misc: string | null;
  @ApiProperty({
    required: false,
    nullable: true,
  })
  captureNumber: string | null;
  @ApiProperty({
    required: false,
    nullable: true,
  })
  permalink: string | null;
  @ApiProperty({
    required: false,
    nullable: true,
  })
  owner: string | null;
  @ApiProperty({
    required: false,
  })
  tags: string[];
}
