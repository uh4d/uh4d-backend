import {
  IsArray,
  IsBoolean,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { CreateObjectDateDto } from '../object-date/create-object-date.dto';
import { CreateOriginDto } from '../origin/create-origin.dto';
import { CreateLocationDto } from '../location/create-location.dto';
import { CreateObjectFileDto } from '../object-file/create-object-file.dto';

export class CreateObjectDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  name: string;
  @ApiProperty()
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => CreateObjectDateDto)
  date: CreateObjectDateDto;
  @ApiProperty()
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => CreateOriginDto)
  origin: CreateOriginDto;
  @ApiProperty()
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => CreateLocationDto)
  location: CreateLocationDto;
  @ApiProperty()
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => CreateObjectFileDto)
  file: CreateObjectFileDto;
  @ApiProperty({
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsBoolean()
  vrcity_forceTextures?: boolean;
  @ApiProperty({
    type: 'integer',
    format: 'int32',
  })
  @IsNotEmpty()
  @IsArray()
  @IsInt({ each: true })
  osm_overlap: number[];
}
