import { ApiProperty } from '@nestjs/swagger';
import { ObjectDateDto } from '../object-date/object-date.dto';
import { OriginDto } from '../origin/origin.dto';
import { LocationDto } from '../location/location.dto';
import { ObjectFileDto } from '../object-file/object-file.dto';

export class ObjectDto {
  @ApiProperty({
    required: false,
  })
  id: string;
  @ApiProperty({
    required: false,
  })
  name: string;
  @ApiProperty({
    required: false,
  })
  date: ObjectDateDto;
  @ApiProperty({
    required: false,
  })
  origin: OriginDto;
  @ApiProperty({
    required: false,
  })
  location: LocationDto;
  @ApiProperty({
    required: false,
  })
  file: ObjectFileDto;
  @ApiProperty({
    required: false,
    nullable: true,
  })
  vrcity_forceTextures: boolean | null;
  @ApiProperty({
    type: 'integer',
    format: 'int32',
    required: false,
  })
  osm_overlap: number[];
}
