import {
  IsArray,
  IsBoolean,
  IsInt,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { UpdateObjectDateDto } from '../object-date/update-object-date.dto';
import { UpdateOriginDto } from '../origin/update-origin.dto';
import { UpdateLocationDto } from '../location/update-location.dto';
import { UpdateObjectFileDto } from '../object-file/update-object-file.dto';

export class UpdateObjectDto {
  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsString()
  name?: string;
  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @ValidateNested()
  @Type(() => UpdateObjectDateDto)
  date?: UpdateObjectDateDto;
  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @ValidateNested()
  @Type(() => UpdateOriginDto)
  origin?: UpdateOriginDto;
  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @ValidateNested()
  @Type(() => UpdateLocationDto)
  location?: UpdateLocationDto;
  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @ValidateNested()
  @Type(() => UpdateObjectFileDto)
  file?: UpdateObjectFileDto;
  @ApiProperty({
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsBoolean()
  vrcity_forceTextures?: boolean | null;
  @ApiProperty({
    type: 'integer',
    format: 'int32',
    required: false,
  })
  @IsOptional()
  @IsArray()
  @IsInt({ each: true })
  osm_overlap?: number[];
}
