import { IsInt, IsOptional, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UpdateImageFileDto {
  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsString()
  path?: string;
  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsString()
  original?: string;
  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsString()
  preview?: string;
  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsString()
  thumb?: string;
  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsString()
  type?: string;
  @ApiProperty({
    type: 'integer',
    format: 'int32',
    required: false,
  })
  @IsOptional()
  @IsInt()
  width?: number;
  @ApiProperty({
    type: 'integer',
    format: 'int32',
    required: false,
  })
  @IsOptional()
  @IsInt()
  height?: number;
  @ApiProperty({
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  pkl?: string | null;
}
