import { ApiProperty } from '@nestjs/swagger';

export class ImageFileDto {
  @ApiProperty({
    required: false,
  })
  path: string;
  @ApiProperty({
    required: false,
  })
  original: string;
  @ApiProperty({
    required: false,
  })
  preview: string;
  @ApiProperty({
    required: false,
  })
  thumb: string;
  @ApiProperty({
    required: false,
  })
  type: string;
  @ApiProperty({
    type: 'integer',
    format: 'int32',
    required: false,
  })
  width: number;
  @ApiProperty({
    type: 'integer',
    format: 'int32',
    required: false,
  })
  height: number;
  @ApiProperty({
    required: false,
    nullable: true,
  })
  pkl: string | null;
}
