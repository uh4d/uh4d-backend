import { ApiProperty } from '@nestjs/swagger';

export class PoiDateDto {
  @ApiProperty({
    type: 'string',
    format: 'date-time',
    required: false,
    nullable: true,
  })
  from: Date | null;
  @ApiProperty({
    type: 'string',
    format: 'date-time',
    required: false,
    nullable: true,
  })
  to: Date | null;
  @ApiProperty({
    required: false,
  })
  objectBound: boolean;
}
