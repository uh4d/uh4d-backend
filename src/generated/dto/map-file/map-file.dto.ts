import { ApiProperty } from '@nestjs/swagger';

export class MapFileDto {
  @ApiProperty({
    required: false,
  })
  path: string;
}
