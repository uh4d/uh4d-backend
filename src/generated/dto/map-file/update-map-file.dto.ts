import { IsOptional, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UpdateMapFileDto {
  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsString()
  path?: string;
}
