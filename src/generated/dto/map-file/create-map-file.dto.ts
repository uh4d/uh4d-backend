import { IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateMapFileDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  path: string;
}
