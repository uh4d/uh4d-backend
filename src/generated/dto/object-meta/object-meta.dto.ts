import { ApiProperty } from '@nestjs/swagger';

export class ObjectMetaDto {
  @ApiProperty({
    required: false,
    nullable: true,
  })
  misc: string | null;
  @ApiProperty({
    required: false,
    nullable: true,
  })
  address: string | null;
  @ApiProperty({
    required: false,
  })
  formerAddress: string[];
  @ApiProperty({
    required: false,
  })
  tags: string[];
}
