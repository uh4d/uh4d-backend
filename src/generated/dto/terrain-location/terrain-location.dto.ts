import { ApiProperty } from '@nestjs/swagger';

export class TerrainLocationDto {
  @ApiProperty({
    type: 'number',
    format: 'float',
    required: false,
  })
  latitude: number;
  @ApiProperty({
    type: 'number',
    format: 'float',
    required: false,
  })
  longitude: number;
  @ApiProperty({
    type: 'number',
    format: 'float',
    required: false,
  })
  altitude: number;
  @ApiProperty({
    type: 'number',
    format: 'float',
    required: false,
  })
  west: number;
  @ApiProperty({
    type: 'number',
    format: 'float',
    required: false,
  })
  east: number;
  @ApiProperty({
    type: 'number',
    format: 'float',
    required: false,
  })
  north: number;
  @ApiProperty({
    type: 'number',
    format: 'float',
    required: false,
  })
  south: number;
}
