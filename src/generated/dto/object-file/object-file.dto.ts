import { ApiProperty } from '@nestjs/swagger';

export class ObjectFileDto {
  @ApiProperty({
    required: false,
  })
  path: string;
  @ApiProperty({
    required: false,
  })
  file: string;
  @ApiProperty({
    required: false,
  })
  original: string;
  @ApiProperty({
    required: false,
  })
  type: string;
}
