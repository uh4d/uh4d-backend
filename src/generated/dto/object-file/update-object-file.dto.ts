import { IsOptional, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UpdateObjectFileDto {
  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsString()
  path?: string;
  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsString()
  file?: string;
  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsString()
  original?: string;
  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsString()
  type?: string;
}
