import { IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateObjectFileDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  path: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  file: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  original: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  type: string;
}
