import { ApiProperty } from '@nestjs/swagger';

export class OriginDto {
  @ApiProperty({
    type: 'number',
    format: 'float',
    required: false,
  })
  latitude: number;
  @ApiProperty({
    type: 'number',
    format: 'float',
    required: false,
  })
  longitude: number;
  @ApiProperty({
    type: 'number',
    format: 'float',
    required: false,
  })
  altitude: number;
  @ApiProperty({
    type: 'number',
    format: 'float',
    required: false,
  })
  omega: number;
  @ApiProperty({
    type: 'number',
    format: 'float',
    required: false,
  })
  phi: number;
  @ApiProperty({
    type: 'number',
    format: 'float',
    required: false,
  })
  kappa: number;
  @ApiProperty({
    type: 'number',
    format: 'float',
    required: false,
  })
  scale: number[];
}
