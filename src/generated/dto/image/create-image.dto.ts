import {
  IsIn,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { CreateImageDateDto } from '../image-date/create-image-date.dto';
import { CreateImageFileDto } from '../image-file/create-image-file.dto';
import { CreateCameraDto } from '../camera/create-camera.dto';

export class CreateImageDto {
  @ApiProperty({
    description: 'Image title',
  })
  @IsNotEmpty()
  @IsString()
  title: string;
  @ApiProperty({
    description: 'Name of the photographer',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  author?: string;
  @ApiProperty({
    description: 'Temporal informatino defining time-span',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @ValidateNested()
  @Type(() => CreateImageDateDto)
  date?: CreateImageDateDto;
  @ApiProperty({
    description: 'File reference information',
  })
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => CreateImageFileDto)
  file: CreateImageFileDto;
  @ApiProperty({
    description:
      'Exterior and interior camera parameters (spatial information)',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @ValidateNested()
  @Type(() => CreateCameraDto)
  camera?: CreateCameraDto;
  @ApiProperty({
    description:
      'Status of spatialization:\n\n* 0 - Not yet spatialized\n* 1 - Spatialization not possible\n* 2 - Spatialization may be possible in future\n* 3 - Position (lat,lon) is known, but no orientation\n* 4 - Manually spatialized\n* 5 - Spatialized by automatic pipeline',
    type: 'integer',
    format: 'int32',
  })
  @IsNotEmpty()
  @IsInt()
  @IsIn([0, 1, 2, 3, 4, 5])
  spatialStatus: number;
}
