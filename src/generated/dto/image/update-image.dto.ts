import {
  IsIn,
  IsInt,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { UpdateImageDateDto } from '../image-date/update-image-date.dto';
import { UpdateImageFileDto } from '../image-file/update-image-file.dto';
import { UpdateCameraDto } from '../camera/update-camera.dto';

export class UpdateImageDto {
  @ApiProperty({
    description: 'Image title',
    required: false,
  })
  @IsOptional()
  @IsString()
  title?: string;
  @ApiProperty({
    description: 'Name of the photographer',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  author?: string | null;
  @ApiProperty({
    description: 'Temporal informatino defining time-span',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @ValidateNested()
  @Type(() => UpdateImageDateDto)
  date?: UpdateImageDateDto | null;
  @ApiProperty({
    description: 'File reference information',
    required: false,
  })
  @IsOptional()
  @ValidateNested()
  @Type(() => UpdateImageFileDto)
  file?: UpdateImageFileDto;
  @ApiProperty({
    description:
      'Exterior and interior camera parameters (spatial information)',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @ValidateNested()
  @Type(() => UpdateCameraDto)
  camera?: UpdateCameraDto | null;
  @ApiProperty({
    description:
      'Status of spatialization:\n\n* 0 - Not yet spatialized\n* 1 - Spatialization not possible\n* 2 - Spatialization may be possible in future\n* 3 - Position (lat,lon) is known, but no orientation\n* 4 - Manually spatialized\n* 5 - Spatialized by automatic pipeline',
    type: 'integer',
    format: 'int32',
    required: false,
  })
  @IsOptional()
  @IsInt()
  @IsIn([0, 1, 2, 3, 4, 5])
  spatialStatus?: number;
}
