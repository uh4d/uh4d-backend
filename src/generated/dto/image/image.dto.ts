import { ApiProperty } from '@nestjs/swagger';
import { ImageDateDto } from '../image-date/image-date.dto';
import { ImageFileDto } from '../image-file/image-file.dto';
import { CameraDto } from '../camera/camera.dto';

export class ImageDto {
  @ApiProperty({
    required: false,
  })
  id: string;
  @ApiProperty({
    description: 'Image title',
    required: false,
  })
  title: string;
  @ApiProperty({
    description: 'Name of the photographer',
    required: false,
    nullable: true,
  })
  author: string | null;
  @ApiProperty({
    description: 'Temporal informatino defining time-span',
    required: false,
    nullable: true,
  })
  date: ImageDateDto | null;
  @ApiProperty({
    description: 'File reference information',
    required: false,
  })
  file: ImageFileDto;
  @ApiProperty({
    description:
      'Exterior and interior camera parameters (spatial information)',
    required: false,
    nullable: true,
  })
  camera: CameraDto | null;
  @ApiProperty({
    description:
      'Status of spatialization:\n\n* 0 - Not yet spatialized\n* 1 - Spatialization not possible\n* 2 - Spatialization may be possible in future\n* 3 - Position (lat,lon) is known, but no orientation\n* 4 - Manually spatialized\n* 5 - Spatialized by automatic pipeline',
    type: 'integer',
    format: 'int32',
    required: false,
  })
  spatialStatus: number;
}
