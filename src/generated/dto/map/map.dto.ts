import { ApiProperty } from '@nestjs/swagger';
import { MapFileDto } from '../map-file/map-file.dto';
import { LocationDto } from '../location/location.dto';

export class MapDto {
  @ApiProperty({
    required: false,
  })
  id: string;
  @ApiProperty({
    description: 'Year of the map',
    required: false,
  })
  date: string;
  @ApiProperty({
    description: 'File reference information',
    required: false,
  })
  file: MapFileDto;
  @ApiProperty({
    description: 'Geographic position of the scene',
    required: false,
  })
  location: LocationDto;
}
