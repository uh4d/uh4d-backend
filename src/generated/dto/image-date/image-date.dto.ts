import { ApiProperty } from '@nestjs/swagger';

export class ImageDateDto {
  @ApiProperty({
    required: false,
  })
  value: string;
  @ApiProperty({
    type: 'string',
    format: 'date-time',
    required: false,
  })
  from: Date;
  @ApiProperty({
    type: 'string',
    format: 'date-time',
    required: false,
  })
  to: Date;
  @ApiProperty({
    required: false,
  })
  display: string;
}
