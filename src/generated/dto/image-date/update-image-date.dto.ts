import { IsDateString, IsOptional, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UpdateImageDateDto {
  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsString()
  value?: string;
  @ApiProperty({
    type: 'string',
    format: 'date-time',
    required: false,
  })
  @IsOptional()
  @IsDateString()
  from?: Date;
  @ApiProperty({
    type: 'string',
    format: 'date-time',
    required: false,
  })
  @IsOptional()
  @IsDateString()
  to?: Date;
  @ApiProperty({
    required: false,
  })
  @IsOptional()
  @IsString()
  display?: string;
}
