import { IsDateString, IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateImageDateDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  value: string;
  @ApiProperty({
    type: 'string',
    format: 'date-time',
  })
  @IsNotEmpty()
  @IsDateString()
  from: Date;
  @ApiProperty({
    type: 'string',
    format: 'date-time',
  })
  @IsNotEmpty()
  @IsDateString()
  to: Date;
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  display: string;
}
