import { IsOptional, IsString, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { UpdateTerrainLocationDto } from '../terrain-location/update-terrain-location.dto';
import { UpdateTerrainFileDto } from '../terrain-file/update-terrain-file.dto';

export class UpdateTerrainDto {
  @ApiProperty({
    description: 'Terrain/place appellation',
    required: false,
  })
  @IsOptional()
  @IsString()
  name?: string;
  @ApiProperty({
    description:
      'Geographic position of the center of the terrain. `west`, `east`, `north`, and `south` describe the boundaries of the terrain.',
    required: false,
  })
  @IsOptional()
  @ValidateNested()
  @Type(() => UpdateTerrainLocationDto)
  location?: UpdateTerrainLocationDto;
  @ApiProperty({
    description: 'File reference information',
    required: false,
  })
  @IsOptional()
  @ValidateNested()
  @Type(() => UpdateTerrainFileDto)
  file?: UpdateTerrainFileDto;
}
