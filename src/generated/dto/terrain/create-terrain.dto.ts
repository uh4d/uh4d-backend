import { IsNotEmpty, IsString, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { CreateTerrainLocationDto } from '../terrain-location/create-terrain-location.dto';
import { CreateTerrainFileDto } from '../terrain-file/create-terrain-file.dto';

export class CreateTerrainDto {
  @ApiProperty({
    description: 'Terrain/place appellation',
  })
  @IsNotEmpty()
  @IsString()
  name: string;
  @ApiProperty({
    description:
      'Geographic position of the center of the terrain. `west`, `east`, `north`, and `south` describe the boundaries of the terrain.',
  })
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => CreateTerrainLocationDto)
  location: CreateTerrainLocationDto;
  @ApiProperty({
    description: 'File reference information',
  })
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => CreateTerrainFileDto)
  file: CreateTerrainFileDto;
}
