import { ApiProperty } from '@nestjs/swagger';
import { TerrainLocationDto } from '../terrain-location/terrain-location.dto';
import { TerrainFileDto } from '../terrain-file/terrain-file.dto';

export class TerrainEntity {
  @ApiProperty({
    required: false,
  })
  id: string;
  @ApiProperty({
    description: 'Terrain/place appellation',
    required: false,
  })
  name: string;
  @ApiProperty({
    description:
      'Geographic position of the center of the terrain. `west`, `east`, `north`, and `south` describe the boundaries of the terrain.',
    required: false,
  })
  location: TerrainLocationDto;
  @ApiProperty({
    description: 'File reference information',
    required: false,
  })
  file: TerrainFileDto;
}
