import {
  IsArray,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { CreateLocationDto } from '../location/create-location.dto';
import { CreatePoiDateDto } from '../poi-date/create-poi-date.dto';

export class CreateUh4dPoiDto {
  @ApiProperty({
    description: 'Point of Interest title',
  })
  @IsNotEmpty()
  @IsString()
  title: string;
  @ApiProperty({
    description:
      'Any text. Can be (but not only) a single link or a Markdown formatted text.',
  })
  @IsNotEmpty()
  @IsString()
  content: string;
  @ApiProperty({
    description: 'Spatial information',
  })
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => CreateLocationDto)
  location: CreateLocationDto;
  @ApiProperty({
    description:
      'Type of the point of interest (refers to the source/origin), e.g., `uh4d`, `wikidata`',
  })
  @IsNotEmpty()
  @IsString()
  type: string;
  @ApiProperty({
    description:
      'Date boundaries in which the point of interest should be visible',
  })
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => CreatePoiDateDto)
  date: CreatePoiDateDto;
  @ApiProperty({
    description: 'ID of object the point of interest is attached to',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  objectId?: string;
  @ApiProperty({
    description:
      'Position of preferred perspective to point of interest in scene coordinates',
    minItems: 3,
    maxItems: 3,
    type: 'number',
    format: 'float',
  })
  @IsNotEmpty()
  @IsArray()
  @IsNumber({}, { each: true })
  camera: number[];
  @ApiProperty({
    description: 'List of damage factors according to preservation order',
  })
  @IsNotEmpty()
  @IsArray()
  @IsString({ each: true })
  damageFactors: string[];
}
