import {
  IsArray,
  IsNumber,
  IsOptional,
  IsString,
  ValidateNested,
} from 'class-validator';
import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { UpdateLocationDto } from '../location/update-location.dto';
import { UpdatePoiDateDto } from '../poi-date/update-poi-date.dto';

export class UpdateUh4dPoiDto {
  @ApiProperty({
    description: 'Point of Interest title',
    required: false,
  })
  @IsOptional()
  @IsString()
  title?: string;
  @ApiProperty({
    description:
      'Any text. Can be (but not only) a single link or a Markdown formatted text.',
    required: false,
  })
  @IsOptional()
  @IsString()
  content?: string;
  @ApiProperty({
    description: 'Spatial information',
    required: false,
  })
  @IsOptional()
  @ValidateNested()
  @Type(() => UpdateLocationDto)
  location?: UpdateLocationDto;
  @ApiProperty({
    description:
      'Type of the point of interest (refers to the source/origin), e.g., `uh4d`, `wikidata`',
    required: false,
  })
  @IsOptional()
  @IsString()
  type?: string;
  @ApiProperty({
    description:
      'Date boundaries in which the point of interest should be visible',
    required: false,
  })
  @IsOptional()
  @ValidateNested()
  @Type(() => UpdatePoiDateDto)
  date?: UpdatePoiDateDto;
  @ApiProperty({
    description: 'ID of object the point of interest is attached to',
    required: false,
    nullable: true,
  })
  @IsOptional()
  @IsString()
  objectId?: string | null;
  @ApiProperty({
    description:
      'Position of preferred perspective to point of interest in scene coordinates',
    minItems: 3,
    maxItems: 3,
    type: 'number',
    format: 'float',
    required: false,
  })
  @IsOptional()
  @IsArray()
  @IsNumber({}, { each: true })
  camera?: number[];
  @ApiProperty({
    description: 'List of damage factors according to preservation order',
    required: false,
  })
  @IsOptional()
  @IsArray()
  @IsString({ each: true })
  damageFactors?: string[];
}
