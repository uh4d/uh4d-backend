import { ApiProperty } from '@nestjs/swagger';
import { LocationDto } from '../location/location.dto';
import { PoiDateDto } from '../poi-date/poi-date.dto';

export class Uh4dPoiEntity {
  @ApiProperty({
    required: false,
  })
  id: string;
  @ApiProperty({
    description: 'Point of Interest title',
    required: false,
  })
  title: string;
  @ApiProperty({
    description:
      'Any text. Can be (but not only) a single link or a Markdown formatted text.',
    required: false,
  })
  content: string;
  @ApiProperty({
    description: 'Spatial information',
    required: false,
  })
  location: LocationDto;
  @ApiProperty({
    description:
      'Type of the point of interest (refers to the source/origin), e.g., `uh4d`, `wikidata`',
    required: false,
  })
  type: string;
  @ApiProperty({
    description:
      'Date boundaries in which the point of interest should be visible',
    required: false,
  })
  date: PoiDateDto;
  @ApiProperty({
    description: 'ID of object the point of interest is attached to',
    required: false,
    nullable: true,
  })
  objectId: string | null;
  @ApiProperty({
    description:
      'Position of preferred perspective to point of interest in scene coordinates',
    minItems: 3,
    maxItems: 3,
    type: 'number',
    format: 'float',
    required: false,
  })
  camera: number[];
  @ApiProperty({
    description: 'List of damage factors according to preservation order',
    required: false,
  })
  damageFactors: string[];
}
