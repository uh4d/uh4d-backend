import { ApiProperty } from '@nestjs/swagger';
import { LocationDto } from '../location/location.dto';

export class GeneralPoiEntity {
  @ApiProperty({
    required: false,
  })
  id: string;
  @ApiProperty({
    description: 'Point of Interest title',
    required: false,
  })
  title: string;
  @ApiProperty({
    description:
      'Any text. Can be (but not only) a single link or a Markdown formatted text.',
    required: false,
  })
  content: string;
  @ApiProperty({
    description: 'Spatial information',
    required: false,
  })
  location: LocationDto;
  @ApiProperty({
    description:
      'Type of the point of interest (refers to the source/origin), e.g., `uh4d`, `wikidata`',
    required: false,
  })
  type: string;
}
