import { IsNotEmpty, IsString, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { ApiProperty } from '@nestjs/swagger';
import { CreateLocationDto } from '../location/create-location.dto';

export class CreateGeneralPoiDto {
  @ApiProperty({
    description: 'Point of Interest title',
  })
  @IsNotEmpty()
  @IsString()
  title: string;
  @ApiProperty({
    description:
      'Any text. Can be (but not only) a single link or a Markdown formatted text.',
  })
  @IsNotEmpty()
  @IsString()
  content: string;
  @ApiProperty({
    description: 'Spatial information',
  })
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => CreateLocationDto)
  location: CreateLocationDto;
  @ApiProperty({
    description:
      'Type of the point of interest (refers to the source/origin), e.g., `uh4d`, `wikidata`',
  })
  @IsNotEmpty()
  @IsString()
  type: string;
}
