import { INestApplication, Injectable } from '@nestjs/common';
import { DocumentBuilder, OpenAPIObject, SwaggerModule } from '@nestjs/swagger';
import { ConfigService } from '@nestjs/config';
import { embedDocFile, getPkgJson } from '@utils/ref-file';

@Injectable()
export class SwaggerConfig {
  constructor(private readonly config: ConfigService) {}

  getDocumentConfig() {
    return new DocumentBuilder()
      .setTitle('UH4D Backend API')
      .setDescription(embedDocFile('intro.md'))
      .setVersion(getPkgJson().version)
      .addServer(this.config.get<string>('host') + '/api')
      .addTag('Images', embedDocFile('image.md'))
      .addTag('Objects', embedDocFile('object.md'))
      .addTag('Terrain', 'Terrain specific requests.')
      .addTag(
        'Maps',
        'Requests specific to (historical) maps (map tiles), only works with custom terrain models.',
      )
      .addTag('Points of Interest', embedDocFile('poi.md'))
      .addTag(
        'Metadata',
        'The following are mainly used to query entities for suggestions/typeahead lists.',
      )
      .addTag('Misc', 'Other various requests.')
      .addTag('Logs', 'Logging (currently only used with VRCity app).')
      .build();
  }

  createDocument(app: INestApplication): OpenAPIObject {
    return SwaggerModule.createDocument(app, this.getDocumentConfig());
  }
}
