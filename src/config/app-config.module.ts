import { Module } from '@nestjs/common';
import { ServeStaticModule } from '@nestjs/serve-static';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { Neo4jModule } from '@brakebein/nest-neo4j';
import { join } from 'node:path';
import { SwaggerConfig } from './swagger.config';
import { RedocConfig } from './redoc.config';
import configuration from './configuration';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [configuration],
    }),
    ServeStaticModule.forRootAsync({
      useFactory: (config: ConfigService) => [
        {
          rootPath: config.get<string>('path.data'),
          serveRoot: '/data',
          exclude: ['data/batch'],
        },
        {
          rootPath: join(__dirname, '..', 'assets'),
          serveRoot: '/api/assets',
        },
      ],
      inject: [ConfigService],
    }),
    Neo4jModule.forRootAsync({
      useFactory: (config: ConfigService) => ({
        scheme: config.get('neo4j.scheme'),
        host: config.get<string>('neo4j.host'),
        port: config.get<string>('neo4j.port'),
        username: config.get<string>('neo4j.username'),
        password: config.get<string>('neo4j.password'),
        database: config.get<string>('neo4j.database'),
      }),
      inject: [ConfigService],
    }),
  ],
  providers: [SwaggerConfig, RedocConfig],
  exports: [
    ConfigModule,
    ServeStaticModule,
    Neo4jModule,
    SwaggerConfig,
    RedocConfig,
  ],
})
export class AppConfigModule {}
