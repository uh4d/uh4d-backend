import { readFileSync } from 'node:fs';
import { join } from 'node:path';
import * as yaml from 'js-yaml';

export default () => {
  return yaml.load(readFileSync(join(__dirname, '../../config.yaml'), 'utf8'));
};
