import {
  ExtrudeBufferGeometry,
  Mesh,
  MeshBasicMaterial,
  Path,
  Shape,
  Vector2,
} from 'three';
import {
  OsmNode,
  OsmRelation,
  OsmWay,
  OverpassDto,
} from '@routes/overpass/dto/overpass.dto';
import { Coordinates } from '@utils/coordinates';

/**
 * Generate meshes from Overpass OSM data.
 */
export function processOsmData(
  result: OverpassDto,
  origin: Coordinates,
): Mesh<ExtrudeBufferGeometry, MeshBasicMaterial>[] {
  const nodes = result.elements.filter((e): e is OsmNode => e.type === 'node');
  const ways = result.elements.filter((e): e is OsmWay => e.type === 'way');
  const relations = result.elements.filter(
    (e): e is OsmRelation => e.type === 'relation',
  );

  const buildings: Mesh<ExtrudeBufferGeometry, MeshBasicMaterial>[] = [];

  const processWay = (way: OsmWay, role: string): Shape | Path => {
    const points: Vector2[] = [];
    way.nodes.forEach((nodeId) => {
      const node = nodes.find((n) => n.id === nodeId);
      if (node) {
        const p = new Coordinates(node.lat, node.lon, 0).toCartesian(origin);
        points.push(new Vector2(p.x, -p.z));
      }
    });

    switch (role) {
      case 'outer':
        return new Shape(points);
      case 'inner':
        return new Path(points);
      default:
        return new Shape(points);
    }
  };

  ways.forEach((way) => {
    if (!way.tags) return;

    const shape = processWay(way, 'part');
    const mesh = generateBuilding(shape as Shape);
    mesh.userData.osmId = way.id;
    buildings.push(mesh);
  });

  relations.forEach((rel) => {
    rel.members.sort((a) => {
      if (a.role === 'outer') return -1;
      if (a.role === 'inner') return 1;
      return 0;
    });

    const wayPaths: (Shape | Path)[] = [];
    rel.members.forEach((m) => {
      const way = ways.find((w) => w.id === m.ref);
      if (way) {
        wayPaths.push(processWay(way, m.role));
      }
    });

    const shapes: Shape[] = [];
    wayPaths.forEach((p) => {
      if (p instanceof Shape) {
        shapes.push(p);
      } else {
        shapes[shapes.length - 1].holes.push(p);
      }
    });

    shapes.forEach((s) => {
      const mesh = generateBuilding(s);
      mesh.userData.osmId = rel.id;
      buildings.push(mesh);
    });
  });

  return buildings;
}

function generateBuilding(
  shape: Shape,
): Mesh<ExtrudeBufferGeometry, MeshBasicMaterial> {
  const geo = new ExtrudeBufferGeometry(shape, {
    depth: 1,
    bevelEnabled: false,
  });

  geo.rotateX(-Math.PI);

  return new Mesh(geo, new MeshBasicMaterial());
}
