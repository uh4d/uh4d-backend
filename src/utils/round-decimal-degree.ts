/**
 * Round decimal degree value by approximate precision value in meters.
 * @param degree Decimal degree (lat, lon)
 * @param precision Precision in meters
 */
export function roundDecimalDegree(degree: number, precision = 50): number {
  // https://gis.stackexchange.com/questions/8650/measuring-accuracy-of-latitude-and-longitude
  const p10 = Math.pow(10, Math.round(precision).toString(10).length);
  let factor = Math.round(p10 / precision);
  const factorLength = Math.round(p10 / factor).toString(10).length;
  const decimalPlace = Math.max(1, 6 - factorLength);

  factor = factor * Math.pow(10, decimalPlace);

  return parseFloat(
    (Math.round(degree * factor) / factor).toFixed(decimalPlace),
  );
}
