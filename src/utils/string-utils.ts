/**
 * Replace special RegExp characters.
 */
export function escapeRegex(str: string): string {
  // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions#escaping
  return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}

/**
 * Replace unusual characters with `_`.
 */
export function replace(str: string): string;
export function replace(str: any): any;
export function replace(str: any): any {
  if (typeof str !== 'string') return str;
  return str.replace(/[^a-zA-Z0-9_\-.]/g, '_');
}
