import { Logger } from '@nestjs/common';
import {
  Box2,
  EdgesGeometry,
  ExtrudeGeometry,
  Group,
  LineBasicMaterial,
  LineSegments,
  Mesh,
  MeshBasicMaterial,
  Object3D,
  Path,
  Raycaster,
  Shape,
  Vector2,
  Vector3,
} from 'three';
import { getOSMBuildingHeight } from './osm-building-heights';
import { Coordinates } from '@utils/coordinates';

interface INode {
  type: 'node';
  id: number;
  lat: number;
  lon: number;
}

type ITags = { [key: string]: string };

interface IMember {
  type: string;
  ref: number;
  role: 'outer' | 'inner';
}

interface IWay {
  type: 'way';
  id: number;
  nodes: number[];
  tags?: ITags;
}

interface IRelation {
  type: 'relation';
  id: number;
  members: IMember[];
  tags: ITags;
}

/**
 * Class to generate and organize 3D objects retrieved from OpenStreetMap (via Overpass API).
 */
export class OSMGenerator {
  private readonly logger = new Logger(OSMGenerator.name);

  private nodes: INode[] = [];
  private ways: IWay[] = [];
  private relations: IRelation[] = [];

  private readonly origin: Coordinates;
  private readonly includeEdges: boolean = false;

  group = new Group();
  buildings: Object3D[] = [];

  constructor(origin: Coordinates, edges = false) {
    this.origin = origin;
    this.includeEdges = edges;
  }

  /**
   * Generate buildings from data from Overpass API.
   */
  async generate(data: {
    elements: (INode | IWay | IRelation)[];
  }): Promise<void> {
    this.dispose();

    // split different types of elements
    this.nodes = data.elements.filter((e): e is INode => e.type === 'node');
    this.ways = data.elements.filter((e): e is IWay => e.type === 'way');
    this.relations = data.elements.filter(
      (e): e is IRelation => e.type === 'relation',
    );

    const buildings: Object3D[] = [];

    // process ways
    for (const way of this.ways) {
      if (!way.tags) continue;

      const existingBuilding = this.buildings.find(
        (obj) => obj.userData.osmId === way.id,
      );
      if (existingBuilding) {
        buildings.push(existingBuilding);
        continue;
      }

      const shape = this.processWay(way, 'part');

      const group = this.generateBuilding(shape, way.tags);
      group.userData.osmId = way.id;
      buildings.push(group);
    }

    // process relations
    for (const rel of this.relations) {
      const existingBuilding = this.buildings.find(
        (obj) => obj.userData.osmId === rel.id,
      );
      if (existingBuilding) {
        buildings.push(existingBuilding);
        continue;
      }

      rel.members.sort((a, b) => {
        if (a.role === 'outer') return -1;
        if (a.role === 'inner') return 1;
        return 0;
      });

      const wayPaths: (Shape | Path)[] = [];
      for (const m of rel.members) {
        const way = this.ways.find((w) => w.id === m.ref);
        if (way) {
          wayPaths.push(this.processWay(way, m.role));
        }
      }

      const shapes: Shape[] = [];
      for (const p of wayPaths) {
        if (p instanceof Shape) {
          shapes.push(p);
        } else {
          shapes[shapes.length - 1].holes.push(p);
        }
      }

      for (const s of shapes) {
        const group = this.generateBuilding(s, rel.tags);
        group.userData.osmId = rel.id;
        buildings.push(group);
      }
    }

    // dispose obsolete buildings
    const obsoleteBuildings = this.buildings.filter(
      (obj) => !buildings.includes(obj),
    );
    for (const obj of obsoleteBuildings) {
      this.group.remove(obj);
      obj.traverse((child) => {
        if (child instanceof Mesh || child instanceof LineSegments) {
          child.geometry.dispose();
        }
      });
    }

    this.buildings = buildings;
  }

  /**
   * Build shape from way element's nodes.
   * @private
   */
  private processWay(way: IWay, role: 'outer' | 'part'): Shape;
  private processWay(way: IWay, role: 'inner'): Path;
  private processWay(way: IWay, role: 'inner' | 'outer' | 'part'): Path | Shape;
  private processWay(
    way: IWay,
    role: 'inner' | 'outer' | 'part',
  ): Path | Shape {
    const points: Vector2[] = [];

    for (const nodeId of way.nodes) {
      const node = this.nodes.find((n) => n.id === nodeId);
      if (node) {
        const p = new Coordinates(node.lat, node.lon, 0).toCartesian(
          this.origin,
        );
        points.push(new Vector2(p.x, -p.z));
      }
    }

    switch (role) {
      case 'outer':
        return new Shape(points);
      case 'inner':
        return new Path(points);
      default:
        return new Shape(points);
    }
  }

  /**
   * Extrude 3D geometry from shape and generate edges.
   * @private
   */
  private generateBuilding(shape: Shape, tags: ITags): Object3D {
    const bbox = new Box2();
    shape.getPoints().forEach((p) => {
      bbox.expandByPoint(p);
    });

    const geo = new ExtrudeGeometry(shape, {
      depth: getOSMBuildingHeight(bbox, tags),
      bevelEnabled: false,
    });
    geo.rotateX(-Math.PI / 2);

    const mesh = new Mesh(geo, new MeshBasicMaterial());

    const group = new Group();
    group.add(mesh);

    if (this.includeEdges) {
      const edges = new LineSegments(
        new EdgesGeometry(geo, 24),
        new LineBasicMaterial(),
      );
      group.add(edges);
    }

    group.userData.points = shape.getPoints();
    group.userData.boundingBox = bbox;

    return group;
  }

  /**
   * Update visibility and position according to terrain and existing objects.
   */
  update(terrain: Object3D): void {
    this.group.remove(...this.buildings);

    const raycaster = new Raycaster();
    const dir = new Vector3(0, -1, 0);

    const startTime = Date.now();

    for (const building of this.buildings) {
      const bbox: Box2 = building.userData.boundingBox;
      const points = [
        bbox.min,
        new Vector2(bbox.min.x, bbox.max.y),
        bbox.max,
        new Vector2(bbox.max.x, bbox.min.y),
      ];

      let height = Number.MAX_VALUE;

      for (const point of points) {
        raycaster.set(new Vector3(point.x, 10000, -point.y), dir);
        const intersections = raycaster.intersectObject(terrain, true);

        if (intersections[0]) {
          height = Math.min(height, intersections[0].point.y);
        }
      }

      building.position.setY(height === Number.MAX_VALUE ? 0 : height);

      this.group.add(building);
    }

    this.logger.log(
      `OSM Generator - Elapsed time: ${(Date.now() - startTime) / 1000}`,
    );
  }

  /**
   * Dispose all generated objects.
   */
  dispose(): void {
    for (const obj of this.buildings) {
      this.group.remove(obj);
      obj.traverse((child) => {
        if (child instanceof Mesh || child instanceof LineSegments) {
          child.geometry.dispose();
          const mats = Array.isArray(child.material)
            ? child.material
            : [child.material];
          mats.forEach((mat) => {
            if (mat.map) mat.map.dispose();
            mat.dispose();
          });
        }
      });
    }
  }
}
