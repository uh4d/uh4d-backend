import { Box2, Vector2 } from 'three';

/**
 * Determine building height from tags.
 */
export function getOSMBuildingHeight(
  boundingBox: Box2,
  tags?: { [key: string]: string },
): number {
  if (!tags) return approxHeight(boundingBox);

  if (tags.height) {
    return parseInt(tags.height, 10);
  } else if (tags['building:levels']) {
    return parseInt(tags['building:levels'], 10) * 4;
  } else if (tags.building && heights.hasOwnProperty(tags.building)) {
    const [min, max] = heights[tags.building];

    return Math.random() * (max - min) + min;
  } else {
    return approxHeight(boundingBox);
  }
}

function approxHeight(boundingBox: Box2): number {
  // guess some random height based on the approximate area
  const { width, height } = boundingBox.getSize(new Vector2());
  const max = Math.min(width, height, 25);
  const min = Math.min(max / 2, 10);

  return Math.random() * (max - min) + min;
}

/**
 * List of building tags and height range.
 * https://wiki.openstreetmap.org/wiki/Key:building
 */
const heights: { [key: string]: [number, number] } = {
  // accommodations
  apartments: [20, 30],
  bungalow: [3, 5],
  cabin: [3, 5],
  detached: [5, 10],
  dormitory: [10, 20],
  farm: [5, 10],
  ger: [3, 4],
  hotel: [10, 20],
  house: [5, 10],
  houseboat: [3, 5],
  residential: [10, 20],
  semidetached_house: [5, 10],
  static_caravan: [3, 5],
  terrace: [5, 10],
  // commercial
  commercial: [5, 20],
  industrial: [10, 30],
  kiosk: [3, 5],
  office: [10, 30],
  retail: [10, 30],
  supermarket: [6, 20],
  warehouse: [20, 30],
  // religious
  cathedral: [30, 100],
  chapel: [5, 10],
  church: [10, 50],
  monastery: [10, 20],
  mosque: [10, 30],
  presbytery: [5, 10],
  religious: [10, 30],
  shrine: [3, 5],
  synagogue: [10, 30],
  temple: [15, 30],
  // civic
  bakehouse: [5, 10],
  civic: [10, 20],
  fire_station: [8, 15],
  government: [20, 30],
  hospital: [10, 40],
  public: [10, 40],
  toilets: [3, 5],
  train_station: [8, 25],
  transportation: [10, 20],
  kindergarten: [10, 15],
  school: [10, 30],
  university: [15, 30],
  college: [10, 30],
  // agricultural
  barn: [10, 20],
  conservatory: [8, 15],
  cowshed: [5, 8],
  farm_auxiliary: [5, 15],
  greenhouse: [5, 10],
  slurry_tank: [3, 5],
  stable: [5, 10],
  sty: [3, 5],
  // sports
  grandstand: [5, 15],
  pavillon: [5, 10],
  riding_hall: [8, 15],
  sports_hall: [10, 20],
  stadium: [8, 25],
  // storage
  hangar: [5, 15],
  hut: [3, 5],
  shed: [3, 5],
  // cars
  carport: [2, 3],
  garage: [2, 4],
  garages: [2, 4],
  parking: [5, 20],
  // power
  digester: [5, 10],
  service: [3, 5],
  transformer_tower: [8, 15],
  water_tower: [10, 25],
  // other
  container: [2, 3],
  ruins: [1, 2],
};
