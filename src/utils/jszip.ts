import * as JSZip from 'jszip';
import { createWriteStream, readFile } from 'fs-extra';

/**
 * Open a Zip file.
 */
export async function openZipFile(path: string): Promise<JSZip> {
  const data = await readFile(path);
  return JSZip.loadAsync(data);
}

/**
 * Extract a file from a zip object.
 * @param zip Zip object
 * @param outFile Path to where the file will be stored
 */
export function extractFileFromZip(
  zip: JSZip.JSZipObject,
  outFile: string,
): Promise<void> {
  return new Promise((resolve, reject) => {
    zip
      .nodeStream()
      .pipe(createWriteStream(outFile))
      .on('finish', resolve)
      .on('error', reject);
  });
}
