import { createStream } from 'byline';
import { createReadStream, createWriteStream, remove, rename } from 'fs-extra';

/**
 * Read file line by line and look for patterns defined by a regular expression.
 * If a match is found, the callback is triggered, the string returned replaces
 * the original string in this line.
 */
export async function findAndReplaceLine(
  file: string,
  regex: RegExp,
  callback: (matches: RegExpExecArray) => string,
): Promise<void> {
  const tempFile = file + '.tmp';
  const readStream = createReadStream(file);
  const writeStream = createWriteStream(tempFile);
  const lr = createStream(readStream, { keepEmptyLines: true });

  return new Promise((resolve, reject) => {
    lr.on('error', (err) => {
      writeStream.end(async () => {
        await remove(tempFile);
        reject(err);
      });
    });

    writeStream.on('error', async (err) => {
      readStream.destroy();
      await remove(tempFile);
      reject(err);
    });

    lr.on('data', (line) => {
      const matches = regex.exec(line);
      if (matches) {
        const replaced = callback(matches);
        writeStream.write(`${replaced}\n`);
      } else {
        writeStream.write(`${line}\n`);
      }
    });

    lr.on('end', () => {
      writeStream.end(async () => {
        try {
          await remove(file);
          await rename(tempFile, file);
          resolve();
        } catch (e) {
          reject(e);
        }
      });
    });
  });
}
