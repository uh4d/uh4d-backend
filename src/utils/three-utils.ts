import {
  Box3,
  BufferGeometry,
  LineSegments,
  Matrix4,
  Mesh,
  Raycaster,
  Vector3,
} from 'three';
import { loadGltf } from 'node-three-gltf';

/**
 * Compute center of 3D model in local Cartesian coordinates.
 * @param path Absolute path to gltf file
 * @param matrix
 */
export async function computeModelCenter(
  path: string,
  matrix = new Matrix4(),
): Promise<Vector3> {
  // load 3D model
  const gltf = await loadGltf(path);
  const obj = gltf.scene.children[0];

  const mat4 =
    matrix instanceof Matrix4 ? matrix : new Matrix4().fromArray(matrix);
  obj.applyMatrix4(mat4);
  obj.matrixAutoUpdate = false;

  // compute center
  const bbox = new Box3();
  bbox.expandByObject(obj);
  const center = bbox.getCenter(new Vector3());
  center.y = bbox.min.y;

  // dispose object
  gltf.scene.traverse((child) => {
    if (child instanceof Mesh || child instanceof LineSegments) {
      child.geometry.dispose();
      const materials = Array.isArray(child.material)
        ? child.material
        : [child.material];
      materials.forEach((mat) => mat.dispose());
    }
  });

  const origin = new Vector3().setFromMatrixPosition(mat4);
  return center.sub(origin);
}

/**
 * Test if two meshes intersect.
 */
export function testIntersection(mesh1: Mesh, mesh2: Mesh): boolean {
  const posAttr = mesh1.geometry.getAttribute('position');

  for (let i = 0; i < posAttr.count; i++) {
    const pos = mesh1.localToWorld(
      new Vector3(posAttr.getX(i), 10000, posAttr.getZ(i)),
    );
    const raycaster = new Raycaster(pos, new Vector3(0, -1, 0));
    const intersections = raycaster.intersectObject(mesh2, true);

    if (intersections[0]) {
      return true;
    }
  }

  return false;
}

/**
 * Generate COLLADA structure for geometry instance.
 */
export function geo2dae(bufferGeo: BufferGeometry) {
  const posAttr = bufferGeo.getAttribute('position');

  const indexArray: number[] = [];
  for (let i = 0, l = posAttr.count; i < l; i++) {
    indexArray.push(i);
  }

  return {
    _attributes: {
      id: bufferGeo.name + '_geometry',
    },
    mesh: {
      source: {
        _attributes: {
          id: bufferGeo.name + '_positions',
        },
        float_array: {
          _attributes: {
            count: posAttr.array.length,
            id: bufferGeo.name + '_positions_array',
          },
          _text: (posAttr.array as number[]).join(' '),
        },
        technique_common: {
          accessor: {
            _attributes: {
              count: posAttr.count,
              source: '#' + bufferGeo.name + '_positions_array',
              stride: posAttr.itemSize,
            },
          },
          param: [
            { _attributes: { name: 'X', type: 'float' } },
            { _attributes: { name: 'Y', type: 'float' } },
            { _attributes: { name: 'Z', type: 'float' } },
          ],
        },
      },
      vertices: {
        _attributes: {
          id: bufferGeo.name + '_vertices',
        },
        input: {
          _attributes: {
            semantic: 'POSITION',
            source: '#' + bufferGeo.name + '_positions',
          },
        },
      },
      lines: {
        _attributes: {
          material: 'defaultMaterial',
          count: posAttr.count / 2,
        },
        input: {
          _attributes: {
            offset: 0,
            semantic: 'VERTEX',
            source: '#' + bufferGeo.name + '_vertices',
          },
        },
        p: {
          _text: indexArray.join(' '),
        },
      },
    },
  };
}
