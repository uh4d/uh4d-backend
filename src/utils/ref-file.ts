import { readFileSync } from 'node:fs';
import { join } from 'node:path';
import { readJsonSync } from 'fs-extra';
import { PackageJson } from 'tsconfig-paths/lib/filesystem';

export function embedDocFile(filename: string): string {
  return readFileSync(join(__dirname, '../assets/docs', filename), {
    encoding: 'utf8',
  });
}

export function getPkgJson(): PackageJson {
  return readJsonSync(join(__dirname, '../../package.json'));
}
