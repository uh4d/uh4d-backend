/**
 * Check if all properties are set in the object/map.
 * @param map Object/map to check against.
 * @param propertyList List of property names. If a 2-dimensional list is provided,
 * it will return `true` if at least one of these list passes the checks.
 */
export function checkRequiredProperties(
  map: { [key: string]: any },
  propertyList: string[] | string[][],
): boolean {
  // check if map is object
  if (typeof map.hasOwnProperty !== 'function') return false;

  // check if any property names provided
  if (!propertyList[0]) return true;

  // check if 2-dimensional array
  const list = Array.isArray(propertyList[0]) ? propertyList : [propertyList];

  return list.some((props) => {
    for (let i = 0; i < props.length; i++) {
      // check if property is set
      if (!Object.prototype.hasOwnProperty.call(map, props[i])) {
        return false;
      }

      const prop = map[props[i]];

      // check if undefined or length of string is 0
      if (
        typeof prop === 'undefined' ||
        (typeof prop === 'string' && prop.length === 0)
      ) {
        return false;
      }
    }

    return true;
  });
}
