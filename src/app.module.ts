import { Module } from '@nestjs/common';
import { AppConfigModule } from '@config/app-config.module';
import { RoutesModule } from '@routes/routes.module';
import { UploadModule } from '@upload/upload.module';

@Module({
  imports: [AppConfigModule, RoutesModule, UploadModule],
})
export class AppModule {}
