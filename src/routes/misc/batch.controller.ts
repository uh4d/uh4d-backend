import { Body, Controller, HttpCode, HttpStatus, Post } from '@nestjs/common';
import {
  ApiExtraModels,
  ApiOkResponse,
  ApiOperation,
  getSchemaPath,
} from '@nestjs/swagger';
import { ConfigService } from '@nestjs/config';
import { readFile } from 'fs-extra';
import { join } from 'node:path';
import { lookup } from 'mime-types';
import { BatchErrorDto, BatchSuccessDto } from './dto/batch-download.dto';
import { BatchDownloadBodyDto } from './dto/batch-download-body.dto';

@Controller('data/batch')
export class BatchController {
  constructor(private readonly config: ConfigService) {}

  @ApiOperation({
    summary: 'Batch download files',
    description:
      'Take array of file requests, load files, and return as base64 encoded strings.',
  })
  @ApiExtraModels(BatchSuccessDto, BatchErrorDto)
  @ApiOkResponse({
    schema: {
      type: 'array',
      items: {
        oneOf: [
          { $ref: getSchemaPath(BatchSuccessDto) },
          { $ref: getSchemaPath(BatchErrorDto) },
        ],
      },
    },
  })
  @Post()
  @HttpCode(HttpStatus.OK)
  async batchFiles(
    @Body() body: BatchDownloadBodyDto,
  ): Promise<(BatchSuccessDto | BatchErrorDto)[]> {
    const promises = body.files.map<Promise<BatchSuccessDto | BatchErrorDto>>(
      async (url) => {
        try {
          // strip data from url, if set
          const filePath = url.replace(/^data\//, '');

          // read binary data as base64 encoded string
          const file = await readFile(
            join(this.config.get<string>('path.data'), filePath),
            'base64',
          );

          return {
            status: 200,
            url,
            mimeType: lookup(url) || 'text/plain',
            file,
          };
        } catch (e) {
          return {
            status: e.code === 'ENOENT' ? 404 : 500,
            error: e.toString(),
            url,
          };
        }
      },
    );

    return Promise.all(promises);
  }
}
