import { IsArray, IsNotEmpty, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class BatchDownloadBodyDto {
  @ApiProperty({
    description: 'List of requested files',
  })
  @IsNotEmpty()
  @IsArray()
  @IsString({ each: true })
  files: string[];
}
