import { ApiProperty } from '@nestjs/swagger';

export class BaseDownloadDto {
  @ApiProperty({
    description:
      'Status code of file request: `200` if successful, `404` if not found',
  })
  status: number;

  @ApiProperty({
    description: 'File request url',
  })
  url: string;
}

export class BatchSuccessDto extends BaseDownloadDto {
  @ApiProperty({
    description: 'Mime-type of file',
  })
  mimeType: string;

  @ApiProperty({
    description: 'Base64 encoded file',
    format: 'base64',
  })
  file: string;
}

export class BatchErrorDto extends BaseDownloadDto {
  @ApiProperty({
    description: 'Error message',
  })
  error: string;
}
