import { rateLimit } from './rate-limit.middleware';

describe('RateLimitMiddleware', () => {
  it('should be defined', () => {
    expect(rateLimit).toBeDefined();
  });
});
