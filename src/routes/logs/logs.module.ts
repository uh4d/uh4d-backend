import { Module } from '@nestjs/common';
import { LogsController } from './logs.controller';
import { LogsGateway } from './logs.gateway';

@Module({
  controllers: [LogsController],
  providers: [LogsGateway],
})
export class LogsModule {}
