import { OnApplicationShutdown, UsePipes } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
  ConnectedSocket,
  MessageBody,
  OnGatewayConnection,
  OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketGateway,
  WsException,
} from '@nestjs/websockets';
import { WebSocket } from 'ws';
import { format, formatDistanceStrict } from 'date-fns';
import { writeFile } from 'fs-extra';
import { join } from 'node:path';
import { nanoid } from 'nanoid';
import { LogMessageDto } from './dto/log-message.dto';
import { WsValidationPipe } from './ws-validation.pipe';

interface LogSession {
  id: string;
  origin: string;
  timestamp: string;
  userAgent?: { [key: string]: any };
  logs: {
    log: string[];
    requests: string[];
    files: string[];
    warnings: string[];
    errors: string[];
    [others: string]: string[];
  };
}

const clients = new Map<WebSocket, LogSession>();

@WebSocketGateway({
  path: '/logs',
})
export class LogsGateway
  implements OnApplicationShutdown, OnGatewayConnection, OnGatewayDisconnect
{
  constructor(private readonly config: ConfigService) {}

  handleConnection(client: WebSocket, incoming: any): void {
    const id = nanoid(9);

    clients.set(client, {
      id,
      origin: incoming.headers.origin || 'unknown',
      timestamp: new Date().toISOString(),
      logs: {
        log: [],
        requests: [],
        files: [],
        warnings: [],
        errors: [],
      },
    });

    client.send(`Connected. Session ID: ${id}`);
  }

  @UsePipes(new WsValidationPipe())
  @SubscribeMessage('message')
  handleMessage(
    @ConnectedSocket() client: WebSocket,
    @MessageBody() payload: LogMessageDto,
  ): void {
    if (['timestamp', 'exitCode'].includes(payload.type)) {
      throw new WsException(`Restricted message type: ${payload.type}`);
    }

    const data = clients.get(client);
    const timestamp = format(new Date(), "yyyy-MM-dd'T'HH:mm:ss.SSS");

    switch (payload.type) {
      case 'userAgent':
        data.userAgent = JSON.parse(payload.value);
        break;

      default:
        if (!data.logs[payload.type]) {
          data.logs[payload.type] = [];
        }
        data.logs[payload.type].push(`[${timestamp}] ${payload.value}`);
    }
  }

  async handleDisconnect(client: WebSocket): Promise<void> {
    const data = clients.get(client);
    const timestamp = new Date();

    // compose output
    let output = `SessionID: ${data.id}\n`;
    output += `Origin: ${data.origin}\n`;
    output += `Timestamp: ${data.timestamp}\n`;
    output += `Exited at ${timestamp.toISOString()} after ${formatDistanceStrict(
      timestamp,
      new Date(data.timestamp),
    )}\n`;

    const userAgent = data.userAgent;
    if (userAgent) {
      output += '\nUserAgent:\n----------\n';
      output += JSON.stringify(userAgent, null, 2);
    }

    for (const [key, value] of Object.entries(data.logs)) {
      const heading =
        key.replace(/^(.)/, (match, p1) => p1.toUpperCase()) + ':';
      output += `\n\n${heading}\n${heading.replace(/./g, '-')}\n`;

      output += value.join('\n');
    }

    // compose filename
    const filename = [format(new Date(data.timestamp), 'yyyyMMdd-HHmmss-S')];

    if (userAgent) {
      if (userAgent.os.name) {
        filename.push(
          userAgent.os.name +
            (userAgent.os.version ? '-' + userAgent.os.version : ''),
        );
      }
      if (userAgent.browser.name) {
        filename.push(
          userAgent.browser.name +
            (userAgent.browser.major ? '-' + userAgent.browser.major : ''),
        );
      }
    }

    // write log file
    const filePath = join(
      this.config.get<string>('path.logs'),
      filename.join('-') + '.log',
    );
    await writeFile(filePath, output, { encoding: 'utf8' });

    clients.delete(client);
  }

  async onApplicationShutdown(): Promise<void> {
    await Promise.all(
      [...clients.keys()].map((client) => this.handleDisconnect(client)),
    );
  }
}
