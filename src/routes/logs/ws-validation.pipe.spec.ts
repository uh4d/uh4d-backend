import { WsValidationPipe } from './ws-validation.pipe';

describe('WsValidationPipe', () => {
  it('should be defined', () => {
    expect(new WsValidationPipe()).toBeDefined();
  });
});
