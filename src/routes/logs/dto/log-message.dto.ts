import { IsNotEmpty, IsString } from 'class-validator';

export class LogMessageDto {
  @IsNotEmpty()
  @IsString()
  type: string;
  @IsNotEmpty()
  @IsString()
  value: string;
}
