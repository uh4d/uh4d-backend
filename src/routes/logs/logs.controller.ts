import {
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  NotFoundException,
  Param,
  Query,
  Res,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {
  ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiProduces,
  ApiTags,
} from '@nestjs/swagger';
import { pathExists, readdir, readFile, remove } from 'fs-extra';
import { join } from 'node:path';
import { Response } from 'express';
import { isWithinInterval, parse } from 'date-fns';
import { GetLogsQueryParams } from './dto/get-logs-query-params';

@ApiTags('Logs')
@Controller('logs')
export class LogsController {
  constructor(private readonly config: ConfigService) {}

  @ApiOperation({
    summary: 'Query logs',
    description:
      'Query log files. A list of log files is returned.\n\nIf query parameter `format=html` is set, it returns an HTML page, on which the log files can be viewed interactively.\n\nNarrow down results by querying by date using `from` and `to` parameters.',
  })
  @ApiOkResponse({
    content: {
      'application/json': {
        schema: { type: 'array', items: { type: 'string' } },
      },
      'text/html': {
        schema: { type: 'string' },
      },
    },
  })
  @Get()
  async query(
    @Query() queryParams: GetLogsQueryParams,
    @Res() res: Response,
  ): Promise<void> {
    if (queryParams.format === 'html') {
      // send log-viewer html page
      const html = await readFile(
        join(__dirname, '../../assets/log-viewer.html'),
        { encoding: 'utf-8' },
      );
      res.setHeader('Content-Type', 'text/html');
      res.send(html);
    } else {
      // query files and return list
      let files = await readdir(this.config.get<string>('path.logs'));

      // filter list
      if (queryParams.from && queryParams.to) {
        files = files.filter((file) =>
          isWithinInterval(
            parse(file.slice(0, 15), 'yyyyMMdd-HHmmss', new Date()),
            {
              start: new Date(queryParams.from),
              end: new Date(queryParams.to),
            },
          ),
        );
      }

      res.send(files);
    }
  }

  @ApiOperation({
    summary: 'Get log file',
    description: 'Get contents of log file.',
  })
  @ApiProduces('text/plain')
  @ApiOkResponse({
    schema: {
      type: 'string',
    },
  })
  @ApiNotFoundResponse()
  @Get(':logFile')
  async getById(@Param('logFile') file: string): Promise<string> {
    const filePath = join(this.config.get<string>('path.logs'), file);

    // check if file exists
    const exists = await pathExists(filePath);
    if (!exists) {
      throw new NotFoundException();
    }

    return readFile(filePath, { encoding: 'utf8' });
  }

  @ApiOperation({
    summary: 'Delete log file',
  })
  @ApiNoContentResponse()
  @ApiNotFoundResponse()
  @HttpCode(HttpStatus.NO_CONTENT)
  @Delete(':logFile')
  async remove(
    @Param('logFile') file: string,
    @Res() res: Response,
  ): Promise<void> {
    const filePath = join(this.config.get<string>('path.logs'), file);

    // check if file exists
    const exists = await pathExists(filePath);
    if (!exists) {
      throw new NotFoundException();
    }

    await remove(filePath);

    res.send();
  }
}
