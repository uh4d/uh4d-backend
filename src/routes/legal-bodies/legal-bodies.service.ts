import { Injectable } from '@nestjs/common';
import { Neo4jService } from '@brakebein/nest-neo4j';
import { escapeRegex } from '@utils/string-utils';
import { LegalBodyDto } from './dto/legal-body.dto';

@Injectable()
export class LegalBodiesService {
  constructor(private readonly neo4j: Neo4jService) {}

  async query(search = ''): Promise<LegalBodyDto[]> {
    // language=Cypher
    const q = `
      MATCH (e40:E40:UH4D)-[:P131]->(name:E82)
      WHERE name.value =~ $search
      RETURN e40.id as id, name.value AS value`;

    const params = {
      search: `(?i).*${escapeRegex(search)}.*`,
    };

    return this.neo4j.read(q, params);
  }

  async getById(legalBodyId: string): Promise<LegalBodyDto> {
    // language=Cypher
    const q = `
      MATCH (e40:E40:UH4D {id: $id})-[:P131]->(name:E82)
      RETURN e40.id AS id, name.value AS value`;

    const params = {
      id: legalBodyId,
    };

    return (await this.neo4j.read(q, params))[0];
  }
}
