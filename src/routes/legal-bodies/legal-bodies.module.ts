import { Module } from '@nestjs/common';
import { LegalBodiesController } from './legal-bodies.controller';
import { LegalBodiesService } from './legal-bodies.service';

@Module({
  controllers: [LegalBodiesController],
  providers: [LegalBodiesService]
})
export class LegalBodiesModule {}
