import { Test, TestingModule } from '@nestjs/testing';
import { LegalBodiesController } from './legal-bodies.controller';

describe('LegalBodiesController', () => {
  let controller: LegalBodiesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [LegalBodiesController],
    }).compile();

    controller = module.get<LegalBodiesController>(LegalBodiesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
