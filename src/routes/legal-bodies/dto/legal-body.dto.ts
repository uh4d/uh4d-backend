import { ApiProperty } from '@nestjs/swagger';

export class LegalBodyDto {
  @ApiProperty({
    description: 'Legal body ID',
    required: false,
  })
  id: string;
  @ApiProperty({
    description: 'Legal body name',
    required: false,
  })
  value: string;
}
