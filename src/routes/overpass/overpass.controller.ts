import {
  BadRequestException,
  Controller,
  Get,
  Logger,
  Query,
  Res,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { ConfigService } from '@nestjs/config';
import { join } from 'node:path';
import { ensureDir, pathExists, readJson, writeJson } from 'fs-extra';
import { Response } from 'express';
import { Worker } from 'worker_threads';
import { roundDecimalDegree } from '@utils/round-decimal-degree';
import { TerrainService } from '@routes/terrain/terrain.service';
import { OverpassService } from './overpass.service';
import { OverpassDto } from './dto/overpass.dto';
import { OverpassQueryParams } from './dto/overpass-query-params';
import { HeightsQueryParams } from './dto/heights-query-params';
import { OsmAltitudesDto } from './dto/osm-altitudes.dto';

@ApiTags('Terrain')
@Controller('overpass')
export class OverpassController {
  private readonly logger = new Logger(OverpassController.name);

  constructor(
    private readonly overpassService: OverpassService,
    private readonly terrainService: TerrainService,
    private readonly config: ConfigService,
  ) {}

  @ApiOperation({
    summary: 'Request OSM data',
    description:
      'Request building footprints from OpenStreetMap using Overpass API. The building footprints can be processed and extruded to 3D geometry.\n\nSince requesting Overpass API can take up to several seconds, the requested data is cached to serve data faster at future requests. For this reason, the `lat` + `lon` query parameters are rounded with an accuracy about 50 meters.\n\nQuery parameters `lat`, `lon`, `r` are required as search parameter.',
  })
  @ApiOkResponse({
    type: OverpassDto,
  })
  @ApiBadRequestResponse()
  @Get()
  async query(
    @Query() queryParams: OverpassQueryParams,
    @Res() res: Response,
  ): Promise<void> {
    // round position
    const r = Math.round(parseFloat(queryParams.r));
    const lat = roundDecimalDegree(parseFloat(queryParams.lat), r / 10);
    const lon = roundDecimalDegree(parseFloat(queryParams.lon), r / 10);

    // query Overpass API
    const data = await this.overpassService.query(
      lat,
      lon,
      parseFloat(queryParams.r),
    );

    try {
      // cache data
      await ensureDir(join(this.config.get<string>('path.cache'), 'overpass'));
      const file = join(
        this.config.get<string>('path.cache'),
        'overpass',
        `${lat}-${lon}-${r}.json`,
      );
      await writeJson(file, data, { spaces: 2 });
    } catch (e) {
      this.logger.error(e);
    }

    // send
    res.json(data);
  }

  @ApiOperation({
    summary: 'Compute altitudes',
    description:
      'Compute altitudes for OSM buildings based on digital elevation model.',
  })
  @ApiOkResponse({
    type: OsmAltitudesDto,
  })
  @ApiBadRequestResponse()
  @Get('altitudes')
  async getAltitudes(
    @Query() queryParams: HeightsQueryParams,
    @Res() res: Response,
  ): Promise<void> {
    // round position
    const r = Math.round(parseFloat(queryParams.r));
    const lat = roundDecimalDegree(parseFloat(queryParams.lat), r / 10);
    const lon = roundDecimalDegree(parseFloat(queryParams.lon), r / 10);

    const cacheFile = join(
      this.config.get<string>('path.cache'),
      'overpass',
      `${queryParams.terrain.split('/').pop()}-${lat}-${lon}-${r}.json`,
    );

    // send cached file
    if (await pathExists(cacheFile)) {
      const data = await readJson(cacheFile);
      res.json(data);
      return;
    }

    // check osm file
    const osmPath = join(
      this.config.get<string>('path.cache'),
      'overpass',
      `${lat}-${lon}-${r}.json`,
    );

    if (!(await pathExists(osmPath)))
      throw new BadRequestException(
        "Path to cached Overpass data couldn't be found!",
      );

    // compute altitudes in worker thread
    const threadData: { [key: string]: any } = {
      lat,
      lon,
      osmPath,
    };

    // if `terrain` is just an ID and not a path with slashes
    if (queryParams.terrain.split('/').length === 1) {
      // custom terrain
      const terrainData = await this.terrainService.getById(
        queryParams.terrain,
      );
      if (!terrainData) {
        throw new BadRequestException(
          `Terrain with ID ${queryParams.terrain} not found!`,
        );
      }

      threadData.terrainData = terrainData;
      threadData.modelPath = join(
        this.config.get<string>('path.data'),
        terrainData.file.path,
        terrainData.file.file,
      );
      threadData.isCustomTerrain = true;
    } else {
      // elevation api terrain
      const modelPath = join(
        this.config.get<string>('path.cache'),
        'elevation',
        queryParams.terrain.replace(/.+\/gltf\//, ''),
      );
      if (!(await pathExists(modelPath)))
        throw new BadRequestException("Path to model file couldn't be found!");

      threadData.modelPath = modelPath;
    }

    // run worker thread
    const altitudes = await new Promise((resolve, reject) => {
      const worker = new Worker(
        join(__dirname, '../../workers/compute-osm-altitudes.js'),
        {
          workerData: threadData,
        },
      );
      worker.on('message', resolve);
      worker.on('error', reject);
      worker.on('exit', (code) => {
        if (code !== 0)
          reject(new Error(`Worker stopped with exit code ${code}`));
      });
    });

    // send data
    res.json(altitudes);

    // write data to cache
    await writeJson(cacheFile, altitudes, { spaces: 2 });
  }
}
