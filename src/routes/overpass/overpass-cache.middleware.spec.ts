import { OverpassCacheMiddleware } from './overpass-cache.middleware';
import { ConfigService } from '@nestjs/config';

describe('OverpassCacheMiddleware', () => {
  let middleware: OverpassCacheMiddleware;

  beforeEach(async () => {
    const configService = new ConfigService();

    middleware = new OverpassCacheMiddleware(configService);
  });

  it('should be defined', () => {
    expect(middleware).toBeDefined();
  });
});
