import { Injectable, Logger } from '@nestjs/common';
import { Coordinates } from '@utils/coordinates';
import { HttpService } from '@nestjs/axios';
import { catchError, delay, lastValueFrom, of, retry, tap } from 'rxjs';
import { OverpassDto } from './dto/overpass.dto';

@Injectable()
export class OverpassService {
  private readonly logger = new Logger(OverpassService.name);

  constructor(private readonly http: HttpService) {}

  /**
   * Query OSM Overpass API for ways/relations of type `building`.
   */
  async query(
    latitude: number,
    longitude: number,
    radius: number,
  ): Promise<OverpassDto> {
    const { west, east, south, north } = new Coordinates(
      latitude,
      longitude,
    ).computeBoundingBox(radius);

    // bbox south, west, north, east
    const body = `[out:json][timeout:25];
(
  way["building"](${south},${west},${north},${east});
  relation["building"](${south},${west},${north},${east});
);
out body;
>;
out skel qt;`;

    // Overpass API sometimes errors with Timeout
    // -> retry 5 times with a small delay
    const observable = this.http
      .post('https://overpass-api.de/api/interpreter', body, {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
        },
      })
      .pipe(
        retry({
          count: 5,
          delay: (error, retryCount) =>
            of(retryCount).pipe(
              tap(() => this.logger.log('Overpass API: Retry in 1 seconds...')),
              delay(1000),
            ),
        }),
        catchError(() => {
          throw 'Overpass API was not accessible after 5 tries.';
        }),
      );

    return (await lastValueFrom(observable)).data;
  }
}
