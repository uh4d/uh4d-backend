import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { rateLimit } from '@routes/rate-limit.middleware';
import { TerrainModule } from '@routes/terrain/terrain.module';
import { OverpassController } from './overpass.controller';
import { OverpassService } from './overpass.service';
import { OverpassCacheMiddleware } from './overpass-cache.middleware';

@Module({
  imports: [HttpModule, TerrainModule],
  controllers: [OverpassController],
  providers: [OverpassService],
  exports: [OverpassService],
})
export class OverpassModule implements NestModule {
  configure(consumer: MiddlewareConsumer): void {
    consumer
      .apply(OverpassCacheMiddleware, rateLimit(1, 1000))
      .forRoutes({ path: 'overpass', method: RequestMethod.GET });
  }
}
