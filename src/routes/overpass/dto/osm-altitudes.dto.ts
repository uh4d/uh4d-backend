export class OsmAltitudesDto {
  [osmId: string]: number;
}
