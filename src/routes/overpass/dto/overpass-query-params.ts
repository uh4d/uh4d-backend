import { ApiProperty } from '@nestjs/swagger';
import {
  IsLatitude,
  IsLongitude,
  IsNotEmpty,
  IsNumberString,
} from 'class-validator';

export class OverpassQueryParams {
  @ApiProperty({
    description: 'Latitude in decimal degree',
  })
  @IsNotEmpty()
  @IsNumberString()
  @IsLatitude()
  lat: string;

  @ApiProperty({
    description: 'Longitude in decimal degree',
  })
  @IsNotEmpty()
  @IsNumberString()
  @IsLongitude()
  lon: string;

  @ApiProperty({
    description: 'Radius in meters',
  })
  @IsNotEmpty()
  @IsNumberString()
  r: string;
}
