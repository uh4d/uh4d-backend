import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';
import { OverpassQueryParams } from './overpass-query-params';

export class HeightsQueryParams extends OverpassQueryParams {
  @ApiProperty({
    description:
      'Terrain ID, if custom terrain model, otherwise path to gltf file (cached model from Elevation API with Web Mercator coordinates)',
  })
  @IsNotEmpty()
  @IsString()
  terrain: string;
}
