import { Injectable } from '@nestjs/common';
import { Neo4jService } from '@brakebein/nest-neo4j';
import { SceneDto } from '@dto/scene';

@Injectable()
export class ScenesService {
  constructor(private readonly neo4j: Neo4jService) {}

  async getById(sceneId: string): Promise<SceneDto | undefined> {
    // language=Cypher
    const q = `
      MATCH (place:E53:UH4D {id: $scene})-[:P1]->(name:E41),
            (place)-[:P168]->(geo:E94)
      RETURN place.id AS id,
             name.value as name,
             geo.latitude AS latitude,
             geo.longitude AS longitude,
             geo.altitude AS altitude
    `;

    const params = {
      scene: sceneId,
    };

    return (await this.neo4j.read(q, params))[0];
  }
}
