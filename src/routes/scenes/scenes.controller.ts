import { Controller, Get, NotFoundException, Param } from '@nestjs/common';
import {
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
} from '@nestjs/swagger';
import { SceneDto } from '@dto/scene';
import { ScenesService } from './scenes.service';

@Controller('scenes')
export class ScenesController {
  constructor(private readonly sceneService: ScenesService) {}

  @ApiOperation({
    summary: 'Get scene',
    description: 'Get basic information about the scene.',
  })
  @ApiOkResponse({
    type: SceneDto,
  })
  @ApiNotFoundResponse()
  @Get(':sceneId')
  async getById(@Param('sceneId') sceneId: string): Promise<SceneDto> {
    const scene = await this.sceneService.getById(sceneId);

    if (!scene) {
      throw new NotFoundException(`Scene with ID ${sceneId} not found!`);
    }

    return scene;
  }
}
