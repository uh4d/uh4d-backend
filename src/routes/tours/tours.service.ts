import { Injectable } from '@nestjs/common';
import { Neo4jService } from '@brakebein/nest-neo4j';
import { nanoid } from 'nanoid';
import { GetToursQueryParams } from './dto/get-tours-query-params';
import { TourDto } from './dto/tour.dto';
import { CreateTourDto } from './dto/create-tour.dto';

@Injectable()
export class ToursService {
  constructor(private readonly neo4j: Neo4jService) {}

  async query(query: GetToursQueryParams): Promise<TourDto[]> {
    // parse query parameter
    const spatialParams = {
      latitude: parseFloat(query.lat),
      longitude: parseFloat(query.lon),
      radius: parseFloat(query.r),
    };
    const doSpatialQuery =
      !isNaN(spatialParams.latitude) &&
      !isNaN(spatialParams.longitude) &&
      !isNaN(spatialParams.radius);

    // language=Cypher
    let q = `
    MATCH (tour:Tour)-[r:CONTAINS]->(poi:E73)-[:P2]->(:E55 {id: "poi"}),
          (poi)-[:P67]->(:E92)-[:P161]->(place:E53)-[:P168]->(geo:E94),
          (poi)-[:P102]->(title:E35)
    ${query.scene ? 'WHERE (place)-[:P89]->(:E53:UH4D {id: $scene})' : ''}
    WITH tour, poi, geo, title
    ORDER BY r.order
    WITH tour, collect({id: poi.id, title: title.value, geo: geo}) AS pois`;

    if (doSpatialQuery) {
      q += `, point({latitude: $geo.latitude, longitude: $geo.longitude}) AS userPoint
        WHERE any(p IN pois WHERE distance(p.geo.point, userPoint) < $geo.radius)`;
    }

    // language=Cypher
    q += `
      RETURN tour.id AS id,
             tour.title AS title,
             [p IN pois | {id: p.id, title: p.title}] as pois
    `;

    const params = {
      scene: query.scene,
      geo: spatialParams,
    };

    return this.neo4j.read(q, params);
  }

  async create(body: CreateTourDto): Promise<TourDto> {
    // language=Cypher
    const q = `
      CREATE (tour:Tour:UH4D $tourMap)
      WITH tour, range(0, size($pois) - 1) as indices
      UNWIND indices as i
      MATCH (poi:E73 {id: $pois[i]})-[:P2]->(:E55 {id: "poi"}),
            (poi)-[:P102]->(pTitle:E35)
      CREATE (tour)-[:CONTAINS {order: i}]->(poi)
      RETURN tour.id AS id,
             tour.title AS title,
             collect({id: poi.id, title: pTitle.value}) as pois
    `;

    const params = {
      tourMap: {
        id: 'tour_' + nanoid(9),
        title: body.title,
      },
      pois: body.pois,
    };

    return (await this.neo4j.write(q, params))[0];
  }

  /**
   * Remove tour.
   * @return Resolves with `true` if operation was successful.
   */
  async remove(tourId: string): Promise<boolean> {
    // language=Cypher
    const q = `
      MATCH (poi:E73)<-[:CONTAINS]-(tour:Tour {id: $id})
      DETACH DELETE tour
      RETURN true AS check
    `;

    const params = {
      id: tourId,
    };

    return !!(await this.neo4j.write(q, params))[0];
  }
}
