import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  NotFoundException,
  Param,
  Post,
  Query,
  Res,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { Response } from 'express';
import { ToursService } from './tours.service';
import { GetToursQueryParams } from './dto/get-tours-query-params';
import { CreateTourDto } from './dto/create-tour.dto';
import { TourDto } from './dto/tour.dto';

@ApiTags('Points of Interest')
@Controller('tours')
export class ToursController {
  constructor(private readonly tourService: ToursService) {}

  @ApiOperation({
    summary: 'Query tours',
    description:
      'Query all tours. Query parameters may be set to filter the data.\n\n`lat`, `lon`, and `r` only work in combination and may be used to look for tours where at least one point of interest is located within a radius at a position.',
  })
  @ApiOkResponse({
    type: TourDto,
    isArray: true,
  })
  @Get()
  async query(@Query() queryParams: GetToursQueryParams): Promise<TourDto[]> {
    return this.tourService.query(queryParams);
  }

  @ApiOperation({
    summary: 'Save tour',
    description: 'Save new tour, i.e. a sequence of points of interest.',
  })
  @ApiCreatedResponse({
    type: TourDto,
  })
  @ApiBadRequestResponse()
  @Post()
  async create(@Body() body: CreateTourDto): Promise<TourDto> {
    const poi = await this.tourService.create(body);

    if (!poi) {
      throw new BadRequestException('Some POIs could not be found!');
    }

    return poi;
  }

  @ApiOperation({
    summary: 'Delete tour',
    description: 'Delete tour.',
  })
  @ApiNoContentResponse()
  @ApiNotFoundResponse()
  @HttpCode(HttpStatus.NO_CONTENT)
  @Delete(':tourId')
  async remove(
    @Param('tourId') tourId: string,
    @Res() res: Response,
  ): Promise<void> {
    const removed = await this.tourService.remove(tourId);

    if (!removed) {
      throw new NotFoundException(`Tour with ID ${tourId} not found!`);
    }

    res.send();
  }
}
