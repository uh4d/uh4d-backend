import { ApiProperty } from '@nestjs/swagger';

export class TourPoi {
  @ApiProperty({
    description: 'Point of interest ID',
    required: false,
  })
  id: string;
  @ApiProperty({
    description: 'Title of point of interest',
    required: false,
  })
  title: string;
}

export class TourDto {
  @ApiProperty({
    description: 'Tour ID',
    required: false,
  })
  id: string;
  @ApiProperty({
    description: 'Tour ID',
    required: false,
  })
  title: string;
  @ApiProperty({
    description: 'List of points of interest, already in order',
    required: false,
    type: [TourPoi],
  })
  pois: TourPoi[];
}
