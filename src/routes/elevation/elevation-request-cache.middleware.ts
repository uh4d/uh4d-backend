import {
  BadRequestException,
  Injectable,
  NestMiddleware,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NextFunction, Request, Response } from 'express';
import { join } from 'node:path';
import { pathExists, readJson } from 'fs-extra';
import { roundDecimalDegree } from '@utils/round-decimal-degree';

@Injectable()
export class ElevationRequestCacheMiddleware implements NestMiddleware {
  constructor(private readonly config: ConfigService) {}

  async use(req: Request, res: Response, next: NextFunction) {
    // check query parameters
    let lat = parseFloat(req.query.lat as string);
    let lon = parseFloat(req.query.lon as string);
    const r = Math.round(parseFloat(req.query.r as string));

    if (Number.isNaN(lat) || Number.isNaN(lon) || Number.isNaN(r))
      throw new BadRequestException(
        'Missing required query parameters lat, lon, and r!',
      );

    // round position
    lat = roundDecimalDegree(lat, r / 10);
    lon = roundDecimalDegree(lon, r / 10);

    // check for cached data file
    const file = join(
      this.config.get<string>('path.cache'),
      'elevation',
      `${lat}-${lon}-${r}.json`,
    );
    const exists = await pathExists(file);

    if (exists) {
      // read file and send
      const data = await readJson(file);
      res.json(data);
    } else {
      // no file, pass request forward
      next();
    }
  }
}
