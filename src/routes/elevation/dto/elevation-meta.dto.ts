import { ApiProperty } from '@nestjs/swagger';

class BoundingBox {
  @ApiProperty({ required: false })
  xMin: number;
  @ApiProperty({ required: false })
  xMax: number;
  @ApiProperty({ required: false })
  yMin: number;
  @ApiProperty({ required: false })
  yMax: number;
  @ApiProperty({ required: false })
  zMin: number;
  @ApiProperty({ required: false })
  zMax: number;
  @ApiProperty({ required: false })
  width: number;
  @ApiProperty({ required: false })
  height: number;
  @ApiProperty({ required: false })
  depth: number;
  @ApiProperty({ required: false, type: [Number] })
  center: number[];
  @ApiProperty({ required: false })
  area: number;
  @ApiProperty({ required: false })
  wkt: string;
  @ApiProperty({ required: false })
  srid: number;
}

class TextureMeta {
  @ApiProperty({ required: false })
  filePath: string;
  @ApiProperty({ required: false })
  fileName: string;
  @ApiProperty({ required: false })
  imageFormat: string;
  @ApiProperty({ required: false })
  width: number;
  @ApiProperty({ required: false })
  height: number;
  @ApiProperty({ required: false })
  projectedZoom: number;
}

class AlbedoTextureMeta extends TextureMeta {
  @ApiProperty({ required: false })
  projectedBounds: BoundingBox;
}

class GeoReference {
  @ApiProperty({ required: false })
  boundingBox: BoundingBox;
  @ApiProperty({ required: false })
  widthMeters: number;
  @ApiProperty({ required: false })
  heightMeters: number;
  @ApiProperty({ required: false })
  medianElevation: number;
}

class Attribution {
  @ApiProperty({ required: false })
  subject: string;
  @ApiProperty({ required: false })
  text: string;
  @ApiProperty({ required: false })
  url: string;
  @ApiProperty({ required: false })
  acknowledgment: string;
}

class AssetInfo {
  @ApiProperty({ required: false })
  minElevation: number;
  @ApiProperty({ required: false })
  maxElevation: number;
  @ApiProperty({ required: false })
  geoReference: GeoReference;
  @ApiProperty({ required: false })
  baseRoute: string;
  @ApiProperty({ required: false })
  fileBaseRoute: string;
  @ApiProperty({ required: false })
  modelFile: string;
  @ApiProperty({ required: false })
  modelFileSizeMB: number;
  @ApiProperty({ required: false })
  heightMap: TextureMeta;
  @ApiProperty({ required: false })
  albedoTexture: AlbedoTextureMeta;
  @ApiProperty({ required: false })
  normalMapTexture: TextureMeta;
  @ApiProperty({ required: false, type: [Attribution] })
  attributions: Attribution[];
  @ApiProperty({ required: false })
  requestId: string;
}

export class ElevationMetaDto {
  @ApiProperty({ required: false })
  success: string;
  @ApiProperty({ required: false })
  assetInfo: AssetInfo;
  @ApiProperty({ required: false })
  estimatedModelFileSizeMB: number;
}
