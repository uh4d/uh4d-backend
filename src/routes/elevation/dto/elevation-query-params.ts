import { ApiProperty } from '@nestjs/swagger';
import {
  IsLatitude,
  IsLongitude,
  IsNotEmpty,
  IsNumberString,
  IsOptional,
} from 'class-validator';

export class ElevationQueryParams {
  @ApiProperty({
    description: 'Latitude in decimal degree',
  })
  @IsNotEmpty()
  @IsNumberString()
  @IsLatitude()
  lat: string;

  @ApiProperty({
    description: 'Longitude in decimal degree',
  })
  @IsNotEmpty()
  @IsNumberString()
  @IsLongitude()
  lon: string;

  @ApiProperty({
    description: 'Radius in meters',
  })
  @IsNotEmpty()
  @IsNumberString()
  r: string;

  @ApiProperty({
    description: 'DEM dataset name',
    required: false,
    default: 'FABDEM',
  })
  @IsOptional()
  dataSet?: string;

  @ApiProperty({
    description: 'Add textures',
    required: false,
    default: 'true',
  })
  @IsOptional()
  textured?: string;

  @ApiProperty({
    description: 'Imagery provider to use for the texture',
    required: false,
    default: 'ThunderForest-Neighbourhood',
  })
  @IsOptional()
  imageryProvider?: string;

  @ApiProperty({
    description: 'Texture quality',
    required: false,
    default: '2',
  })
  @IsOptional()
  textureQuality?: string;
}
