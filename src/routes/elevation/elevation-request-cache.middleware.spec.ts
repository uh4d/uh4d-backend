import { ElevationRequestCacheMiddleware } from './elevation-request-cache.middleware';
import { ConfigService } from '@nestjs/config';

describe('ElevationRequestCacheMiddleware', () => {
  let middleware: ElevationRequestCacheMiddleware;

  beforeEach(async () => {
    const configService = new ConfigService();

    middleware = new ElevationRequestCacheMiddleware(configService);
  });

  it('should be defined', () => {
    expect(middleware).toBeDefined();
  });
});
