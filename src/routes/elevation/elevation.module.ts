import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { rateLimit } from '@routes/rate-limit.middleware';
import { ElevationController } from './elevation.controller';
import { ElevationRequestCacheMiddleware } from './elevation-request-cache.middleware';
import { ElevationFileCacheMiddleware } from './elevation-file-cache.middleware';

@Module({
  imports: [HttpModule],
  controllers: [ElevationController],
})
export class ElevationModule implements NestModule {
  configure(consumer: MiddlewareConsumer): void {
    consumer
      .apply(ElevationRequestCacheMiddleware, rateLimit(1, 6000))
      .forRoutes({ path: 'elevation', method: RequestMethod.GET });
    consumer.apply(ElevationFileCacheMiddleware).forRoutes({
      path: 'elevation/gltf/:folder/:file',
      method: RequestMethod.GET,
    });
  }
}
