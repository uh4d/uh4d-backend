import { ElevationFileCacheMiddleware } from './elevation-file-cache.middleware';
import { ConfigService } from '@nestjs/config';

describe('ElevationFileCacheMiddleware', () => {
  let middleware: ElevationFileCacheMiddleware;

  beforeEach(async () => {
    const configService = new ConfigService();

    middleware = new ElevationFileCacheMiddleware(configService);
  });

  it('should be defined', () => {
    expect(middleware).toBeDefined();
  });
});
