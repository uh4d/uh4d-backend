import { Body, Controller, Post } from '@nestjs/common';
import { ApiCreatedResponse, ApiOperation } from '@nestjs/swagger';
import { HttpService } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';
import { CreateBugDto } from './dto/create-bug.dto';

@Controller('bugs')
export class BugsController {
  constructor(
    private readonly http: HttpService,
    private readonly config: ConfigService,
  ) {}

  @ApiOperation({
    summary: 'Post bug',
    description: 'Open new issue in GitLab repo utilizing GitLab API.',
  })
  @ApiCreatedResponse()
  @Post()
  async createBug(@Body() body: CreateBugDto) {
    const gitlab = this.config.get('gitlab.' + body.context)
      ? this.config.get('gitlab.' + body.context)
      : this.config.get('gitlab.uh4d-browser-app');
    const url = `${gitlab.apiEndpoint}/projects/${gitlab.projectId}/issues`;

    const response = await this.http.axiosRef.post(
      url,
      {
        title: body.title,
        description: `_Scene:_ ${body.scene || 'any'}\n\n${body.description}`,
      },
      {
        headers: {
          'PRIVATE-TOKEN': gitlab.privateToken,
        },
      },
    );

    return response.data;
  }
}
