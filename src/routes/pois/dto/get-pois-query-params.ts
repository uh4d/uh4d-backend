import { ApiProperty } from '@nestjs/swagger';
import {
  IsDateString,
  IsLatitude,
  IsLongitude,
  IsNumberString,
  IsOptional,
} from 'class-validator';

export class GetPoisQueryParams {
  @ApiProperty({
    description: 'Filter by date',
    required: false,
  })
  @IsOptional()
  @IsDateString()
  date?: string;

  @ApiProperty({
    description: 'Latitude in decimal degree',
    required: false,
  })
  @IsOptional()
  @IsNumberString()
  @IsLatitude()
  lat?: string;

  @ApiProperty({
    description: 'Longitude in decimal degree',
    required: false,
  })
  @IsOptional()
  @IsNumberString()
  @IsLongitude()
  lon?: string;

  @ApiProperty({
    description: 'Radius in meters',
    required: false,
  })
  @IsOptional()
  @IsNumberString()
  r?: string;

  @ApiProperty({
    description: 'Filter by specific scene',
    required: false,
  })
  @IsOptional()
  scene?: string;

  @ApiProperty({
    description:
      'Type of sources points of interest should be queried from. Default: `uh4d`',
    required: false,
  })
  @IsOptional()
  type?: string | string[];
}
