import { BadRequestException, Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { Neo4jService } from '@brakebein/nest-neo4j';
import { nanoid } from 'nanoid';
import { CreateUh4dPoiDto, Uh4dPoiDto, UpdateUh4dPoiDto } from '@dto/uh4d-poi';
import { GeneralPoiDto } from '@dto/general-poi';
import { GetPoisQueryParams } from './dto/get-pois-query-params';

@Injectable()
export class PoisService {
  constructor(
    private readonly neo4j: Neo4jService,
    private readonly http: HttpService,
  ) {}

  async query(query: GetPoisQueryParams): Promise<Uh4dPoiDto[]> {
    const spatialParams = {
      latitude: parseFloat(query.lat),
      longitude: parseFloat(query.lon),
      radius: parseFloat(query.r),
    };
    const doSpatialQuery =
      !isNaN(spatialParams.latitude) &&
      !isNaN(spatialParams.longitude) &&
      !isNaN(spatialParams.radius);

    let q = '';

    if (doSpatialQuery) {
      q += `WITH point({latitude: $geo.latitude, longitude: $geo.longitude}) AS userPoint `;
    }

    q += 'MATCH ' + (query.scene ? '(:E53:UH4D {id: $scene})<-[:P89]-' : '');
    q += `(place:E53)<-[:P161]-(e92:E92)<-[:P67]-(poi:E73)-[:P2]->(:E55 {id: "poi"}),
          (place)-[:P168]->(geo:E94) `;

    if (doSpatialQuery) {
      q += `WHERE distance(geo.point, userPoint) < $geo.radius`;
    }

    q += `
      OPTIONAL MATCH (e92)-[:P160]->(:E52)-[:P82]->(poiDate:E61)
      OPTIONAL MATCH (poi)-[:P67]->(e22:E22)
      OPTIONAL MATCH (e22)<-[:P108]-(:E12)-[:P4]->(:E52)-[:P82]->(objFrom:E61)
      OPTIONAL MATCH (e22)<-[:P13]-(:E6)-[:P4]->(:E52)-[:P82]->(objTo:E61)
      WITH poi, place, e22,
           CASE
             WHEN poiDate IS NOT NULL THEN { from: poiDate.from, to: poiDate.to, objectBound: false }
             ELSE { from: objFrom.value, to: objTo.value, objectBound: true }
             END AS date
  `;

    if (query.date) {
      q +=
        'WHERE (date.from IS NULL OR date.from < date($date)) AND (date.to IS NULL OR date.to > date($date))';
    }

    // language=Cypher
    q += `
      MATCH (place)-[:P168]->(geo:E94),
            (poi)-[:P102]->(title:E35),
            (poi)-[:P3]->(content:E62)
      RETURN poi.id AS id,
             title.value AS title,
             content.value AS content,
             apoc.map.removeKey(geo, 'id') AS location,
             e22.id AS objectId,
             poi.camera AS camera,
             poi.timestamp As timestamp,
             date,
             poi.damageFactors AS damageFactors,
             'uh4d' AS type
    `;

    const params = {
      scene: query.scene,
      geo: spatialParams,
      date: query.date,
    };

    return this.neo4j.read(q, params);
  }

  async getById(poiId: string): Promise<Uh4dPoiDto | undefined> {
    // language=Cypher
    const q = `
      MATCH (poi:E73 {id: $id})-[:P2]->(:E55 {id: "poi"}),
            (poi)-[:P102]->(title:E35),
            (poi)-[:P3]->(content:E62),
            (poi)-[:P67]->(e92:E92)-[:P161]->(place:E53)-[:P168]->(geo:E94)
      OPTIONAL MATCH (e92)-[:P160]->(:E52)-[:P82]->(poiDate:E61)
      OPTIONAL MATCH (poi)-[:P67]->(e22:E22)
      OPTIONAL MATCH (e22)<-[:P108]-(:E12)-[:P4]->(:E52)-[:P82]->(objFrom:E61)
      OPTIONAL MATCH (e22)<-[:P13]-(:E6)-[:P4]->(:E52)-[:P82]->(objTo:E61)
      RETURN poi.id AS id,
             title.value AS title,
             content.value AS content,
             apoc.map.removeKey(geo, 'id') AS location,
             e22.id AS objectId,
             poi.camera AS camera,
             poi.timestamp As timestamp,
             CASE
               WHEN poiDate IS NOT NULL THEN { from: poiDate.from, to: poiDate.to, objectBound: false }
               ELSE { from: objFrom.value, to: objTo.value, objectBound: true }
             END AS date,
             poi.damageFactors AS damageFactors,
             'uh4d' AS type
    `;

    const params = {
      id: poiId,
    };

    return (await this.neo4j.read(q, params))[0];
  }

  async create(body: CreateUh4dPoiDto): Promise<Uh4dPoiDto> {
    // TODO: update camera property to lat,lon,altitude
    // language=Cypher
    const q = `
      MATCH (tpoi:E55:UH4D {id: "poi"})
      CREATE (poi:E73:UH4D {id: $id, timestamp: datetime()})-[:P67]->(e92:E92:UH4D {id: $e92id})-[:P161]->(place:E53:UH4D {id: $placeId})-[:P168]->(geo:E94:UH4D $geo),
             (poi)-[:P2]->(tpoi),
             (poi)-[:P102]->(title:E35:UH4D $title),
             (poi)-[:P3]->(content:E62:UH4D $content)
      SET poi.damageFactors = $damageFactors,
          geo.point = point({latitude: $geo.latitude, longitude: $geo.longitude})
      
      WITH poi, e92, geo, title, content
      
      CALL apoc.do.when($objectId IS NOT NULL,
        'MATCH (object:E22 {id: objectId}) CREATE (poi)-[:P67]->(object) RETURN object',
        'RETURN NULL AS object',
        { poi: poi, objectId: $objectId }) YIELD value
      
      WITH poi, e92, geo, title, content, value.object AS object
      OPTIONAL MATCH (object)<-[:P108]-(:E12)-[:P4]->(:E52)-[:P82]->(objFrom:E61)
      OPTIONAL MATCH (object)<-[:P13]-(:E6)-[:P4]->(:E52)-[:P82]->(objTo:E61)
      
      CALL apoc.do.when($objectId IS NOT NULL AND $dateMap.objectBound,
        'RETURN { from: objFrom.value, to: objTo.value } AS date, true AS objectBound',
        'CREATE (e92)-[:P160]->(:E52:UH4D {id: e52id})-[:P82]->(date:E61:UH4D {id: dateMap.id, from: CASE WHEN dateMap.from IS NULL THEN NULL ELSE date(dateMap.from) END, to: CASE WHEN dateMap.to IS NULL THEN NULL ELSE date(dateMap.to) END}) RETURN date, false AS objectBound',
        { e92: e92, e52id: $e52id, dateMap: $dateMap, objFrom: objFrom, objTo: objTo }) YIELD value AS dateValue
      
      RETURN poi.id AS id,
             title.value AS title,
             content.value AS content,
             apoc.map.removeKey(geo, 'id') as location,
             object.id AS objectId,
             [] AS camera,
             poi.timestamp As timestamp,
             { from: dateValue.date.from, to: dateValue.date.to, objectBound: dateValue.objectBound } AS date,
             poi.damageFactors AS damageFactors,
             'uh4d' AS type
    `;

    const id = nanoid(9);

    const params = {
      id: 'e73_poi_' + id,
      placeId: 'e53_poi_' + id,
      e92id: 'e92_poi_' + id,
      geo: {
        id: 'e94_poi_' + id,
        latitude: body.location.latitude,
        longitude: body.location.longitude,
        altitude: body.location.altitude,
      },
      title: {
        id: 'e35_poi_' + id,
        value: body.title,
      },
      content: {
        id: 'e62_poi_' + id,
        value: body.content,
      },
      objectId: body.objectId || null,
      damageFactors: body.damageFactors,
      e52id: 'e52_poi_' + id,
      dateMap: {
        id: 'e61_poi_' + id,
        from: body.date.from,
        to: body.date.to,
        objectBound: !!body.date.objectBound,
      },
    };

    return (await this.neo4j.write(q, params))[0];
  }

  async update(poiId: string, body: UpdateUh4dPoiDto): Promise<Uh4dPoiDto> {
    // language=Cypher
    const q = `
      MATCH (poi:E73 {id: $id})-[:P2]->(:E55 {id: "poi"}),
            (poi)-[:P102]->(title:E35),
            (poi)-[:P3]->(content:E62),
            (poi)-[:P67]->(e92:E92)-[:P161]->(place:E53)-[:P168]->(geo:E94)
      OPTIONAL MATCH (poi)-[:P67]->(e22:E22)
      OPTIONAL MATCH (e22)<-[:P108]-(:E12)-[:P4]->(:E52)-[:P82]->(objFrom:E61)
      OPTIONAL MATCH (e22)<-[:P13]-(:E6)-[:P4]->(:E52)-[:P82]->(objTo:E61)
      
      CALL apoc.do.when(e22 IS NOT NULL AND $dateMap.objectBound,
        'OPTIONAL MATCH (e92)-[:P160]->(e52:E52)-[:P82]->(date:E61) DETACH DELETE e52, date RETURN { from: objFrom.value, to: objTo.value } AS date, true AS objectBound',
        'MERGE (e92)-[:P160]->(e52:E52:UH4D)-[:P82]->(date:E61:UH4D) ON CREATE SET e52.id = e52id, date.id = dateMap.id SET date.from = CASE WHEN dateMap.from IS NULL THEN NULL ELSE date(dateMap.from) END, date.to = CASE WHEN dateMap.to IS NULL THEN NULL ELSE date(dateMap.to) END RETURN date, false AS objectBound',
        { e92: e92, e52id: $e52id, dateMap: $dateMap, objFrom: objFrom, objTo: objTo }) YIELD value AS dateValue
      
      CALL apoc.do.when($damageFactors IS NULL,
        'REMOVE poi.damageFactors RETURN poi',
        'SET poi.damageFactors = factors RETURN poi',
        { poi: poi, factors: $damageFactors }) YIELD value 
  
      SET title.value = $title,
      content.value = $content,
      poi.camera = $camera
      
      RETURN poi.id AS id,
             title.value AS title,
             content.value AS content,
             apoc.map.removeKey(geo, 'id') AS location,
             e22.id AS objectId,
             poi.camera AS camera,
             poi.timestamp As timestamp,
             { from: dateValue.date.from, to: dateValue.date.to, objectBound: dateValue.objectBound } AS date,
             poi.damageFactors AS damageFactors,
             'uh4d' AS type
    `;

    const id = nanoid(9);

    const params = {
      id: poiId,
      title: body.title,
      content: body.content,
      e52id: 'e52_poi_' + id,
      dateMap: {
        id: 'e61_poi_' + id,
        from: body.date.from,
        to: body.date.to,
        objectBound: !!body.date.objectBound,
      },
      camera: body.camera,
      damageFactors: body.damageFactors || null,
    };

    return (await this.neo4j.write(q, params))[0];
  }

  /**
   * Remove point of interest.
   * @return Resolves with `true` if operation was successful.
   */
  async remove(poiId: string): Promise<boolean> {
    // language=Cypher
    const q = `
      MATCH (poi:E73 {id: $id})-[:P2]->(:E55 {id: "poi"}),
            (poi)-[:P102]->(title:E35),
            (poi)-[:P3]->(content:E62),
            (poi)-[:P67]->(e92:E92)-[:P161]->(place:E53)-[:P168]->(geo:E94)
      OPTIONAL MATCH (e92)-[:P160]->(e52:E52)-[:P82]->(date:E61)
      DETACH DELETE poi, place, geo, title, content, e92, e52, date
      RETURN true AS check
    `;

    const params = {
      id: poiId,
    };

    return !!(await this.neo4j.write(q, params))[0];
  }

  async queryByType(
    type: string,
    queryParams: GetPoisQueryParams,
  ): Promise<(GeneralPoiDto | Uh4dPoiDto)[]> {
    switch (type) {
      case 'uh4d':
        return this.query(queryParams);
      case 'wikidata':
        return this.queryWikidata(queryParams);
      default:
        return [];
    }
  }

  async queryWikidata(query: GetPoisQueryParams): Promise<GeneralPoiDto[]> {
    const latitude = parseFloat(query.lat);
    const longitude = parseFloat(query.lon);
    const radius = parseFloat(query.r);

    if (isNaN(latitude) || isNaN(longitude) || isNaN(radius)) {
      throw new BadRequestException(
        'Wikidata query requires lat|lon|r query parameters',
      );
    }

    const wdBuilding = 'Q41176';
    const wdPointOfInterest = 'Q960648';
    const wIds = [wdBuilding, wdPointOfInterest].map((wd) => 'wd:' + wd);

    // language=Sparql
    const sparql = `
      SELECT ?item ?itemLabel ?lat ?lon ?sitelink WHERE {
        ?item wdt:P31/wdt:P279* ?wId .
        FILTER (?wId IN (${wIds.join(', ')}))
        ?item p:P625 ?statement .
        ?statement psv:P625 ?coordinate_node .
        ?coordinate_node wikibase:geoLatitude ?lat .
        ?coordinate_node wikibase:geoLongitude ?lon .
        OPTIONAL {
            ?sitelinkDe schema:about ?item .
            ?sitelinkDe schema:isPartOf <https://de.wikipedia.org/> .
            ?item rdfs:label ?labelDe FILTER (LANG(?labelDe) = "de")
        }
        OPTIONAL {
            ?sitelinkEn schema:about ?item .
            ?sitelinkEn schema:isPartOf <https://en.wikipedia.org/> .
            ?item rdfs:label ?labelEn FILTER (LANG(?labelEn) = "en")
        }
        BIND (COALESCE(?sitelinkDe, ?sitelinkEn) AS ?sitelink)
        BIND (COALESCE(?labelDe, ?labelEn) AS ?itemLabel)
        FILTER (BOUND(?itemLabel) && BOUND(?sitelink))
        SERVICE wikibase:around {
          ?item wdt:P625 ?location .
          bd:serviceParam wikibase:center "Point(${longitude} ${latitude})"^^geo:wktLiteral .
          bd:serviceParam wikibase:radius "${radius / 1000}".
        }
      }
      GROUP BY ?item ?itemLabel ?lat ?lon ?sitelink
    `;

    const response = await this.http.axiosRef.get(
      'https://query.wikidata.org/sparql?query=' + encodeURIComponent(sparql),
      { headers: { Accept: 'application/sparql-results+json' } },
    );
    const results: any[] = response.data.results.bindings;

    // map to POI format
    return results.map((r) => ({
      id: r.item.value,
      title: r.itemLabel.value,
      content: r.sitelink.value,
      location: {
        latitude: parseFloat(r.lat.value),
        longitude: parseFloat(r.lon.value),
        altitude: 0,
      },
      type: 'wikidata',
    }));
  }
}
