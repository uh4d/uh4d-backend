import {
  BadRequestException,
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  NotFoundException,
  Param,
  Patch,
  Post,
  Query,
  Res,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiExtraModels,
  ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
  getSchemaPath,
} from '@nestjs/swagger';
import { Response } from 'express';
import { CreateUh4dPoiDto, Uh4dPoiDto, UpdateUh4dPoiDto } from '@dto/uh4d-poi';
import { GeneralPoiDto } from '@dto/general-poi';
import { PoisService } from './pois.service';
import { GetPoisQueryParams } from './dto/get-pois-query-params';

@ApiTags('Points of Interest')
@Controller('pois')
export class PoisController {
  constructor(private readonly poiService: PoisService) {}

  @ApiOperation({
    summary: 'Query points of interest',
    description:
      'Query all points of interest. Query parameters may be set to filter the data.\n\n`lat`, `lon`, and `r` only work in combination and may be used to look for points of interest within a radius at a position.\n\n`type` specifies the source API/database the points of interests should be queried from (provided respective retrieval script). If omitted, only UH4D points of interest will be queried (equals `type=uh4d`). Multiple types are possible, e.g., `?type=uh4d&type=wikidata`.',
  })
  @ApiExtraModels(Uh4dPoiDto, GeneralPoiDto)
  @ApiOkResponse({
    schema: {
      type: 'array',
      items: {
        oneOf: [
          { $ref: getSchemaPath(Uh4dPoiDto) },
          { $ref: getSchemaPath(GeneralPoiDto) },
        ],
      },
    },
  })
  @Get()
  async query(
    @Query() queryParams: GetPoisQueryParams,
  ): Promise<(GeneralPoiDto | Uh4dPoiDto)[]> {
    // parse types
    const types = queryParams.type
      ? Array.isArray(queryParams.type)
        ? queryParams.type
        : [queryParams.type]
      : ['uh4d'];

    // iterate over types and execute query
    const promises = types.map((type) =>
      this.poiService.queryByType(type, queryParams),
    );

    // concatenate lists of POIs
    const results = await Promise.all(promises);
    return results.reduce((previousValue, currentValue) =>
      previousValue.concat(currentValue),
    );
  }

  @ApiOperation({
    summary: 'Get point of interest',
    description:
      "Get point of interest by ID (only possible for `type: 'uh4d'`).",
  })
  @ApiOkResponse({
    type: Uh4dPoiDto,
  })
  @ApiNotFoundResponse()
  @Get(':poiId')
  async getById(@Param('poiId') poiId: string): Promise<Uh4dPoiDto> {
    const poi = await this.poiService.getById(poiId);

    if (!poi) {
      throw new NotFoundException(`POI with ID ${poiId} not found!`);
    }

    return poi;
  }

  @ApiOperation({
    summary: 'Save point of interest',
    description: 'Save new point of interest at position.',
  })
  @ApiCreatedResponse({
    type: Uh4dPoiDto,
  })
  @ApiBadRequestResponse()
  @Post()
  async create(@Body() body: CreateUh4dPoiDto): Promise<Uh4dPoiDto> {
    const poi = await this.poiService.create(body);

    if (!poi) {
      throw new BadRequestException(`No POI created!`);
    }

    return poi;
  }

  @ApiOperation({
    summary: 'Update point of interest',
    description:
      "Update properties of point of interest (only possible for `type: 'uh4d'`). At the moment, only `title`, `content`, `date` properties, and `camera` are updated. Location/position updates are not yet supported.",
  })
  @ApiOkResponse({
    type: Uh4dPoiDto,
  })
  @ApiBadRequestResponse()
  @ApiNotFoundResponse()
  @Patch(':poiId')
  async update(
    @Param('poiId') poiId: string,
    @Body() body: UpdateUh4dPoiDto,
  ): Promise<Uh4dPoiDto> {
    const poi = await this.poiService.update(poiId, body);

    if (!poi) {
      throw new NotFoundException(`POI with ID ${poiId} not found!`);
    }

    return poi;
  }

  @ApiOperation({
    summary: 'Delete point of interest',
    description:
      "Delete point of interest by ID (only possible for `type: 'uh4d'`).",
  })
  @ApiNoContentResponse()
  @ApiNotFoundResponse()
  @HttpCode(HttpStatus.NO_CONTENT)
  @Delete(':poiId')
  async remove(
    @Param('poiId') poiId: string,
    @Res() res: Response,
  ): Promise<void> {
    const removed = await this.poiService.remove(poiId);

    if (!removed) {
      throw new NotFoundException(`POI with ID ${poiId} not found!`);
    }

    res.send();
  }
}
