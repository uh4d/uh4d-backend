import { Controller, DefaultValuePipe, Get, Query } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';
import { TagsService } from './tags.service';
import { TagDto } from './dto/tag.dto';

@ApiTags('Metadata')
@Controller('tags')
export class TagsController {
  constructor(private readonly tagService: TagsService) {}

  @ApiOperation({
    summary: 'Query tags',
    description:
      'Query all tags. Use `search` query parameter to filter results.',
  })
  @ApiOkResponse({
    description: 'List of tags',
    type: TagDto,
    isArray: true,
  })
  @Get()
  async query(
    @Query('search', new DefaultValuePipe('')) search: string,
  ): Promise<TagDto[]> {
    return this.tagService.query(search);
  }
}
