import { ApiProperty } from '@nestjs/swagger';

export class TagDto {
  @ApiProperty({
    description: 'Tag value',
    required: false,
  })
  value: string;
}
