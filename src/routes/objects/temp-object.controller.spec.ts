import { Test, TestingModule } from '@nestjs/testing';
import { TempObjectController } from './temp-object.controller';

describe('TempObjectController', () => {
  let controller: TempObjectController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TempObjectController],
    }).compile();

    controller = module.get<TempObjectController>(TempObjectController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
