import { Module } from '@nestjs/common';
import { OverpassModule } from '@routes/overpass/overpass.module';
import { ObjectsController } from './objects.controller';
import { ObjectsService } from './objects.service';
import { TempObjectController } from './temp-object.controller';

@Module({
  imports: [OverpassModule],
  controllers: [ObjectsController, TempObjectController],
  providers: [ObjectsService],
})
export class ObjectsModule {}
