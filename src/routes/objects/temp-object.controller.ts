import {
  Controller,
  Delete,
  HttpCode,
  HttpStatus,
  NotFoundException,
  Param,
  Post,
  Res,
  UnsupportedMediaTypeException,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBody,
  ApiConsumes,
  ApiCreatedResponse,
  ApiNoContentResponse,
  ApiNotFoundResponse,
  ApiOperation,
  ApiTags,
  ApiUnsupportedMediaTypeResponse,
} from '@nestjs/swagger';
import { ConfigService } from '@nestjs/config';
import { FileInterceptor } from '@nestjs/platform-express';
import { Express, Response } from 'express';
import { join } from 'node:path';
import * as fs from 'fs-extra';
import { nanoid } from 'nanoid';
import { lookup } from 'mime-types';
import { replace } from '@utils/string-utils';
import { TempObjectDto } from '@routes/objects/dto/temp-object.dto';
import { ObjectMediaService } from '@upload/object-media.service';
import { UploadObjectDto } from '@routes/objects/dto/upload-object.dto';

@ApiTags('Objects')
@Controller('objects/tmp')
export class TempObjectController {
  constructor(
    private readonly config: ConfigService,
    private readonly objectMediaService: ObjectMediaService,
  ) {}

  @ApiOperation({
    summary: 'Upload object',
    description: 'Upload object to temporary location.',
  })
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    type: UploadObjectDto,
  })
  @ApiCreatedResponse({
    type: TempObjectDto,
  })
  @ApiUnsupportedMediaTypeResponse()
  @Post()
  @UseInterceptors(FileInterceptor('uploadModel'))
  async upload(
    @UploadedFile() file: Express.Multer.File,
  ): Promise<TempObjectDto> {
    const id = nanoid(9);
    const relPath = `tmp/${id}/`;
    const absPath = join(this.config.get<string>('path.data'), relPath);
    const fileName = replace(file.originalname);

    try {
      // create tmp folder
      await fs.ensureDir(absPath);
      // copy uploaded file to tmp folder
      await fs.rename(file.path, join(absPath, fileName));

      const responseBody = {
        id,
        path: relPath,
        original: fileName,
      };

      switch (lookup(fileName)) {
        case 'application/zip': {
          // process zip
          const { name, gltf, type } =
            await this.objectMediaService.generateGltfFromZip({
              path: absPath,
              name: fileName,
            });
          return Object.assign(responseBody, { name, file: gltf, type });
        }
        case 'model/obj': {
          const { name, gltf, type } =
            await this.objectMediaService.generateGltfFromObj({
              path: absPath,
              name: fileName,
            });
          return Object.assign(responseBody, { name, file: gltf, type });
        }
        default:
          throw new UnsupportedMediaTypeException();
      }
    } catch (e) {
      // cleanup paths
      await fs.remove(file.path);
      await fs.remove(absPath);

      throw e;
    }
  }

  @ApiOperation({
    summary: 'Delete temporary object',
    description: 'Delete all files of temporary object.',
  })
  @ApiNoContentResponse()
  @ApiNotFoundResponse()
  @HttpCode(HttpStatus.NO_CONTENT)
  @Delete(':tmpId')
  async remove(
    @Param('tmpId') tmpId: string,
    @Res() res: Response,
  ): Promise<void> {
    const absPath = join(this.config.get<string>('path.data'), 'tmp', tmpId);
    const exists = await fs.pathExists(absPath);

    if (!exists) {
      throw new NotFoundException(`Temporary folder ${tmpId} does not exist!`);
    }

    await fs.remove(absPath);

    res.send();
  }
}
