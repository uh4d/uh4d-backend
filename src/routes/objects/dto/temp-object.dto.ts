import { ApiProperty } from '@nestjs/swagger';

export class TempObjectDto {
  @ApiProperty({ required: false })
  id: string;
  @ApiProperty({ required: false })
  path: string;
  @ApiProperty({ required: false })
  original: string;
  @ApiProperty({ required: false })
  name: string;
  @ApiProperty({ required: false })
  file: string;
  @ApiProperty({ required: false })
  type: string;
}
