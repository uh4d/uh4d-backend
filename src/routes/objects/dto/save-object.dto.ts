import { ApiProperty } from '@nestjs/swagger';
import {
  IsArray,
  IsLatitude,
  IsLongitude,
  IsNotEmpty,
  IsNumber,
  IsString,
} from 'class-validator';

export class SaveObjectDto {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  id: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  name: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  path: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  file: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  original: string;
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  type: string;
  @ApiProperty({ format: 'float' })
  @IsNotEmpty()
  @IsNumber()
  @IsLatitude()
  latitude: number;
  @ApiProperty({ format: 'float' })
  @IsNotEmpty()
  @IsNumber()
  @IsLongitude()
  longitude: number;
  @ApiProperty({ format: 'float' })
  @IsNotEmpty()
  @IsNumber()
  altitude: number;
  @ApiProperty({ format: 'float' })
  @IsNotEmpty()
  @IsNumber()
  omega: number;
  @ApiProperty({ format: 'float' })
  @IsNotEmpty()
  @IsNumber()
  phi: number;
  @ApiProperty({ format: 'float' })
  @IsNotEmpty()
  @IsNumber()
  kappa: number;
  @ApiProperty({ type: 'array', items: { type: 'number', format: 'float' } })
  @IsArray()
  @IsNumber({}, { each: true })
  scale: number[];
}
