import { IntersectionType } from '@nestjs/swagger';
import { ObjectDto, UpdateObjectDto } from '@dto/object';
import { ObjectMetaDto, UpdateObjectMetaDto } from '@dto/object-meta';

export class ObjectFullDto extends IntersectionType(ObjectDto, ObjectMetaDto) {}

export class UpdateObjectFullDto extends IntersectionType(
  UpdateObjectDto,
  UpdateObjectMetaDto,
) {}
