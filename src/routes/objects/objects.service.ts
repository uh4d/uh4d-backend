import {
  BadRequestException,
  ConflictException,
  Injectable,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Neo4jService } from '@brakebein/nest-neo4j';
import * as fs from 'fs-extra';
import { join } from 'node:path';
import { v4 as uuid } from 'uuid';
import { nanoid } from 'nanoid';
import {
  BufferGeometry,
  Euler,
  Matrix4,
  Mesh,
  MeshBasicMaterial,
  Quaternion,
  Vector3,
} from 'three';
import { loadGltf } from 'node-three-gltf';
import { replace } from '@utils/string-utils';
import * as BufferGeometryUtils from '@utils/three/buffer-geometry-utils';
import { processOsmData } from '@utils/osm-processing';
import { Coordinates } from '@utils/coordinates';
import { computeModelCenter, testIntersection } from '@utils/three-utils';
import { OverpassService } from '@routes/overpass/overpass.service';
import { ObjectDto } from '@dto/object';
import { GetObjectsQueryParams } from './dto/get-objects-query-params';
import { ObjectFullDto, UpdateObjectFullDto } from './dto/object-full.dto';
import { CreateDuplicateDto } from './dto/create-duplicate.dto';
import { SaveObjectDto } from './dto/save-object.dto';

@Injectable()
export class ObjectsService {
  private readonly logger = new Logger(ObjectsService.name);

  constructor(
    private readonly neo4j: Neo4jService,
    private readonly config: ConfigService,
    private readonly overpassService: OverpassService,
  ) {}

  query(query: GetObjectsQueryParams): Promise<ObjectDto[]> {
    const spatialParams = {
      latitude: parseFloat(query.lat),
      longitude: parseFloat(query.lon),
      radius: parseFloat(query.r),
    };
    const doSpatialQuery =
      !isNaN(spatialParams.latitude) &&
      !isNaN(spatialParams.longitude) &&
      !isNaN(spatialParams.radius);

    let q = '';

    if (doSpatialQuery) {
      q += `WITH point({latitude: $geo.latitude, longitude: $geo.longitude}) AS userPoint `;
    }

    q += 'MATCH ' + (query.scene ? '(:E53:UH4D {id: $scene})<-[:P89]-' : '');
    q += `(place:E53)-[:P157]->(e22:E22:UH4D)-[:P1]->(name:E41),
          (e22)<-[:P67]-(object:D1)-[:P106]->(file:D9),
          (place)-[:P168]->(geo:E94) `;

    if (doSpatialQuery) {
      q += `WHERE distance(geo.point, userPoint) < $geo.radius`;
    }

    q += `
      OPTIONAL MATCH (e22)<-[:P108]-(:E12)-[:P4]->(:E52)-[:P82]->(from:E61)
      OPTIONAL MATCH (e22)<-[:P13]-(:E6)-[:P4]->(:E52)-[:P82]->(to:E61)`;

    if (query.date) {
      q += `
        WITH e22, name, object, file, from, to, geo, place
        WHERE (from IS NULL OR from.value < date($date)) AND (to IS NULL OR to.value > date($date))`;
    }

    q += ` OPTIONAL MATCH (place)-[:P121]->(osmPlace:E53)`;

    q += `
      RETURN e22.id AS id,
             name.value AS name,
             {from: from.value, to: to.value} AS date,
             apoc.map.removeKeys(object, ['id', 'vrcity_forceTextures']) AS origin,
             apoc.map.removeKey(file, 'id') AS file,
             apoc.map.removeKey(geo, 'id') AS location,
             object.vrcity_forceTextures AS vrcity_forceTextures,
             collect(osmPlace.osm) AS osm_overlap`;

    const params = {
      scene: query.scene,
      geo: spatialParams,
      date: query.date,
    };

    return this.neo4j.read(q, params);
  }

  async getById(objectId: string): Promise<ObjectFullDto | undefined> {
    // language=Cypher
    const q = `
      MATCH (e22:E22:UH4D {id: $id})-[:P1]->(name:E41),
            (e22)<-[:P67]-(object:D1)-[:P106]->(file:D9),
            (e22)<-[:P157]-(place:E53)-[:P168]->(geo:E94)
      OPTIONAL MATCH (e22)<-[:P108]-(:E12)-[:P4]->(:E52)-[:P82]->(from:E61)
      OPTIONAL MATCH (e22)<-[:P13]-(:E6)-[:P4]->(:E52)-[:P82]->(to:E61)
      OPTIONAL MATCH (e22)-[:P3]->(misc:E62)
      OPTIONAL MATCH (place)-[:P87]->(addr:E45)
      OPTIONAL MATCH (addr)-[:P139]-(fAddr:E45)
      WITH e22, name, misc, from, to, object, file, geo, addr, place, collect(fAddr.value) AS formerAddresses
      OPTIONAL MATCH (place)-[:P121]->(osmPlace:E53)
      WITH e22, name, misc, from, to, object, file, geo, addr, formerAddresses, collect(osmPlace.osm) AS osm_overlap
      OPTIONAL MATCH (e22)-[:has_tag]->(tag:TAG)
      RETURN e22.id AS id,
             name.value AS name,
             {from: from.value, to: to.value} AS date,
             apoc.map.removeKeys(object, ['id', 'vrcity_forceTextures']) AS origin,
             apoc.map.removeKey(file, 'id') AS file,
             apoc.map.removeKey(geo, 'id') AS location,
             object.vrcity_forceTextures AS vrcity_forceTextures,
             misc.value AS misc,
             addr.value as address,
             formerAddresses,
             osm_overlap,
             collect(tag.id) AS tags
   `;

    const params = {
      id: objectId,
    };

    return (await this.neo4j.read(q, params))[0];
  }

  /**
   * Move uploaded files from temp folder to final destination and store in database.
   */
  async create(body: SaveObjectDto): Promise<ObjectFullDto> {
    // check if tmp path exists
    const tmpPath = join(this.config.get('path.data'), 'tmp', body.id);

    const exists = await fs.pathExists(tmpPath);
    if (!exists) {
      throw new BadRequestException(
        `Temporary folder ${body.id} does not exist!`,
      );
    }

    // compute center
    const location = new Coordinates(
      body.latitude,
      body.longitude,
      body.altitude,
    );
    const scale = new Vector3(1, 1, 1);
    const rotation = new Euler(body.omega, body.phi, body.kappa, 'YXZ');
    if (body.scale) {
      const s = body.scale;
      if (typeof s === 'number') {
        scale.set(s, s, s);
      } else if (Array.isArray(s) && s.length === 3) {
        scale.fromArray(s);
      } else {
        body.scale = undefined;
      }
    }
    const matrix = new Matrix4().compose(
      new Vector3(),
      new Quaternion().setFromEuler(rotation),
      scale,
    );
    const center = await computeModelCenter(join(tmpPath, body.file), matrix);
    location.add(center);

    // parameters
    const id = nanoid(9);
    const relPath = `objects/${uuid()}/`;
    const absPath = join(this.config.get<string>('path.data'), relPath);

    const params = {
      e22id: 'e22_' + id,
      placeId: 'e53_' + id,
      e41: {
        id: 'e41_' + id,
        value: body.name,
      },
      dobj: {
        id: 'd1_' + id,
        latitude: body.latitude,
        longitude: body.longitude,
        altitude: body.altitude,
        omega: body.omega,
        phi: body.phi,
        kappa: body.kappa,
        scale: body.scale,
      },
      file: {
        id: 'd9_' + id,
        path: relPath,
        file: body.file,
        type: body.type,
        original: body.original,
      },
      geo: {
        id: 'e94_' + id,
        ...location.toJSON(),
      },
    };

    // language=Cypher
    const q = `
      CREATE (e22:E22:UH4D {id: $e22id})-[:P1]->(name:E41:UH4D $e41),
             (e22)<-[:P67]-(object:D1:UH4D $dobj)-[:P106]->(file:D9:UH4D $file),
             (e22)<-[:P157]-(place:E53:UH4D {id: $placeId}),
             (place)-[:P168]->(geo:E94:UH4D $geo)
      SET geo.point = point({latitude: $geo.latitude, longitude: $geo.longitude})
      RETURN e22.id AS id,
             name.value AS name,
             {from: NULL, to: NULL} AS date,
             apoc.map.removeKey(object, 'id') AS origin,
             apoc.map.removeKey(file, 'id') AS file,
             apoc.map.removeKey(geo, 'id') AS location,
             NULL AS misc,
             NULL as address,
             [] AS formerAddresses,
             [] AS tags
    `;

    try {
      // move to final location
      this.logger.log(`Move: ${tmpPath} ${absPath}`);
      await fs.move(tmpPath, absPath);

      const result = (await this.neo4j.write(q, params))[0];

      if (result) {
        // update OSM intersections
        result.osm_overlap = await this.updateOsmIntersections(result.id);
        return result;
      } else {
        throw new Error('No nodes added for ' + body.file);
      }
    } catch (e) {
      // cleanup paths
      await fs.remove(tmpPath);
      await fs.remove(absPath);

      throw e;
    }
  }

  async update(
    objectId: string,
    body: UpdateObjectFullDto,
    prop: string,
  ): Promise<{ object: { id: string } } | undefined> {
    let q = `MATCH (e22:E22:UH4D {id: $id})<-[:P157]-(place:E53) `;

    const params: any = {
      id: objectId,
    };

    const id = nanoid(9);

    switch (prop) {
      case 'name':
        q += `MATCH (e22)-[:P1]->(name:E41) SET name.value = $name`;
        params.name = body.name;
        break;

      case 'from':
        if (body.date.from) {
          // set from date
          q += `
            MERGE (e22)<-[:P108]-(e12:E12:UH4D)-[:P4]->(e52:E52:UH4D)-[:P82]->(date:E61:UH4D)
            ON CREATE SET e12.id = $e12id, e52.id = $e52id, date.id = $e61id
            SET date.value = date($date)`;
          params.e12id = 'e12_' + id;
          params.e52id = 'e52_e12_' + id;
          params.e61id = 'e61_e52_e12_' + id;
          params.date = body.date.from;
        } else {
          // remove date
          q += `
            OPTIONAL MATCH (e22)<-[:P108]-(e12:E12)-[:P4]->(e52:E52)-[:P82]->(from:E61)
            DETACH DELETE e12, e52, from`;
        }
        break;

      case 'to':
        if (body.date.to) {
          // set to date
          q += `
            MERGE (e22)<-[:P13]-(e6:E6:UH4D)-[:P4]->(e52:E52:UH4D)-[:P82]->(date:E61:UH4D)
            ON CREATE SET e6.id = $e6id, e52.id = $e52id, date.id = $e61id
            SET date.value = date($date)`;
          params.e6id = 'e6_' + id;
          params.e52id = 'e52_e6_' + id;
          params.e61id = 'e61_e52_e6_' + id;
          params.date = body.date.to;
        } else {
          // remove date
          q += `
            OPTIONAL MATCH (e22)<-[:P13]-(e6:E6)-[:P4]->(e52:E52)-[:P82]->(to:E61)
            DETACH DELETE e6, e52, to`;
        }
        break;

      case 'misc':
        if (body.misc) {
          // set misc
          q += `
            MERGE (e22)-[:P3]->(misc:E62)
            ON CREATE SET misc.id = $misc.id
            SET misc.value = $misc.value`;
          params.misc = { id: 'e62_misc_' + id, value: body.misc };
        } else {
          // remove misc
          q += `OPTIONAL MATCH (e22)-[:P3]->(misc:E62) DETACH DELETE misc`;
        }
        break;

      case 'address':
        q += `
          OPTIONAL MATCH (place)-[r:P87]->(:E45)
          DELETE r
          MERGE (addr:E45:UH4D {value: $address.value})
          ON CREATE SET addr.id = $address.id
          MERGE (place)-[:P87]->(addr)`;
        params.address = { id: 'e45_' + id, value: body.address };
        break;

      case 'tags':
        q += `
          OPTIONAL MATCH (e22)-[r:has_tag]->(:TAG)
          DELETE r
          WITH e22
          FOREACH (tag IN $tags |
            MERGE (t:TAG:UH4D {id: tag})
            MERGE (e22)-[:has_tag]->(t)
          )`;
        params.tags = body.tags || [];
        break;

      case 'origin': {
        // compute new center
        const rotation = new Euler(
          body.origin.omega,
          body.origin.phi,
          body.origin.kappa,
          'YXZ',
        );
        const scale = new Vector3(1, 1, 1);
        if (body.origin.scale) {
          const s = body.origin.scale as number | number[];
          if (typeof s === 'number') {
            scale.set(s, s, s);
          } else if (Array.isArray(s) && s.length === 3) {
            scale.fromArray(s);
          } else {
            body.origin.scale = undefined;
          }
        }
        const matrix = new Matrix4().compose(
          new Vector3(),
          new Quaternion().setFromEuler(rotation),
          scale,
        );
        const filePath = join(
          this.config.get<string>('path.data'),
          body.file.path,
          body.file.file,
        );
        const center = await computeModelCenter(filePath, matrix);
        body.location = new Coordinates(
          body.origin.latitude,
          body.origin.longitude,
          body.origin.altitude,
        )
          .add(center)
          .toJSON();

        q += `
          MATCH (e22)<-[:P67]-(object:D1), (place)-[:P168]->(geo:E94)
          OPTIONAL MATCH (object)-[:P67]->(:E22)<-[:P157]-(:E53)-[:P168]->(geoDuplicate:E94)
          SET object += $origin,
              geo += $location,
              geoDuplicate += $location,
              geo.point = point({latitude: $location.latitude, longitude: $location.longitude}),
              geoDuplicate.point = point({latitude: $location.latitude, longitude: $location.longitude})
        `;
        params.origin = body.origin;
        params.location = body.location;
        break;
      }

      case 'vrcity_forceTextures':
        q += `
          MATCH (e22)<-[:P67]-(object:D1)
          SET object.vrcity_forceTextures = $forceTextures
        `;
        params.forceTextures = !!body.vrcity_forceTextures;
        break;
    }

    q += ' RETURN e22 AS object';

    const result = (await this.neo4j.write(q, params))[0];

    if (!result) return undefined;

    // execute tasks after database update
    switch (prop) {
      case 'origin':
        await this.updateOsmIntersections(objectId);
        await this.cleanOsmData();
    }

    return result;
  }

  /**
   * Remove object from database and delete files.
   * However, files won't be deleted if there is a duplicate, i.e. other objects referencing the files.
   */
  async remove(objectId: string): Promise<void> {
    // detach POI from object
    // language=Cypher
    const qPoi = `
      MATCH (e22:E22:UH4D {id: $id})<-[:P67]-(poi:E73)-[:P2]-(:E55 {id: "poi"}),
            (poi)-[:P67]->(e92:E92)
      OPTIONAL MATCH (e22)<-[:P108]-(:E12)-[:P4]->(:E52)-[:P82]->(from:E61)
      OPTIONAL MATCH (e22)<-[:P13]-(:E6)-[:P4]->(:E52)-[:P82]->(to:E61)
      MERGE (e92)-[:P160]->(e52:E52:UH4D)-[:P82]->(date:E61:UH4D)
        ON CREATE SET e52.id = replace(poi.id, "e73_", "e52_"), date.id = replace(poi.id, "e73_", "e61_")
      SET date.from = CASE WHEN from IS NULL THEN NULL ELSE from.value END,
      date.to = CASE WHEN to IS NULL THEN NULL ELSE to.value END
      RETURN poi
    `;

    // language=Cypher
    const qRemove = `
      MATCH (e22:E22:UH4D {id: $id})-[:P1]->(name:E41),
            (e22)<-[:P67]-(object:D1)-[:P106]->(file:D9),
            (e22)<-[:P157]-(place)-[:P168]->(geo:E94)
      OPTIONAL MATCH (object)-[:P67]->(duplicate:E22) WHERE NOT e22 = duplicate
      WITH e22, place, name, object, file, geo, duplicate, file.path AS path
      CALL apoc.do.when(duplicate IS NULL, 'DETACH DELETE object, file RETURN 1 AS check', '', { object: object, file: file }) YIELD value
      OPTIONAL MATCH (e22)<-[:P108]-(e12:E12)-[:P4]->(e12e52:E52)-[:P82]->(from:E61)
      OPTIONAL MATCH (e22)<-[:P13]-(e6:E6)-[:P4]->(e6e52:E52)-[:P82]->(to:E61)
      OPTIONAL MATCH (e22)-[:P3]->(misc:E62)
      DETACH DELETE e22, place, geo, name, e12, e12e52, from, e6, e6e52, to, misc
      RETURN path, duplicate`;

    const params = {
      id: objectId,
    };

    // wrap in custom transaction
    const session = this.neo4j.getWriteSession();
    const txc = session.beginTransaction();

    try {
      // execute cypher queries
      await txc.run(qPoi, params);

      const result = await txc.run(qRemove, params);
      const records = Neo4jService.extractRecords<{
        path: string;
        duplicate: { id: string };
      }>(result.records);

      if (!records[0]) {
        throw new NotFoundException(`Object with ID ${objectId} not found!`);
      }

      // check for corrupted path to not remove wrong directory
      const relPath = records[0].path;
      if (!/^objects\/\S+$/.test(relPath)) {
        throw new ConflictException('Path to delete does not match pattern!');
      }

      // remove if there is no duplicate
      if (!records[0].duplicate) {
        this.logger.log('Remove: ' + relPath);
        await fs.remove(join(this.config.get<string>('path.data'), relPath));
      }

      await txc.commit();
    } catch (e) {
      await txc.rollback();
      throw e;
    } finally {
      await session.close();
    }
  }

  async duplicate(
    objectId: string,
    body: CreateDuplicateDto,
  ): Promise<ObjectFullDto | undefined> {
    // language=Cypher
    const q = `
      MATCH (original:E22:UH4D {id: $id})<-[:P67]-(object:D1)-[:P106]->(file:D9),
            (original)<-[:P157]-(place:E53)-[:P168]->(geo:E94)
      OPTIONAL MATCH (place)-[:P89]->(scene:E53)
      CREATE (e22:E22:UH4D {id: $e22id})-[:P1]->(name:E41:UH4D $e41),
             (e22)<-[:P157]-(placeNew:E53:UH4D {id: $e53id})-[:P89]->(scene),
             (placeNew)-[:P168]->(geoNew:E94 {id: $e94id}),
             (e22)<-[:P67]-(object)
      SET geoNew += apoc.map.removeKey(geo, 'id')
      CALL apoc.do.when(scene IS NOT NULL, 'CREATE (place)-[:P89]->(scene)', '', {place: placeNew, scene: scene}) YIELD value
      RETURN e22.id AS id,
             name.value AS name,
             {from: NULL, to: NULL} AS date,
             apoc.map.removeKeys(object, ['id', 'vrcity_forceTextures']) AS origin,
             apoc.map.removeKey(file, 'id') AS file,
             apoc.map.removeKey(geoNew, 'id') AS location,
             object.vrcity_forceTextures AS vrcity_forceTextures,
             NULL AS misc,
             NULL as address,
             [] AS formerAddresses,
             [] AS tags,
             [] AS osm_overlap
    `;

    const id = nanoid(9) + '_' + replace(body.name) + '_duplicate';

    const params = {
      id: objectId,
      e22id: 'e22_' + id,
      e41: {
        id: 'e41_' + id,
        value: body.name + ' Duplicate',
      },
      e53id: 'e53_' + id,
      e94id: 'e94_' + id,
    };

    return (await this.neo4j.write(q, params))[0];
  }

  async updateOsmIntersections(objectId: string): Promise<number[]> {
    // query object
    // language=Cypher
    const q0 = `
      MATCH (e22:E22:UH4D {id: $id})<-[:P67]-(object:D1)-[:P106]->(file:D9),
            (e22)<-[:P157]-(place:E53)-[:P168]->(geo:E94)
      RETURN e22.id AS id, object AS origin, file, geo AS location
    `;

    const item = (await this.neo4j.read(q0, { id: objectId }))[0];

    if (!item) {
      throw new Error(`Cannot find object with ID ${objectId}`);
    }

    // load gltf file
    const gltf = await loadGltf(
      join(
        this.config.get<string>('path.data'),
        item.file.path,
        item.file.file,
      ),
    );

    // merge geometries
    const geometries: BufferGeometry[] = [];
    gltf.scene.traverse((child) => {
      if (child instanceof Mesh) {
        geometries.push(child.geometry);
      }
    });

    const geo = BufferGeometryUtils.mergeBufferGeometries(geometries);
    const mesh = new Mesh(geo, new MeshBasicMaterial());
    mesh.rotation.set(
      item.origin.omega,
      item.origin.phi,
      item.origin.kappa,
      'YXZ',
    );
    if (item.origin.scale) {
      if (Array.isArray(item.origin.scale)) {
        mesh.scale.fromArray(item.origin.scale);
      } else {
        mesh.scale.set(item.origin.scale, item.origin.scale, item.origin.scale);
      }
    }
    geo.computeBoundingSphere();

    // query Overpass API
    const result = await this.overpassService.query(
      item.location.latitude,
      item.location.longitude,
      geo.boundingSphere.radius,
    );
    const origin = new Coordinates(
      item.origin.latitude,
      item.origin.longitude,
      item.origin.altitude,
    );

    // generate OSM buildings
    const buildings = processOsmData(result, origin);

    // test object and OSM buildings against each other
    const osmIds: number[] = [];

    for (const building of buildings) {
      // test intersection
      if (
        testIntersection(mesh, building) ||
        testIntersection(building, mesh)
      ) {
        if (!osmIds.includes(building.userData.osmId)) {
          osmIds.push(building.userData.osmId);
        }
      }

      // dispose OSM mesh
      building.geometry.dispose();
      building.material.dispose();
    }

    // dispose object mesh
    mesh.geometry.dispose();
    mesh.material.dispose();

    // update database
    // language=Cypher
    const q1 = `
      MATCH (e22:E22:UH4D {id: $id})<-[:P157]-(place:E53)
      OPTIONAL MATCH (place)-[r:P121]->(:E53)
      DELETE r
      WITH place
      UNWIND $osmIds AS osmId
      MERGE (osmPlace:E53:UH4D {id: 'e53_osm_' + toInteger(osmId), osm: toInteger(osmId)})
      MERGE (place)-[:P121]->(osmPlace)
    `;

    await this.neo4j.write(q1, { id: objectId, osmIds });

    return osmIds;
  }

  /**
   * Delete OSM place nodes that are not connected anymore.
   */
  async cleanOsmData(): Promise<void> {
    // language=Cypher
    const q = `
      MATCH (place:E53:UH4D)
      WHERE exists(place.osm)
      AND NOT (place)<-[:P121]-()
      DELETE place
    `;

    await this.neo4j.write(q);
  }
}
