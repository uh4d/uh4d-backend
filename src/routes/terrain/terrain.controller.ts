import {
  Controller,
  Get,
  NotFoundException,
  Param,
  Query,
} from '@nestjs/common';
import {
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { TerrainDto } from '@dto/terrain';
import { TerrainService } from './terrain.service';
import { TerrainQueryParams } from './dto/terrain-query-params';

@ApiTags('Terrain')
@Controller('terrain')
export class TerrainController {
  constructor(private readonly terrainService: TerrainService) {}

  @ApiOperation({
    summary: 'Query custom terrains',
    description:
      'Query available custom terrains. Query parameters `lat` and `lon` may be set to filter the data by searching for terrain models where the position is within their boundaries.',
  })
  @ApiOkResponse({
    type: TerrainDto,
    isArray: true,
  })
  @Get()
  async query(@Query() queryParams: TerrainQueryParams): Promise<TerrainDto[]> {
    return this.terrainService.query(queryParams);
  }

  @ApiOperation({
    summary: 'Get custom terrain',
    description: 'Get custom terrain by ID.',
  })
  @ApiOkResponse({
    type: TerrainDto,
  })
  @ApiNotFoundResponse()
  @Get(':terrainId')
  async getById(@Param('terrainId') terrainId: string): Promise<TerrainDto> {
    const terrain = await this.terrainService.getById(terrainId);

    if (!terrain) {
      throw new NotFoundException(`Terrain with ID ${terrainId} not found!`);
    }

    return terrain;
  }
}
