import { Injectable } from '@nestjs/common';
import { Neo4jService } from '@brakebein/nest-neo4j';
import { TerrainDto } from '@dto/terrain';
import { TerrainQueryParams } from './dto/terrain-query-params';

@Injectable()
export class TerrainService {
  constructor(private readonly neo4j: Neo4jService) {}

  async query(query: TerrainQueryParams): Promise<TerrainDto[]> {
    // check query parameter
    const spatialParams = {
      latitude: parseFloat(query.lat),
      longitude: parseFloat(query.lon),
    };
    const doSpatialQuery =
      !isNaN(spatialParams.latitude) && !isNaN(spatialParams.longitude);

    // compose query
    let q = `
      MATCH (scene:E53:UH4D)-[:P1]->(name:E41),
            (scene)<-[:P121]-(terrain)<-[:P67]-(:D1)-[:P106]->(file:D9),
            (terrain)-[:P168]->(geo:E94)`;

    if (doSpatialQuery) {
      q += `
        WHERE $geo.latitude > geo.south AND $geo.latitude < geo.north
        AND $geo.longitude > geo.west AND $geo.longitude < geo.east`;
    }

    q += `
      RETURN scene.id AS id,
             name.value AS name,
             apoc.map.removeKey(geo, 'id') AS location,
             apoc.map.removeKey(file, 'id') AS file
    `;

    const params = {
      geo: spatialParams,
    };

    return this.neo4j.read(q, params);
  }

  async getById(terrainId: string): Promise<TerrainDto | undefined> {
    // language=Cypher
    const q = `
      MATCH (scene:E53:UH4D {id: $sceneId})-[:P1]->(name:E41),
            (scene)<-[:P121]-(terrain)<-[:P67]-(:D1)-[:P106]->(file:D9),
            (terrain)-[:P168]->(geo:E94)
      RETURN scene.id AS id,
             name.value AS name,
             apoc.map.removeKey(geo, 'id') AS location,
             apoc.map.removeKey(file, 'id') AS file
  `;

    const params = {
      sceneId: terrainId,
    };

    return (await this.neo4j.read(q, params))[0];
  }
}
