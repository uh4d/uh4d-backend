import {
  Controller,
  Get,
  NotFoundException,
  Param,
  Query,
} from '@nestjs/common';
import {
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { MapDto } from '@dto/map';
import { MapsService } from './maps.service';
import { MapsQueryParams } from './dto/maps-query-params';

@ApiTags('Maps')
@Controller('terrain/:terrainId/maps')
export class MapsController {
  constructor(private readonly mapService: MapsService) {}

  @ApiOperation({
    summary: 'Query maps',
    description: 'Query available maps for terrain.',
  })
  @ApiOkResponse({
    type: MapDto,
    isArray: true,
  })
  @Get()
  async query(
    @Param('terrainId') terrainId: string,
    @Query() queryParams: MapsQueryParams,
  ) {
    return this.mapService.query(terrainId, queryParams.date);
  }

  @ApiOperation({
    summary: 'Get map',
    description: 'Get map by ID.',
  })
  @ApiOkResponse({
    type: MapDto,
  })
  @ApiNotFoundResponse()
  @Get(':mapId')
  async getById(@Param('mapId') mapId: string): Promise<MapDto> {
    const map = await this.mapService.getById(mapId);

    if (!map) {
      throw new NotFoundException(`Map with ID ${mapId} not found!`);
    }

    return map;
  }
}
