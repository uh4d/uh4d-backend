import { Injectable } from '@nestjs/common';
import { Neo4jService } from '@brakebein/nest-neo4j';
import { MapDto } from '@dto/map';

@Injectable()
export class MapsService {
  constructor(private readonly neo4j: Neo4jService) {}

  async query(terrainId: string, date?: string): Promise<MapDto[]> {
    // language=Cypher
    const q = `
    MATCH (e92:E92:UH4D)-[:P161]->(:E53 {id: $scene})-[:P168]->(geo:E94),
          (e92)-[:P160]->(:E52)-[:P82]->(date:E61 ${
            date ? '{value: $date}' : ''
          }),
          (e92)<-[:P67]-(:E38)-[:P106]->(file:D9)
    RETURN e92.id AS id,
           date.value AS date,
           apoc.map.removeKey(file, 'id') AS file,
           apoc.map.removeKey(geo, 'id') AS location
    ORDER BY date
  `;

    const params = {
      scene: terrainId,
      date: date,
    };

    return this.neo4j.read(q, params);
  }

  async getById(mapId: string): Promise<MapDto | undefined> {
    // language=Cypher
    const q = `
      MATCH (e92:E92:UH4D {id: $id})-[:P161]->(:E53)-[:P168]->(geo:E94),
            (e92)-[:P160]->(:E52)-[:P82]->(date:E61),
            (e92)<-[:P67]-(:E38)-[:P106]->(file:D9)
      RETURN e92.id AS id,
             date.value AS date,
             apoc.map.removeKey(file, 'id') AS file,
             apoc.map.removeKey(geo, 'id') AS location
    `;

    const params = {
      id: mapId,
    };

    return (await this.neo4j.read(q, params))[0];
  }
}
