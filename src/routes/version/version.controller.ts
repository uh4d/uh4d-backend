import { Controller, Get, Logger } from '@nestjs/common';
import { ApiOkResponse, ApiOperation } from '@nestjs/swagger';
import { ConfigService } from '@nestjs/config';
import { Neo4jService } from '@brakebein/nest-neo4j';
import { execFile } from 'child-process-promise';
import { VersionDto } from './dto/version.dto';
import { getPkgJson } from '@utils/ref-file';

@Controller('version')
export class VersionController {
  private readonly logger = new Logger(VersionController.name);

  constructor(
    private readonly config: ConfigService,
    private readonly neo4j: Neo4jService,
  ) {}

  @ApiOperation({
    summary: 'Print version',
    description:
      'Print version of this API and third-party applications, on which some operations rely on.',
  })
  @ApiOkResponse({
    type: VersionDto,
  })
  @Get()
  async printVersion(): Promise<VersionDto> {
    // Neo4j
    let neo4j = '';
    try {
      const neo4jServerInfo = await this.neo4j
        .getDriver()
        .verifyConnectivity({ database: this.config.get('neo4j.database') });
      neo4j = neo4jServerInfo.version;
    } catch (e) {
      this.logger.error(e);
    }

    // Assimp
    let assimp = '';
    try {
      const result = await execFile(this.config.get('exec.Assimp'), [
        'version',
      ]);
      const matches = result.stdout.match(/Version\s(\S+)\s/);
      assimp = matches[1];
    } catch (e) {
      this.logger.error(e);
    }

    // Collada2Gltf
    let collada = '';
    try {
      // Collada2Gltf has no explicit command to get the version
      // check if executed without command options will output help message
      const version = await execFile(this.config.get('exec.Collada2Gltf'), [])
        .then((result) => result)
        .catch((reason) => reason)
        .then((result) => {
          const matches = result.stdout.match(/^(COLLADA2GLTF)/);
          if (matches && matches[1] === 'COLLADA2GLTF') {
            return matches[1];
          } else {
            return Promise.reject(result);
          }
        });
      collada = `${version} (unknown version)`;
    } catch (e) {
      this.logger.error(e);
    }

    return {
      API: `UH4D ${getPkgJson().version}`,
      Neoj4: neo4j || 'FAILED!',
      Assimp: assimp || 'FAILED!',
      Collada2Gltf: collada || 'FAILED!',
    };
  }
}
