import { ApiProperty } from '@nestjs/swagger';

export class VersionDto {
  @ApiProperty({ required: false })
  API: string;
  @ApiProperty({ required: false })
  Neoj4: string;
  @ApiProperty({ required: false })
  Assimp: string;
  @ApiProperty({ required: false })
  Collada2Gltf: string;
}
