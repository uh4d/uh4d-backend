import {
  BadRequestException,
  Body,
  Controller,
  DefaultValuePipe,
  Get,
  InternalServerErrorException,
  NotFoundException,
  Param,
  Patch,
  Post,
  Query,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import {
  ApiBadRequestResponse,
  ApiConsumes,
  ApiCreatedResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { ConfigService } from '@nestjs/config';
import { FileInterceptor } from '@nestjs/platform-express';
import { Express } from 'express';
import { join, parse } from 'node:path';
import { nanoid } from 'nanoid';
import * as fs from 'fs-extra';
import { replace } from '@utils/string-utils';
import { ImageMediaService } from '@upload/image-media.service';
import { ImagesService } from './images.service';
import { ImageDto } from '@dto/image';
import { GetImageQueryParams } from './dto/get-image-query-params';
import { GetImagesQueryParams } from './dto/get-images-query-params';
import {
  CreateImageFullDto,
  ImageFullDto,
  UpdateImageFullDto,
} from './dto/image-full.dto';
import { DateExtentDto } from './dto/date-extent.dto';
import { UploadImageDto } from './dto/upload-image.dto';
import { LocationQueryParams } from './dto/location-query-params';
import {
  CreateLinkToObjectsDto,
  LinkToObjectsDto,
} from './dto/link-to-objects.dto';
import { FileUpdateDto } from './dto/file-update.dto';

@ApiTags('Images')
@Controller('images')
export class ImagesController {
  constructor(
    private readonly imageService: ImagesService,
    private readonly config: ConfigService,
    private readonly imageMediaService: ImageMediaService,
  ) {}

  @ApiOperation({
    summary: 'Query images',
    description:
      'Query all images. Query parameters may be set to filter the data. Only the most essential information gets returned. For more detailed information of an image, refer to `/images/{imageId}`.\n\n`lat`, `lon`, and `r` works only in combination and may be used to look for images within a radius at a position.',
  })
  @ApiOkResponse({
    type: ImageDto,
    isArray: true,
  })
  @Get()
  async query(@Query() queryParams: GetImagesQueryParams): Promise<ImageDto[]> {
    return this.imageService.query(queryParams);
  }

  @ApiOperation({
    summary: 'Get date extent',
    description: 'Get minimum and maximum date of all images.',
  })
  @ApiOkResponse({
    type: DateExtentDto,
  })
  @Get('dateExtent')
  async getDateExtent(
    @Query() queryParams: LocationQueryParams,
  ): Promise<DateExtentDto> {
    const results = await this.imageService.getDateExtent(queryParams);

    // if there are no images with dates yet, return standard dates
    if (!results[0]) {
      return {
        from: '1800-01-01',
        to: '2020-01-01',
      };
    }

    return {
      from: results[0].d,
      to: results[1].d,
    };
  }

  @ApiOperation({
    summary: 'Get image',
    description: 'Get image by ID.',
  })
  @ApiOkResponse({
    type: ImageFullDto,
  })
  @ApiNotFoundResponse({
    description: 'Image with ID not found!',
  })
  @Get(':imageId')
  async getById(
    @Param('imageId') imageId: string,
    @Query() queryParams: GetImageQueryParams,
  ): Promise<ImageFullDto> {
    const image = await this.imageService.getById(
      imageId,
      queryParams.regexp === '1',
    );

    if (!image) {
      throw new NotFoundException(`Image with ID ${imageId} not found!`);
    }

    return image;
  }

  @ApiOperation({
    summary: 'Upload image',
    description:
      'Upload an image to position defined by `latitude` and `longitude`. If you choose a `radius` greater than `0`, The position is considered as unknown, but rests in an area inside this radius.',
  })
  @ApiConsumes('multipart/form-data')
  @ApiCreatedResponse({
    type: ImageFullDto,
  })
  @ApiBadRequestResponse()
  @Post()
  @UseInterceptors(FileInterceptor('uploadImage'))
  async upload(
    @UploadedFile() file: Express.Multer.File,
    @Body() body: UploadImageDto,
  ): Promise<ImageFullDto> {
    // check required data
    if (!body.latitude || !body.longitude) {
      await fs.remove(file.path);
      throw new BadRequestException('No latitude/longitude provided!');
    }

    const tmpDir = join(this.config.get<string>('path.data'), 'tmp', nanoid(9));
    const fileName = replace(file.originalname);
    const tmpFile = join(tmpDir, fileName);

    try {
      // create tmp folder
      await fs.ensureDir(tmpDir);
      // copy uploaded file to tmp folder
      await fs.rename(file.path, tmpFile);

      const meta: CreateImageFullDto = {
        title: body.title || parse(file.originalname).name,
        camera: {
          latitude: body.latitude,
          longitude: body.longitude,
          altitude: 0,
          radius: body.radius,
          offset: [],
          k: [],
        },
        spatialStatus: body.radius > 0 ? 0 : 3,
        file: await this.imageMediaService.generateImageMedia(tmpFile),
        tags: [],
      };

      // copy into image folder
      await fs.copy(
        join(tmpDir, meta.file.path),
        join(this.config.get<string>('path.data'), meta.file.path),
      );

      // write to database
      const result = await this.imageService.create(meta);

      if (!result) {
        // remove generated files
        await fs.remove(
          join(this.config.get<string>('path.data'), meta.file.path),
        );
        throw new Error('No data written to database!');
      }

      // retrieve image meta data
      return this.imageService.getById(result.image.id);
    } catch (e) {
      throw new InternalServerErrorException(e);
    } finally {
      // remove temporary folder
      await fs.remove(tmpDir);
    }
  }

  @ApiOperation({
    summary: 'Update image',
    description:
      'Update properties of an image. At the moment, it is only possible to update one property at a time. Use prop query parameter to specify which property should be updated. Currently supported values are: `title`, `date`, `description`, `misc`, `author`, `owner`, `tags`, `camera`, `spatialStatus`.',
  })
  @ApiOkResponse({
    type: ImageFullDto,
  })
  @ApiBadRequestResponse()
  @ApiNotFoundResponse()
  @Patch(':imageId')
  async update(
    @Param('imageId') imageId: string,
    @Body() body: UpdateImageFullDto,
    @Query('prop', new DefaultValuePipe('')) prop: string,
  ): Promise<ImageFullDto> {
    const image = this.imageService.update(imageId, body, prop);

    if (!image) {
      throw new NotFoundException(`Image with ID ${imageId} not found!`);
    }

    return this.imageService.getById(imageId);
  }

  @ApiOperation({
    summary: 'Link to objects',
    description:
      'Link image to objects with weights. Existing links will be overwritten or deleted. If no `weights` object is supplied, all links will be deleted.',
  })
  @ApiCreatedResponse({
    type: LinkToObjectsDto,
  })
  @ApiBadRequestResponse()
  @ApiNotFoundResponse()
  @Post(':imageId/link')
  async linkToObjects(
    @Param('imageId') imageId: string,
    @Body() body: CreateLinkToObjectsDto,
  ): Promise<LinkToObjectsDto> {
    const weights = [];
    if (body.weights) {
      for (const [key, value] of Object.entries(body.weights)) {
        if (typeof value !== 'number')
          throw new BadRequestException(
            `Object weight is not a number: "${key}": ${value}`,
          );
        weights.push({
          id: key,
          weight: value,
        });
      }
    }

    const result = await this.imageService.linkToObjects(imageId, weights);

    if (!result) {
      throw new NotFoundException(`Image with ID ${imageId} not found!`);
    }

    return {
      image: result.image,
      objects: result.weights.reduce((map, curr) => {
        map[curr.id] = curr.weight;
        return map;
      }, {}),
    };
  }

  @ApiOperation({
    summary: 'Check file update',
    description:
      'Check if a new, bigger sized image file is available. (Currently disabled and always returns `false`.)',
  })
  @ApiOkResponse({
    type: FileUpdateDto,
  })
  @Get(':imageId/file/check')
  async fileUpdateCheck(
    @Param('imageId') imageId: string,
  ): Promise<FileUpdateDto> {
    return {
      updateAvailable: false,
      reason: 'No permalink available!',
    };
  }
}
