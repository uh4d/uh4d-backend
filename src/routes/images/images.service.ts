import { BadRequestException, Injectable } from '@nestjs/common';
import { Neo4jService } from '@brakebein/nest-neo4j';
import { nanoid } from 'nanoid';
import { escapeRegex, replace } from '@utils/string-utils';
import { parseDate } from '@utils/parse-date';
import { checkRequiredProperties } from '@utils/check-required-properties';
import { ImageDto } from '@dto/image';
import { GetImagesQueryParams } from './dto/get-images-query-params';
import {
  CreateImageFullDto,
  ImageFullDto,
  UpdateImageFullDto,
} from './dto/image-full.dto';
import { LocationQueryParams } from './dto/location-query-params';

@Injectable()
export class ImagesService {
  constructor(private readonly neo4j: Neo4jService) {}

  async query(query: GetImagesQueryParams): Promise<ImageDto[]> {
    const stringFilter = [];
    const objFilter = [];
    const regexPosList = [];
    const regexNegList = [];
    const regexAuthorList = [];
    const regexOwnerList = [];

    // first triage
    (query.q ? (Array.isArray(query.q) ? query.q : [query.q]) : []).forEach(
      (value) => {
        if (/^obj:/.test(value)) {
          objFilter.push(value.replace(/^obj:/, ''));
        } else {
          stringFilter.push(value);
        }
      },
    );

    const spatialParams = {
      latitude: parseFloat(query.lat),
      longitude: parseFloat(query.lon),
      radius: parseFloat(query.r),
    };
    const doSpatialQuery =
      !isNaN(spatialParams.latitude) &&
      !isNaN(spatialParams.longitude) &&
      !isNaN(spatialParams.radius);

    let q = '';

    if (doSpatialQuery) {
      q +=
        'WITH point({latitude: $geo.latitude, longitude: $geo.longitude}) AS userPoint ';
    }

    if (query.scene) {
      q += `MATCH (:E53:UH4D {id: $scene})<-[:P89]-`;
    } else {
      q += 'MATCH ';
    }

    q += `(place:E53)<-[:P7]-(e65:E65)-[:P94]->(image:E38)`;

    if (objFilter.length) {
      q += `
      MATCH (image)-[:P138]->(e22:E22)
      WHERE e22.id IN $objList
    `;
    }

    if (doSpatialQuery) {
      q += `
      MATCH (place)-[:P168]->(geo:E94)
      WHERE distance(geo.point, userPoint) < $geo.radius + coalesce(geo.radius, 0)
    `;
    }

    if (
      query.from &&
      query.to &&
      query.from !== 'null' &&
      query.to !== 'null'
    ) {
      q += `
      OPTIONAL MATCH (e65)-[:P4]->(:E52)-[:P82]->(date:E61)
      WITH image, place, e65, date
      WHERE date ${
        query.undated === '1' ? 'IS NULL OR' : 'IS NOT NULL AND'
      } date.to > date($from) AND date.from < date($to)
    `;
    }

    q += `
      WITH image, place, e65
      MATCH (image)-[:P106]->(file:D9),
            (image)-[:P102]->(title:E35)
      OPTIONAL MATCH (e65)-[:P14]->(authorId:E21)-[:P131]->(author:E82)
      OPTIONAL MATCH (image)-[:P105]->(ownerId:E40)-[:P131]->(owner:E82)
      OPTIONAL MATCH (e65)-[:P4]->(:E52)-[:P82]->(date:E61)
      OPTIONAL MATCH (e65)-[:P16]->(spatial:E22)
      OPTIONAL MATCH (place)-[:P168]->(geo:E94)
      OPTIONAL MATCH (image)-[:has_tag]->(tag:TAG)
      WITH image, file, title, authorId, author, date, ownerId, owner, spatial, geo, collect(tag.id) AS tags
    `;

    stringFilter.forEach((value, index) => {
      const qPrefix = index === 0 ? ' WHERE' : ' AND';

      if (value === 'spatial:set') {
        q += qPrefix + ' spatial IS NOT NULL';
      } else if (value === 'spatial:unset') {
        q += qPrefix + ' spatial IS NULL';
      } else if (/^author:/.test(value)) {
        if (regexAuthorList.length < 1) {
          q +=
            qPrefix +
            ` any(e IN $regexAuthorList
                WHERE authorId.id = e
                OR author.value =~ "(?i).*" + e + ".*"
              )`;
        }
        regexAuthorList.push(escapeRegex(value.replace(/^author:/, '')));
      } else if (/^owner:/.test(value)) {
        if (regexOwnerList.length < 1) {
          q +=
            qPrefix +
            ` any(e IN $regexOwnerList
                WHERE ownerId.id = e
                OR owner.value =~ "(?i).*" + e + ".*"
              )`;
        }
        regexOwnerList.push(escapeRegex(value.replace(/^owner:/, '')));
      } else if (/^-/.test(value)) {
        if (regexNegList.length < 1) {
          q +=
            qPrefix +
            ` none(e IN $regexNegList
                WHERE title.value =~ e
                OR author.value =~ e
                OR owner.value =~ e
                OR any(tag IN tags WHERE tag =~ e)
              )`;
        }
        regexNegList.push(
          `(?i).*${escapeRegex(value.replace(/^-/, '').replace(/"/g, ''))}.*`,
        );
      } else {
        if (regexPosList.length < 1) {
          q +=
            qPrefix +
            ` all(e IN $regexPosList
                WHERE title.value =~ e
                OR author.value =~ e
                OR owner.value =~ e
                OR any(tag IN tags WHERE tag =~ e)
              )`;
        }
        regexPosList.push(`(?i).*${escapeRegex(value.replace(/"/g, ''))}.*`);
      }
    });

    if (query.weights === '1') {
      q += `
      WITH image, file, title, author, date, owner, spatial, geo, tags
      OPTIONAL MATCH (image)-[rw:P138]->(e22:E22)`;
    }

    q += ` RETURN
    image.id AS id,
    apoc.map.removeKey(file, 'id') AS file,
    title.value AS title,
    author.value AS author,
    owner.value AS owner,
    date,
    apoc.map.removeKey(apoc.map.merge(geo, spatial), 'id') AS camera,
    geo.status AS spatialStatus
  `;

    if (query.weights === '1') {
      q += `,
      CASE
        WHEN e22 IS NULL THEN []
        ELSE collect({id: e22.id, weight: rw.weight})
      END AS objects`;
    }

    const params = {
      scene: query.scene,
      from: query.from,
      to: query.to,
      regexPosList,
      regexNegList,
      regexAuthorList,
      regexOwnerList,
      objList: objFilter,
      geo: spatialParams,
    };

    const results = await this.neo4j.read(q, params);

    if (query.weights === '1') {
      results.forEach((img) => {
        img.objects = img.objects.reduce((map, curr) => {
          map[curr.id] = curr.weight;
          return map;
        }, {});
      });
    }

    return results;
  }

  async getById(
    imageId: string,
    fuzzySearch = false,
  ): Promise<ImageFullDto | undefined> {
    let q = '';

    if (fuzzySearch) {
      q += `MATCH (image:E38:UH4D)
        WHERE image.id =~ ".*" + $id + ".*"
        MATCH `;
    } else {
      q += 'MATCH (image:E38:UH4D {id: $id}),';
    }

    // language=Cypher
    q += `  (image)<-[:P94]->(e65:E65),
            (image)-[:P106]->(file:D9),
            (image)-[:P102]->(title:E35),
            (image)-[:P48]->(identifier:E42)
      OPTIONAL MATCH (e65)-[:P14]->(:E21)-[:P131]->(author:E82)
      OPTIONAL MATCH (e65)-[:P4]->(:E52)-[:P82]->(date:E61)
      OPTIONAL MATCH (image)-[:P105]->(:E40)-[:P131]->(owner:E82)
      OPTIONAL MATCH (image)-[:P3]->(desc:E62)-[:P3_1]->(:E55 {id: "image_description"})
      OPTIONAL MATCH (image)-[:P3]->(misc:E62)-[:P3_1]->(:E55 {id: "image_miscellaneous"})
      OPTIONAL MATCH (e65)-[:P16]->(spatial:E22)
      OPTIONAL MATCH (e65)-[:P7]->(:E53)-[:P168]->(geo:E94)
      OPTIONAL MATCH (image)-[:has_tag]->(tag:TAG)
    
      RETURN image.id AS id,
        apoc.map.removeKey(file, 'id') AS file,
        title.value AS title,
        identifier.permalink AS permalink,
        identifier.slub_cap_no AS captureNumber,
        author.value AS author,
        date {.*, from: toString(date.from), to: toString(date.to)} AS date,
        owner.value AS owner,
        desc.value AS description,
        misc.value AS misc,
        apoc.map.removeKey(apoc.map.merge(geo, spatial), 'id') AS camera,
        geo.status AS spatialStatus, 
        collect(tag.id) AS tags
    `;

    const params = {
      id: imageId,
    };

    return (await this.neo4j.read(q, params))[0];
  }

  async create(
    data: CreateImageFullDto,
  ): Promise<{ image: { id: string } } | undefined> {
    const date = parseDate(data.date?.value);

    let q = `
      MATCH (tdesc:E55:UH4D {id: "image_description"}), (tmisc:E55:UH4D {id: "image_miscellaneous"})
      CREATE (image:E38:UH4D {id: $imageId}),
        (image)-[:P102]->(title:E35:UH4D $title),
        (image)-[:P106]->(file:D9:UH4D $file),
        (image)-[:P48]->(identifier:E42:UH4D $identifier),
        (image)<-[:P94]-(e65:E65:UH4D {id: $e65id})-[:P7]->(place:E53:UH4D {id: $placeId})-[:P168]->(geo:E94:UH4D $geo)
      SET geo.point = point({latitude: $geo.latitude, longitude: $geo.longitude}) `;

    if (data.author)
      q += `MERGE (author:E21:UH4D)-[:P131]->(authorName:E82:UH4D {value: $author.value})
        ON CREATE SET author.id = $authorId, authorName.id = $author.id
      CREATE (e65)-[:P14]->(author) `;

    if (date)
      q += `CREATE (e65)-[:P4]->(:E52:UH4D {id: $e52id})-[:P82]->(date:E61:UH4D {id: $date.id, value: $date.value, from: date($date.from), to: date($date.to), display: $date.display}) `;

    if (data.owner)
      q += `MERGE (owner:E40:UH4D)-[:P131]->(ownerName:E82:UH4D {value: $owner.value})
        ON CREATE SET owner.id = $ownerId, ownerName.id = $owner.id
      CREATE (image)-[:P105]->(owner) `;

    if (data.description)
      q += `CREATE (image)-[:P3]->(desc:E62:UH4D $desc)-[:P3_1]->(tdesc) `;

    if (data.misc && data.misc.length)
      q += `CREATE (image)-[:P3]->(:E62:UH4D $misc)-[:P3_1]->(tmisc) `;

    q += `FOREACH (tag IN $tags |
        MERGE (t:TAG:UH4D {id: tag})
        MERGE (image)-[:has_tag]->(t)
      )
		
		  RETURN image`;

    const id = nanoid(9) + '_' + data.file.original;
    const authorId = nanoid(9) + '_' + replace(data.author);
    const ownerId = nanoid(9) + '_' + replace(data.owner);

    const params = {
      imageId: id,
      title: {
        id: 'e35_' + id,
        value: data.title,
      },
      identifier: {
        id: 'e42_' + id,
        permalink: data.permalink,
        // slub_id: data.id, TODO: save SLUB id and numbers
        // slub_cap_no: data.captureNumber,
      },
      file: { id: 'd9_' + id, ...data.file },
      placeId: 'e53_' + id,
      geo: {
        id: 'e94_' + id,
        latitude: data.camera.latitude,
        longitude: data.camera.longitude,
        altitude: data.camera.altitude,
        radius: data.camera.radius || 0,
        status: data.spatialStatus,
      },
      e65id: 'e65_' + id,
      e52id: 'e52_' + id,
      date: { id: 'e61_e52_' + id, ...date },
      author: {
        id: 'e82_' + authorId,
        value: data.author,
      },
      authorId: 'e21_' + authorId,
      owner: {
        id: 'e82_' + ownerId,
        value: data.owner,
      },
      ownerId: 'e40_' + ownerId,
      desc: {
        id: 'e62_desc_' + id,
        value: data.description,
      },
      misc: {
        id: 'e62_misc_' + id,
        value: Array.isArray(data.misc) ? data.misc.join(', ') : data.misc,
      },
      tags: data.tags || [],
    };

    return (await this.neo4j.write(q, params))[0];
  }

  async update(
    imageId: string,
    body: UpdateImageFullDto,
    prop: string,
  ): Promise<{ image: { id: string } } | undefined> {
    const exists = (
      await this.neo4j.read('MATCH (image:E38:UH4D {id: $id}) RETURN image', {
        id: imageId,
      })
    )[0];
    if (!exists) return undefined;

    const id = nanoid(9);

    let q = `MATCH (image:E38:UH4D {id: $id})<-[:P94]-(e65:E65)-[:P7]->(place:E53)-[:P168]->(geo:E94) `;

    const params: any = {
      id: imageId,
    };

    switch (prop) {
      case 'title':
        if (body.title) {
          throw new BadRequestException('Title must not be empty!');
        }

        q += `MATCH (image)-[:P102]->(title:E35) SET title.value = $title`;
        params.title = body.title;
        break;

      case 'date': {
        const date = parseDate(body.date.value);
        q += `
          MERGE (e65)-[:P4]->(e52:E52:UH4D)-[:P82]->(date:E61:UH4D)
            ON CREATE SET e52.id = $e52id, date.id = $e61id 
          SET date.value = $date.value`;
        if (date) {
          q += `,
            date.from = date($date.from),
            date.to = date($date.to),
            date.display = $date.display`;
        }
        params.date = date;
        params.e52id = 'e52_' + id;
        params.e61id = 'e61_e52_' + id;
        break;
      }

      case 'description':
        q += `MATCH (tdesc:E55:UH4D {id: "image_description"}) `;
        if (body.description) {
          // set desc
          q += `
            MERGE (image)-[:P3]->(desc:E62:UH4D)-[:P3_1]->(tdesc)
              ON CREATE SET desc.id = $desc.id
            SET desc.value = $desc.value`;
          params.desc = { id: 'e62_desc_' + id, value: body.description };
        } else {
          // remove desc
          q += `OPTIONAL MATCH (image)-[:P3]->(desc:E62)-[:P3_1]->(tdesc) DETACH DELETE desc`;
        }
        break;

      case 'misc':
        q += `MATCH (tmisc:E55:UH4D {id: "image_miscellaneous"}) `;
        if (body.misc) {
          // set misc
          q += `
            MERGE (image)-[:P3]->(misc:E62:UH4D)-[:P3_1]->(tmisc)
              ON CREATE SET misc.id = $misc.id
            SET misc.value = $misc.value`;
          params.misc = { id: 'e62_misc_' + id, value: body.misc };
        } else {
          // remove misc
          q += `OPTIONAL MATCH (image)-[:P3]->(misc:E62)-[:P3_1]->(tmisc) DETACH DELETE misc`;
        }
        break;

      case 'author':
        q += `OPTIONAL MATCH (e65)-[r14:P14]->(:E21)-[:P131]->(:E82)`;
        if (body.author) {
          q += `
            MERGE (e21:E21:UH4D)-[:P131]->(e82:E82:UH4D {value: $name})
              ON CREATE SET e21.id = $e21id, e82.id = $e82id
            CREATE (e65)-[:P14]->(e21)`;
          params.name = body.author;
          params.e21id = `e21_${id}_${replace(body.author)}`;
          params.e82id = `e82_${id}_${replace(body.author)}`;
        }
        q += ` DELETE r14`;
        break;

      case 'owner':
        q += `OPTIONAL MATCH (image)-[r105:P105]->(:E40)-[:P131}->(:E82)`;
        if (body.owner) {
          q += `
            MERGE (e40:E40:UH4D)-[:P131]->(e82:E82:UH4D {value: $owner})
              ON CREATE SET e40.id = $e40id, e82.id = $e82id
            CREATE (image)-[:P105]->(e40)`;
          params.owner = body.owner;
          params.e40id = `e40_${id}_${replace(body.owner)}`;
          params.e82id = `e82_${id}_${replace(body.owner)}`;
        }
        q += ' DELETE r105';
        break;

      case 'tags':
        q += `
          OPTIONAL MATCH (image)-[rtag:has_tag]->(:TAG)
          DELETE rtag
          WITH image
          FOREACH (tag in $tags |
            MERGE (t:TAG:UH4D {id: tag})
            MERGE (image)-[:has_tag]->(t)
          )`;
        params.tags = body.tags || [];
        break;

      case 'camera': {
        // manual spatialization
        const requiredProps = [
          'ck',
          'offset',
          'latitude',
          'longitude',
          'altitude',
          'omega',
          'phi',
          'kappa',
        ];
        if (!checkRequiredProperties(body.camera, requiredProps)) {
          throw new BadRequestException(
            'Missing values: ' + requiredProps.join('|'),
          );
        }

        q += `
          MERGE (e65)-[:P16]->(camera:E22:UH4D)
          SET camera = $camera
          SET geo += $geo
          SET geo.status = 4, geo.point = point({latitude: $geo.latitude, longitude: $geo.longitude})
          REMOVE geo.radius
        `;
        params.camera = {
          id: 'camera_' + imageId,
          ck: body.camera.ck,
          offset: body.camera.offset,
          k: [0],
          omega: body.camera.omega,
          phi: body.camera.phi,
          kappa: body.camera.kappa,
          method: 'manual',
        };
        params.geo = {
          id: 'e94_' + imageId,
          latitude: body.camera.latitude,
          longitude: body.camera.longitude,
          altitude: body.camera.altitude,
        };
        break;
      }

      case 'spatialStatus':
        // status can only be changed, if not yet spatialized
        const result = await this.neo4j.read(
          'MATCH (image:E38:UH4D {id: $id})<-[:P94]-(e65:E65) OPTIONAL MATCH (e65)-[:P16]->(camera:E22) RETURN camera',
          params,
        );
        if (result[0].camera) {
          throw new BadRequestException(
            'Image already properly spatialized. Status cannot be updated!',
          );
        }

        // update status
        switch (body.spatialStatus) {
          case 0:
            q += ' SET geo.status = 0';
            break;
          case 1:
            q += ' SET geo.status = 1';
            break;
          case 2:
            q += ' SET geo.status = 2';
            break;
          case 3:
            throw new BadRequestException(
              'spatialStatus: 3 cannot be set manually! Use `camera` property instead.',
            );
          case 4:
            throw new BadRequestException(
              'spatialStatus: 4 cannot be set manually! Use `camera` property instead.',
            );
          case 5:
            throw new BadRequestException(
              'spatialStatus: 5 cannot be set manually! Use POST `pipeline/images` route instead.',
            );
          default:
            throw new BadRequestException('No valid spatialStatus!');
        }
    }

    q += ' RETURN image';

    return (await this.neo4j.write(q, params))[0];
  }

  async linkToObjects(
    imageId: string,
    weights: { id: string; weight: number }[],
  ) {
    // language=Cypher
    let q = `
      MATCH (image:E38:UH4D {id: $id})
      OPTIONAL MATCH (image)-[r:P138]->(:E22)
      DELETE r`;

    if (weights.length) {
      // language=Cypher
      q += `
        WITH image
        UNWIND $weights AS value
        MATCH (e22:E22:UH4D {id: value.id})
        MERGE (image)-[r:P138]->(e22)
        SET r.weight = value.weight
        RETURN image.id AS image, collect({id: e22.id, weight: r.weight}) AS weights
  `;
    } else {
      q += `
        RETURN image.id AS image, [] AS weights
    `;
    }

    const params = {
      id: imageId,
      weights,
    };

    return (await this.neo4j.write(q, params))[0];
  }

  async getDateExtent(query: LocationQueryParams): Promise<{ d: string }[]> {
    const spatialParams = {
      latitude: parseFloat(query.lat),
      longitude: parseFloat(query.lon),
      radius: parseFloat(query.r),
    };
    const doSpatialQuery =
      !isNaN(spatialParams.latitude) &&
      !isNaN(spatialParams.longitude) &&
      !isNaN(spatialParams.radius);

    let q = '';

    if (doSpatialQuery) {
      q +=
        'WITH point({latitude: $geo.latitude, longitude: $geo.longitude}) AS userPoint ';
    }

    // language=Cypher
    q += `
      MATCH (image:E38:UH4D)<-[:P94]-(e65:E65)-[:P7]->(place:E53)`;

    if (doSpatialQuery) {
      // language=Cypher
      q += `
        MATCH (place)-[:P168]->(geo:E94)
        WHERE distance(geo.point, userPoint) < coalesce(geo.radius, $geo.radius, 0)`;
    }

    // language=Cypher
    q += `
      CALL {
        WITH e65
        MATCH (e65)-[:P4]->(:E52)-[:P82]->(date:E61)
        WHERE exists(date.from)
        RETURN date.to AS d
        ORDER BY d
        LIMIT 1
        UNION
        WITH e65
        MATCH (e65)-[:P4]->(:E52)-[:P82]->(date:E61)
        WHERE exists(date.to)
        RETURN date.to AS d
        ORDER BY d DESC
        LIMIT 1
      }
      RETURN d
    `;

    const params = {
      geo: spatialParams,
    };

    return this.neo4j.read(q, params);
  }
}
