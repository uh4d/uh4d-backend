import { ApiProperty } from '@nestjs/swagger';

export class DateExtentDto {
  @ApiProperty({
    type: 'string',
    format: 'date-time',
    required: false,
  })
  from: string;
  @ApiProperty({
    type: 'string',
    format: 'date-time',
    required: false,
  })
  to: string;
}
