import { IsDateString, IsIn, IsOptional } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { LocationQueryParams } from './location-query-params';

export class GetImagesQueryParams extends LocationQueryParams {
  @ApiProperty({
    description:
      'Query string to filter results (looks for title, author, owner, tags). The query string can be prefixed to search more precisely. Currently supported prefixes are: `obj:`, `author:`, `owner:`. A simple dash `-` will exclude query from results. Special queries are `spatial:set` and `spatial:unset` to look for images that are already spatialized or not yet spatialized, respectively.',
    required: false,
  })
  @IsOptional()
  q?: string;

  @ApiProperty({
    description: 'Time-span minimum value',
    required: false,
  })
  @IsOptional()
  @IsDateString()
  from?: string;

  @ApiProperty({
    description: 'Time-span maximum value',
    required: false,
  })
  @IsOptional()
  @IsDateString()
  to?: string;

  @ApiProperty({
    description:
      "If `from` and `to` are set, `undated=1` will include also images that don't have a date yet.",
    required: false,
  })
  @IsOptional()
  @IsIn(['0', '1'])
  undated?: string;

  @ApiProperty({
    description: 'Filter by specific scene',
    required: false,
  })
  @IsOptional()
  scene?: string;

  @ApiProperty({
    description: 'Also retrieve object weights if `weights=1`',
    required: false,
  })
  @IsOptional()
  @IsIn(['0', '1'])
  weights?: string;
}
