import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsObject } from 'class-validator';

export class LinkToObjectsDto {
  @ApiProperty({
    description: 'Image ID',
  })
  image: string;
  @ApiProperty({
    description:
      'Hash map, where the keys are object ids and the values the weights `{[objectId: string]: number}`',
    type: 'object',
    additionalProperties: {
      type: 'number',
      format: 'float',
    },
    example: {
      e22_TDN1l6fHHA_rathausgasse_1: 0.0349838461077,
    },
  })
  objects: { [objectId: string]: number };
}

export class CreateLinkToObjectsDto {
  @ApiProperty({
    description:
      'Hash map, where the keys are object ids and the values the weights `{[objectId: string]: number}`',
    type: 'object',
    additionalProperties: {
      type: 'number',
      format: 'float',
    },
    example: {
      e22_TDN1l6fHHA_rathausgasse_1: 0.0349838461077,
    },
  })
  @IsNotEmpty()
  @IsObject()
  weights: { [objectId: string]: number };
}
