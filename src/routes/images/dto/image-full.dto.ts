import { IntersectionType } from '@nestjs/swagger';
import { CreateImageDto, ImageDto, UpdateImageDto } from '@dto/image';
import {
  CreateImageMetaDto,
  ImageMetaDto,
  UpdateImageMetaDto,
} from '@dto/image-meta';

export class ImageFullDto extends IntersectionType(ImageDto, ImageMetaDto) {}

export class CreateImageFullDto extends IntersectionType(
  CreateImageDto,
  CreateImageMetaDto,
) {}

export class UpdateImageFullDto extends IntersectionType(
  UpdateImageDto,
  UpdateImageMetaDto,
) {}
