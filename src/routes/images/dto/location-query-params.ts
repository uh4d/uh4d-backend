import { ApiProperty } from '@nestjs/swagger';
import {
  IsLatitude,
  IsLongitude,
  IsNumberString,
  IsOptional,
} from 'class-validator';

export class LocationQueryParams {
  @ApiProperty({
    description: 'Latitude in decimal degree',
    required: false,
  })
  @IsOptional()
  @IsNumberString()
  @IsLatitude()
  lat?: string;

  @ApiProperty({
    description: 'Longitude in decimal degree',
    required: false,
  })
  @IsOptional()
  @IsNumberString()
  @IsLongitude()
  lon?: string;

  @ApiProperty({
    description: 'Radius in meters',
    required: false,
  })
  @IsOptional()
  @IsNumberString()
  r?: string;
}
