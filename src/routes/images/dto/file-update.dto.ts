import { ApiProperty } from '@nestjs/swagger';

export class FileUpdateDto {
  @ApiProperty({
    description: 'Resolves `true` if update is available',
    required: false,
  })
  updateAvailable: boolean;
  @ApiProperty({
    description: 'Reason why update is not available',
    required: false,
  })
  reason: string;
}
