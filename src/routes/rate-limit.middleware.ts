import { NextFunction, Request, Response } from 'express';
import { Subject } from 'rxjs';
import { rateLimitOperator } from '@utils/rate-limit-operator';

/**
 * Middleware to rate limit requests.
 * @param limit Number of requests
 * @param window Rolling window duration (in milliseconds)
 */
export function rateLimit(limit: number, window: number) {
  const subject = new Subject();

  subject
    .pipe(rateLimitOperator(limit, window))
    .subscribe((next: NextFunction) => {
      next();
    });

  return (req: Request, res: Response, next: NextFunction) => {
    subject.next(next);
  };
}
