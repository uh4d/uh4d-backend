import {
  Controller,
  DefaultValuePipe,
  Get,
  NotFoundException,
  Param,
  Query,
} from '@nestjs/common';
import {
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiTags,
} from '@nestjs/swagger';
import { PersonsService } from './persons.service';
import { PersonDto } from './dto/person.dto';

@ApiTags('Metadata')
@Controller('persons')
export class PersonsController {
  constructor(private readonly personService: PersonsService) {}

  @ApiOperation({
    summary: 'Query persons',
    description:
      'Query all persons. Use `search` query parameter to filter results.',
  })
  @ApiOkResponse({
    type: PersonDto,
    isArray: true,
  })
  @Get()
  async query(
    @Query('search', new DefaultValuePipe('')) search: string,
  ): Promise<PersonDto[]> {
    return this.personService.query(search);
  }

  @ApiOperation({
    summary: 'Get person',
    description: 'Get person by ID.',
  })
  @ApiOkResponse({
    type: PersonDto,
  })
  @ApiNotFoundResponse()
  @Get(':personId')
  async getById(@Param('personId') personId: string): Promise<PersonDto> {
    const person = await this.personService.getById(personId);

    if (!person) {
      throw new NotFoundException(`Person with ID ${personId} not found!`);
    }

    return person;
  }
}
