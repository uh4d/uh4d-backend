import { ApiProperty } from '@nestjs/swagger';

export class PersonDto {
  @ApiProperty({
    description: 'Person ID',
    required: false,
  })
  id: string;
  @ApiProperty({
    description: 'Person name',
    required: false,
  })
  value: string;
}
