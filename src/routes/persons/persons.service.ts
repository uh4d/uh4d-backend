import { Injectable } from '@nestjs/common';
import { Neo4jService } from '@brakebein/nest-neo4j';
import { escapeRegex } from '@utils/string-utils';
import { PersonDto } from './dto/person.dto';

@Injectable()
export class PersonsService {
  constructor(private readonly neo4j: Neo4jService) {}

  async query(search = ''): Promise<PersonDto[]> {
    // language=Cypher
    const q = `
      MATCH (e21:E21:UH4D)-[:P131]->(name:E82)
      WHERE name.value =~ $search
      RETURN e21.id as id, name.value AS value`;

    const params = {
      search: `(?i).*${escapeRegex(search)}.*`,
    };

    return this.neo4j.read(q, params);
  }

  async getById(personId: string): Promise<PersonDto> {
    // language=Cypher
    const q = `
      MATCH (e21:E21:UH4D {id: $id})-[:P131]->(name:E82)
      RETURN e21.id AS id, name.value AS value`;

    const params = {
      id: personId,
    };

    return (await this.neo4j.read(q, params))[0];
  }
}
