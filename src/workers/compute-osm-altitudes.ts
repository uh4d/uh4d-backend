import { workerData, parentPort } from 'worker_threads';
import { readJson } from 'fs-extra';
import { Mesh, Object3D } from 'three';
import { loadGltf } from 'node-three-gltf';
import { Coordinates } from '@utils/coordinates';
import { OSMGenerator } from '@utils/osm/osm-generator';

/**
 * Compute altitudes for OSM buildings based on digital elevation model.
 */
async function compute() {
  if (
    typeof workerData.lat !== 'number' ||
    typeof workerData.lon !== 'number'
  ) {
    throw new Error('lat,lon not defined');
  }

  // load model
  const origin = new Coordinates(workerData.lat, workerData.lon, 0);
  let terrainModel: Object3D;

  if (!workerData.modelPath) {
    throw new Error('modelPath not defined');
  }
  if (!workerData.osmPath) {
    throw new Error('osmPath not defined');
  }

  if (workerData.isCustomTerrain) {
    // load custom dgm model
    if (!workerData.terrainData?.location) {
      throw new Error('terrainData not defined');
    }

    const gltf = await loadGltf(workerData.modelPath);
    terrainModel = gltf.scene.children[0];
    terrainModel.position.copy(
      new Coordinates(workerData.terrainData.location).toCartesian(origin),
    );
    terrainModel.updateMatrixWorld(true);
  } else {
    // load cached elevation api terrain
    const gltf = await loadGltf(workerData.modelPath);
    const terrain = gltf.scene.getObjectByName('TerrainNode') as Mesh;

    // transform geometry
    const attr = terrain.geometry.getAttribute('position');
    for (let i = 0, l = attr.count; i < l; i++) {
      const vec = Coordinates.fromWebMercator(
        attr.getX(i),
        attr.getZ(i),
        attr.getY(i),
      ).toCartesian(origin);
      attr.setXYZ(i, vec.x, vec.y, vec.z);
    }
    terrain.geometry.computeBoundingBox();
    terrain.geometry.computeBoundingSphere();

    terrainModel = terrain;
  }

  // generate OSM buildings
  const osmData = await readJson(workerData.osmPath);
  const osmGenerator = new OSMGenerator(origin);

  await osmGenerator.generate(osmData);
  osmGenerator.update(terrainModel);

  // get altitudes
  const altitudes: { [osmId: string]: number } = {};

  osmGenerator.buildings.forEach((building) => {
    altitudes[building.userData.osmId] = building.position.y;
  });

  // send data
  parentPort.postMessage(altitudes);

  // dispose geometries and materials
  osmGenerator.dispose();
  terrainModel.traverse((child) => {
    if (child instanceof Mesh) {
      child.geometry.dispose();
      (Array.isArray(child.material)
        ? child.material
        : [child.material]
      ).forEach((mat) => mat.dispose());
    }
  });
}

// run async function
compute().then(() => {
  process.exit();
});
